#TCL script for Libero to convert PPD to STAPL
#A.Skoczen - February 2020 - creation

#libero_proj_dir
set dir actel
###################################################################################
# read project config from file
set fp [open "./config.cfg" r]
while { [gets $fp data] >= 0 } {
   #puts $data
   set tab [split $data =]
   set pattern [lindex $tab 0]
   switch $pattern {
	proj_name {set proj_name [lindex $tab 1]
				puts "Project: $proj_name"}
	top_name {set top_name [lindex $tab 1]
				puts "Top module: $top_name"}
	lib_name {set lib_name [lindex $tab 1]
				puts "Library: $lib_name"}
	tech {set tech [lindex $tab 1]
				puts "FPGA: $tech"}
	}
}
close $fp

puts "TCL script: stapl.tcl"

##############################################################
# open existing Libero design
set prj_file [format "./%s/%s.prjx" $dir $proj_name]
if { [file exists $prj_file] } {
	open_project -file $prj_file
} else {
	puts "as: Project file $prj_file does not exist"
	exit
}

##############################################################
# check old STAPL bitstream and if exists backup it
set bitstr [format "./bitstream/%s.stp" $top_name]
if { [file exists $bitstr] } {
	if { [file exists ./bitstream_backup] } {
		puts "as: Removing old backup ./bitstream backuped"
		file delete -force -- ./bitstream_backup
	}
	file rename ./bitstream ./bitstream_backup
	file mkdir ./bitstream 
	puts "as: Old directory ./bitstream backuped"
	#puts $aslogf "as: Old directory ./bitstream backuped"
} else {
	#puts $aslogf "as: No old STAPL"
	puts "as: No old STAPL"
}

##############################################################
# take top name from PPD name in Libero design tree
set ppd_file [format ./%s/designer/%s/%s.ppd $dir $top_name $top_name]
if { [file exists $ppd_file] } {
	set fbasename [file rootname [file tail $ppd_file]]
	puts "as: Top name: $fbasename"
} else {
	puts "as: Lack of PPD $ppd_file"
	save_project
	exit
}

##############################################################
# convert PPD to STAPL directly to ./bitstraem directory
# top_name in config.cfg must bi exactly the same as enity name of top VHDL unit.
# it is case sensitive !
export_bitstream_file \
         -file_name $top_name \
		 -export_dir {./bitstream} \
         -format {STP} \
         -limit_SVF_file_size 0 \
         -limit_SVF_file_by_max_filesize_or_vectors {SIZE} \
         -svf_max_filesize {1024} \
         -svf_max_vectors {1000} \
         -master_file 0 \
         -master_file_components {  } \
         -encrypted_uek1_file 0 \
         -encrypted_uek1_file_components { } \
         -encrypted_uek2_file 0 \
         -encrypted_uek2_file_components { } \
         -trusted_facility_file 1 \
         -trusted_facility_file_components {FABRIC} \
         -add_golden_image 0 \
         -golden_image_address {} \
         -golden_image_design_version {} \
         -add_update_image 0 \
         -update_image_address {} \
         -update_image_design_version {} \
         -serialization_stapl_type {SINGLE} \
         -serialization_target_solution {FLASHPRO_3_4_5} 
set file_stp [format "./%s/designer/%s/export/%s.stp" $dir $top_name $top_name]
if {[file exists $file_stp]} {
	file copy -force $file_stp ./bitstream/
	puts "as: STAPL programming file copied"
} else {
puts "as: STAPL programming file $file_stp dose not exist"
}
set res [glob -directory ./bitstream/ *.stp]
puts "as: STAP is: $res"

save_project
