#script for Libero with IGLOO2
#A.Skoczen - 12.2014
#A.Skoczen - revision 3.2015
#A.Skoczen - revision 8.2015
#A.Skoczen - revision 9.2015
#A.Skoczen - February 2016 - added config file for variables and path
#A.Skoczen - March 2019 - modification for Libero v12.0, added argument
#A.Skoczen - July 2019 - modification for Synplify 2019.03
#
#Requirements:
# synthesised design EDIF - file ./'argv 0'/'proj_name'.edn
# timing and physical constraints file - file ./constraints/'proj_name'_sdc.sdc and ./constraints/'top_name'.pdc
#
#Results:
# results directory - ./actel
# backannotation VHDL and SDF - files in ./backanno
# bitstream STAPL - file in ./bitstream
#
set systemTime [clock seconds]
set start_time [clock format $systemTime -format %H:%M:%S]
set start_date [clock format $systemTime -format %D]

#necessary configuration:
###################################################################################
# use arguments
if { $argc == 0 } {
	puts "Path to directory containing EDN file as argument for $argv0 TCL script is required"
	exit
} else {
	set synthres [format "%s" [lindex $argv 0]]
}
###################################################################################
# set 1 if you need backannotated design for postlayout simulation
# set 0 to save time
set gen_annotated_hdl 1
# for debug set 1
set short 0

###################################################################################
# read project config from file
set fp [open "./config.cfg" r]
while { [gets $fp data] >= 0 } {
   #puts $data
   set tab [split $data =]
   set pattern [lindex $tab 0]
   switch $pattern {
	proj_name {set proj_name [lindex $tab 1]
				puts "Project: $proj_name"}
	top_name {set top_name [lindex $tab 1]
				puts "Top module: $top_name"}
	lib_name {set lib_name [lindex $tab 1]
				puts "Library: $lib_name"}
	tech {set tech [lindex $tab 1]
				puts "FPGA: $tech"}
	}
}
close $fp

###################################################################################
# libero_proj_dir, dir for annotated netlist
set dir ./actel
set bann ./backanno

###################################################################################
# HDL language can be VHDL or verilog
set lang VHDL
if { $lang == "VHDL"} {
	set ext vhd
} else {
	set ext v
}

set aslogf [open aslogfile12.log a]
puts "as: ############################ START ###################################"
puts "as: Libero 12 project $proj_name with top unit $top_name in language $lang"
puts $aslogf "\n\nas: ############################ START ###################################"
puts $aslogf "as: Libero 12 project $proj_name with top unit $top_name in language $lang \n\n"
puts $aslogf "as: Time stamp: $start_date $start_time"
puts "as: Time stamp: $start_date $start_time"

###################################################################################
# setup directories and files 
file delete -force ./$dir
puts "as: Old directory ./$dir deleted"
puts $aslogf "as: Old directory ./$dir deleted"

if { [file exists ./bitstream_backup] } {
	file delete -force ./bitstream_backup
	puts "as: Old directory ./bitstream_backup deleted"
	puts $aslogf "as: Old directory ./bitstream_backup deleted"
}
set bitstr [format "./bitstream/%s.stp" $top_name]
if { [file exists $bitstr] } {
	file rename ./bitstream ./bitstream_backup
	file mkdir ./bitstream 
	puts "as: Old directoriy ./bitstream backuped"
	puts $aslogf "as: Old directoriy ./bitstream backuped"
} else {
	puts $aslogf "as: No old STAPL"
	puts "as: No old STAPL"
}
foreach baf [glob -nocomplain -directory $bann *] {
	puts $aslogf "as: $baf to delete"
	file delete $baf
}
puts "as: Directory $bann emptied"
puts $aslogf "as: Directory $bann emptied"

###################################################################################
# Create Libero project for specific FPGA device
# setting with lowered error generation level in constraint faile
# set EDN netlist
puts "FPGA: $tech"
switch $tech {
	IG2_150 {
		puts "as: IGLOO2"
		puts $aslogf "as: IGLOO2 M2GL150"
		new_project -location ./$dir -name $proj_name -hdl $lang \
		-family {IGLOO2} \
		-die {M2GL150} \
		-package {1152 FC} \
		-speed {-1} \
		-die_voltage {1.2} \
		-adv_options {DSW_VCCA_VOLTAGE_RAMP_RATE:100_MS} \
		-adv_options {IO_DEFT_STD:LVCMOS 2.5V} \
		-adv_options {RESTRICTPROBEPINS:0} \
		-adv_options {TEMPR:COM} \
		-adv_options {VCCI_1.2_VOLTR:COM} \
		-adv_options {VCCI_1.5_VOLTR:COM} \
		-adv_options {VCCI_1.8_VOLTR:COM} \
		-adv_options {VCCI_2.5_VOLTR:COM} \
		-adv_options {VCCI_3.3_VOLTR:COM} \
		-adv_options {VOLTR:COM}
	}
	IG2_060 {
		puts "as: IGLOO2"
		puts $aslogf "as: IGLOO2 M2GL060"
		new_project -location ./$dir -name $proj_name -hdl $lang \
		-family {IGLOO2} -die {M2GL060} -package {484 FBGA} -speed {-1} \
		-die_voltage {1.2} \
		-adv_options {DSW_VCCA_VOLTAGE_RAMP_RATE:100_MS} \
		-adv_options {IO_DEFT_STD:LVCMOS 3.3V} \
		-adv_options {RESTRICTPROBEPINS:0} \
		-adv_options {TEMPR:COM} \
		-adv_options {VCCI_1.2_VOLTR:COM} \
		-adv_options {VCCI_1.5_VOLTR:COM} \
		-adv_options {VCCI_1.8_VOLTR:COM} \
		-adv_options {VCCI_2.5_VOLTR:COM} \
		-adv_options {VCCI_3.3_VOLTR:COM} \
		-adv_options {VOLTR:COM}
	}
	IG2_10 {
		puts "as: IGLOO2"
		puts $aslogf "as: IGLOO2 M2GL010"
		new_project -location ./$dir -name $proj_name -hdl $lang \
		-family {IGLOO2} -die {M2GL010} -package {484 FBGA} -speed {-1} \
		-die_voltage {1.2} \
		-adv_options {DSW_VCCA_VOLTAGE_RAMP_RATE:100_MS} \
		-adv_options {IO_DEFT_STD:LVCMOS 2.5V} \
		-adv_options {RESTRICTPROBEPINS:0} \
		-adv_options {TEMPR:COM} \
		-adv_options {VCCI_1.2_VOLTR:COM} \
		-adv_options {VCCI_1.5_VOLTR:COM} \
		-adv_options {VCCI_1.8_VOLTR:COM} \
		-adv_options {VCCI_2.5_VOLTR:COM} \
		-adv_options {VCCI_3.3_VOLTR:COM} \
		-adv_options {VOLTR:COM}
	}
	SF2 {
		puts "as: SmartFusion2"
		puts $aslogf "as: SmartFusion2"
		new_project -location ./$dir -name $proj_name -hdl $lang \
		-family {SmartFusion2} \
		-die {M2S010} \
		-package {484 FBGA} \
		-speed {-1} \
		-die_voltage {1.2} \
		-adv_options {DSW_VCCA_VOLTAGE_RAMP_RATE:100_MS} \
		-adv_options {IO_DEFT_STD:LVCMOS 2.5V} \
		-adv_options {RESTRICTPROBEPINS:1} \
		-adv_options {TEMPR:COM} \
		-adv_options {VCCI_1.2_VOLTR:COM} \
		-adv_options {VCCI_1.5_VOLTR:COM} \
		-adv_options {VCCI_1.8_VOLTR:COM} \
		-adv_options {VCCI_2.5_VOLTR:COM} \
		-adv_options {VCCI_3.3_VOLTR:COM} \
		-adv_options {VOLTR:COM}
	}
	default {
		puts "as: Lack of technology settings"
		puts $aslogf "as: Lack of technology settings"
		exit
	}
}

puts "as: Project $proj_name created"
puts $aslogf "as: Project $proj_name created"

project_settings -hdl {VHDL} -verilog_mode {VERILOG_2K} -vhdl_mode {VHDL_2008} -auto_update_modelsim_ini 1 -auto_update_viewdraw_ini 1 -enable_viewdraw 0 -standalone_peripheral_initialization 0 \
-instantiate_in_smartdesign 0 -ondemand_build_dh 1 -auto_generate_synth_hdl 0 -auto_generate_physynth_hdl 0 -auto_run_drc 0 -auto_generate_viewdraw_hdl 1 -auto_file_detection 1 -sim_flow_mode 0 -vm_netlist_flow 0 \
-enable_set_mitigation 0 -enable_design_separation 0 -display_fanout_limit {10} -abort_flow_on_sdc_errors 0 -abort_flow_on_pdc_errors 0 -block_mode 0 

puts "as: Project $proj_name settings assigned"
puts $aslogf "as: Project $proj_name settings assigned"

###################################################################################
# Report version and release (only after opening a project) 
set lv [get_libero_version]
set lr [get_libero_release]
puts "as: Libero release: $lr		Libero version: $lv"
puts $aslogf "as: Libero release: $lr		Libero version: $lv"

###################################################################################
# check version and stop if defferent then v12
set rev [lindex [split $lr .] 0]
if { $rev != "v12" } {
	puts "as: Incorrect Libero version: $lv is in PATH"
	puts $aslogf "as: Incorrect Libero version: $lv is in PATH"
	#save_project
	#close $aslogf
} else {

# basic part of Libero flow processing - begining
###################################################################################
# Import EDIF file 
set edif [format "%s/%s.edn" $synthres $proj_name]
puts "as: $edif ..."
puts $aslogf "as: $edif ..."
import_files -edif $edif
puts "as: EDIF for project $proj_name imported"
puts $aslogf "as: EDIF for project $proj_name imported"

set mod [format "%s::%s" $top_name $lib_name]
set_root -module $mod
#save_log -file {./test_log_file.txt}
save_project

###################################################################################
# Import constraint files
#set sdc [format "./constraints/%s_sdc.sdc" $proj_name]
set sdc [format "./constraints/%s.sdc" $top_name]
set iopdc [format "./constraints/%s_io.pdc" $top_name]
set iondc [format "./constraints/%s_io.ndc" $top_name]
set fppdc [format "./constraints/%s_fp.pdc" $top_name]
puts "as: $sdc $iopdc ..."
puts $aslogf "as: $sdc $iopdc ..."

import_files -sdc $sdc
puts "as: SDC $sdc for project $proj_name imported"
puts $aslogf "as: SDC $sdc for project $proj_name imported"

import_files -io_pdc $iopdc
puts "as: IO_PDC $iopdc for project $proj_name imported"
puts $aslogf "as: IO_PDC $iopdc for project $proj_name imported"

if {[file exists $iondc]} {
	import_files -ndc $iondc
	puts "as: NDC $iondc for project $proj_name imported"
	puts $aslogf "as: NDC $iondc for project $proj_name imported"
} else {
	puts "as: NDC $iondc for project $proj_name does not exist"
	puts $aslogf "as: NDC $iondc for project $proj_name does not exist"
}

if {[file exists $fppdc]} {
	import_files -fp_pdc $fppdc
	puts "as: FP_PDC $fppdc for project $proj_name imported"
	puts $aslogf "as: FP_PDC $fppdc for project $proj_name imported"
} else {
	puts "as: FP_PDC $fppdc for project $proj_name does not exist"
	puts $aslogf "as: FP_PDC $fppdc for project $proj_name does not exist"
}

#locations after import
#set file_sdc [format "./%s/constraint/%s_sdc.sdc" $dir $proj_name]
set file_sdc [format "./%s/constraint/%s.sdc" $dir $top_name]
set file_iopdc [format "./%s/constraint/io/%s_io.pdc" $dir $top_name]
set file_ndc [format "./%s/constraint/%s_io.ndc" $dir $top_name]
set file_fppdc [format "./%s/constraint/fp/%s_fp.pdc" $dir $top_name]
if {[file exists $file_ndc]} {
	puts "as: location after import $file_sdc $file_iopdc $file_ndc"
	puts $aslogf "as: location after import $file_sdc $file_iopdc $file_ndc"
} else {
	puts "as: location after import $file_sdc $file_iopdc"
	puts $aslogf "as: location after import $file_sdc $file_iopdc"
}

save_project

###################################################################################
# Constraints management
if {[file exists $file_ndc]} {
	organize_tool_files -tool {COMPILE} \
	-module $mod -input_type {constraint} \
	-file $file_ndc 
}
if {[file exists $file_fppdc]} {
	organize_tool_files -tool {PLACEROUTE} \
	-module $mod -input_type {constraint} \
	-file $file_sdc \
	-file $file_iopdc \
	-file $file_fppdc 
} else {
	organize_tool_files -tool {PLACEROUTE} \
	-module $mod -input_type {constraint} \
	-file $file_sdc \
	-file $file_iopdc 
}
puts "as: organize_constraints and _tool_files for PLACEROUTE - ok\n"
puts $aslogf "as: organize_constraints and _tool_files for PLACEROUTE - ok\n"

organize_tool_files -tool {VERIFYTIMING} \
-module $mod -input_type {constraint} \
-file $file_sdc 
puts "as: organize_constraints and _tool_files for VERIFYTIMING - ok\n"
puts $aslogf "as: organize_constraints and _tool_files for VERIFYTIMING - ok\n"

run_tool -name {CONSTRAINT_MANAGEMENT} 
#check_sdc_constraints -tool {designer}
#check_sdc_constraints -tool {placeroute}
#check_sdc_constraints -tool {timing}
puts "as: run_tool CONSTRAINT_MANAGEMENT - OK"
#derive_constraints_sdc 
#set sdc_ok [check_sdc_constraints –tool {designer}]
#if {$sdc_ok == 1} {exit}
#set pdc_ok [check_pdc_constraints –tool {designer}]
#if {$pdc_ok == 1} {exit}
#save_log -file {./test_log_file.txt} 
#generate_sdc_constraint_coverage -tool {PLACEROUTE}

# for debug 
if {$short} {
	puts "as: finish of short processing Libero"
	puts $aslogf "as: finish of short processing Libero"
	
} else {

###################################################################################
# Place and Route - configure and run
# Power-Driven Place and Route - no
# Timing-Driven Place and Route - yes
configure_tool \
-name {PLACEROUTE} \
-params {EFFORT_LEVEL:true} \
-params {INCRPLACEANDROUTE:false} \
-params {PDPR:false} \
-params {TDPR:true} \
-params {IOREG_COMBINING:true} \
-params {MULTI_PASS_LAYOUT:false} \
-params {REPAIR_MIN_DELAY:false}

run_tool -name {PLACEROUTE}
puts "as: PLACEROUTE ok"
puts $aslogf "as: PLACEROUTE ok"

###################################################################################
# Timing Analysis - configure and run
configure_tool \
-name {VERIFYTIMING} \
-params {CONSTRAINTS_COVERAGE:1} -params {FORMAT:TXT} \
-params {MAX_TIMING_FAST_HV_LT:1} -params {MAX_TIMING_VIOLATIONS_FAST_HV_LT:1} \
-params {MAX_TIMING_SLOW_LV_HT:1} -params {MAX_TIMING_VIOLATIONS_SLOW_LV_HT:1} \
-params {MIN_TIMING_FAST_HV_LT:1} -params {MIN_TIMING_VIOLATIONS_FAST_HV_LT:1} \
-params {MIN_TIMING_SLOW_LV_HT:1} -params {MIN_TIMING_VIOLATIONS_SLOW_LV_HT:1} 

run_tool -name {VERIFYTIMING}
puts "as: VERIFYTIMING ok"
puts $aslogf "as: VERIFYTIMING ok"

if { $gen_annotated_hdl } {
###################################################################################
# Export model and delays for post-layout simulation
# for generation chenge 0 to 1 above
configure_tool -name {EXPORTSDF} -params {DELAY_TYPE:falseE} -params {EXPORT_HDL_TYPE:$lang}
puts "as: configure_tool for EXPORTSDF ok"
puts $aslogf "as: configure_tool for EXPORTSDF ok"

run_tool -name {EXPORTSDF}
puts "as: EXPORTSDF ok"
puts $aslogf "as: EXPORTSDF ok"

set file_ba_vhd [format "./%s/designer/%s/%s_ba.%s" $dir $top_name $top_name $ext]
if {[file exists $file_ba_vhd]} {
	file copy -force $file_ba_vhd $bann
	puts  "as: $ext copied"
	puts $aslogf "as: $ext copied"
} else {
	puts "as: nestlist $lang does not exist"
	puts $aslogf "as: nestlist $lang does not exist"
}
set file_ba_sdf [format "./%s/designer/%s/%s_ba.sdf" $dir $top_name $top_name]
if {[file exists $file_ba_sdf]} {
	file copy -force $file_ba_sdf $bann
	puts "as: SDF copied"
	puts $aslogf "as: SDF copied"
} else {
	puts "as: SDF file dose not exist"
	puts $aslogf "as: SDF file dose not exist"
}
} else {
puts "as: NO generation of post-layout simulation model"
puts $aslogf "as: NO generation of post-layout simulation model"
}

###################################################################################
# Generate report about pins and I/O cells
if {[file exists $file_ndc]} {
	set dir_rep_pin [format "%s/designer/%s" $dir $top_name]
	export_pin_reports -export_dir $dir_rep_pin \
         -pin_report_by_name 1 \
         -pin_report_by_pkg_pin 1 \
         -bank_report 1 \
         -io_report 1 
	set ioff [format "%s/%s_ioff.rpt" $dir_rep_pin $top_name]
	puts "as: I/O Register Combining Summary is available in file $ioff"
	puts $aslogf "as: I/O Register Combining Summary is available in file $ioff"
}

###################################################################################
# Generate and export programing file
#configure_tool -name {GENERATEPROGRAMMINGFILE} 
run_tool -name {GENERATEPROGRAMMINGFILE} 
puts "as: Programming file generated"
puts $aslogf "as: Programming file generated"



##############################################################
# take top name from PPD name in Libero design tree
set ppd_file [format ./%s/designer/%s/%s.ppd $dir $top_name $top_name]
if { [file exists $ppd_file] } {
	set fbasename [file rootname [file tail $ppd_file]]
	puts "as: Top name: $fbasename"
} else {
	puts "as: Lack of PPD $ppd_file"
	save_project
	exit
}

##############################################################
# convert PPD to STAPL directly to ./bitstream directory
# top_name in config.cfg must bi exactly the same as enity name of top VHDL unit.
# it is case sensitive !
export_bitstream_file \
         -file_name $top_name \
		 -export_dir {./bitstream} \
         -format {STP} \
         -limit_SVF_file_size 0 \
         -limit_SVF_file_by_max_filesize_or_vectors {SIZE} \
         -svf_max_filesize {1024} \
         -svf_max_vectors {1000} \
         -master_file 0 \
         -master_file_components {  } \
         -encrypted_uek1_file 0 \
         -encrypted_uek1_file_components { } \
         -encrypted_uek2_file 0 \
         -encrypted_uek2_file_components { } \
         -trusted_facility_file 1 \
         -trusted_facility_file_components {FABRIC} \
         -add_golden_image 0 \
         -golden_image_address {} \
         -golden_image_design_version {} \
         -add_update_image 0 \
         -update_image_address {} \
         -update_image_design_version {} \
         -serialization_stapl_type {SINGLE} \
         -serialization_target_solution {FLASHPRO_3_4_5} 

set res [glob -directory ./bitstream/ *.stp]
puts "as: STAP is: $res"

}	
#short version
}
# basic part of Libero flow processing - finish

save_project

set systemTime [clock seconds]
set stop_time [clock format $systemTime -format %H:%M:%S]
set stop_date [clock format $systemTime -format %D]
puts "as: Time stamp for the end: $stop_date $stop_time (begining: $start_date $start_time)"
puts "as: ################################ STOP ###############################"
puts $aslogf "as: Time stamp for the end: $stop_date $stop_time (begining: $start_date $start_time)"
puts $aslogf "\nas: ############################# STOP ##################################\n\n"
close $aslogf
