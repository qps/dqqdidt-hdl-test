library ieee;
use ieee.std_logic_1164.all;

package util is
	function log2c(m: integer) return integer;
end util;

package body util is
	function log2c(m: integer) return integer is
		variable n, p: integer;
	begin
		n := 0;
		p := 1;
		while p < m loop
			n := n + 1;
			p := p * 2;
		end loop;
	return n;
	end log2c;
end util;

