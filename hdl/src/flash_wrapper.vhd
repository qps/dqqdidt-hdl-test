LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;
Library UQDSLib;
use UQDSLib.UQDSLib.ALL;

---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: flash_wrapper.vhd
-- File history:
--      0.1: 10/07/2010: 	initial version, works
--		0.2: 07/25/2011:	added write after erase functionality
--		0.3: 11/09/2011:	support for two byte read
--		0.4: 04/30/2012:	support for 2Mbit flash --> 24-bit address vector
--      0.5: 01/11/2013: 	added state for transparent mode to make busy timing 
--							identical to wrapper mode
--		1.0: 01/22/2018: 	Added support for 4-byte read and writes
--							added auto wel operation for "1001" sector unprotect command
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
-- 				wraps around the flash_at25f512b interface and provides extended features as verify
--				after write etc
--				Four commands are supported right now:
--				-read
--				-write (unlocks device before if needed and verifies afterwards)
--				-erase (unlocks device before)
--				-erase & write (erases block and writes one byte to specified address	
--				Transparent --> all cmd's are passed unaltered to the flash interface
--	
-- Function: 	
--				provides read and write routines to the flash interface
--
-- ToDo:		
--				
--
--
-- Targeted device: Actel ProAsic3 A3PE1500 PQ208
-- Author: Jens Steckert
--
----------------------------------------------------------------------------------------------------------
entity flash_wrapper is
port (
    clk : in std_logic;
    rst : in std_logic;
	--low level signals
	pso 		: in std_logic;
    pAT_flash_out 	: out AT_flash_outputs;
	--high(er) level signals
	pinputs : in flash_inputs; 
	poutputs : out flash_outputs
    
);
end flash_wrapper;


architecture a of flash_wrapper is

--component flash_at25f512b is
--port (
--    clk : in std_logic;
--    rst : in std_logic;
--	sck : out std_logic;
--	so 	: in std_logic;
--	si	: out std_logic;
--	cs	: out std_logic;
--	wp	: out std_logic;
--	hold : out std_logic;
--    cmd	: in std_logic_vector(3 downto 0);
--	busy : out std_logic;
--	pstatus : out std_logic_vector(7 downto 0);
--	paddress : in std_logic_vector(23 downto 0);
--	pwdata	: in std_logic_vector( 7 downto 0);
--	pwdata2	: in std_logic_vector( 7 downto 0);
--	prdata2 : out std_logic_vector(15 downto 0);
--	prdata2_valid : out std_logic;
--	prdata	: out std_logic_vector(7 downto 0)
--    
--);
--end component;


signal command  : std_logic_vector(3 downto 0);
signal wcommand : std_logic_vector(3 downto 0); --wcommand refers to the write command used 
												  --either byte write or config register write
signal write_after_erase : std_logic;

signal fbusy	: std_logic;
signal flash_in : flash_inputs;
signal flash_out : flash_outputs;
signal AT_flash_out : AT_flash_outputs;

type state_type is (idle, transparent, w_read_stat, w_eval_stat, w_unlock, w_wait_unlock, 
						w_write_byte, w_wait_write, w_wait_read, w_read_byte, w_verify,
						r_read, r_wait_read, wait_op,
						up_read_stat, up_eval_stat, up_unlock, up_wait_unlock, up_unprotect, up_wait_unprotect,
						e_read_stat, e_eval_stat, e_unlock, e_wait_unlock, e_erase, e_wait_erase);
					
signal state : state_type;

begin

flash_in <= pinputs;
poutputs <= flash_out;

pAT_flash_out <= AT_flash_out;
--command <= flash_in.cmd when flash_in.transparent = '1' else 


fsm: process(clk)
begin
if(clk'event and clk='1') then 
	if rst = '1' then
		command <= (others => '0');
		wcommand <= (others => '0');
		state <= idle;
		flash_out.busy <= '0';
		flash_out.write_ok <= '0';
		write_after_erase <= '0';
		flash_out.last_adr <= (others => '0');
	else
		case state is
			when idle =>
				if flash_in.transparent = '0' then
					
					case flash_in.cmd is
						when "0000" =>					--idle
							flash_out.busy <= '0';
							state <= idle;
						when "0001" =>					--read
							flash_out.busy <= '1';
							command <= "0001";
							
							state <= r_read;
						when "0010" =>					--write 1 byte
							command <= "0100"; --read status
							wcommand <= "0010"; --std byte program command
							flash_out.busy <= '1';
							state <= w_read_stat;
						when "1111" =>					--write 4 bytes
							command <= "0100"; --read status
							wcommand <= "1111"; --4 1111byte program command
							flash_out.busy <= '1';
							state <= w_read_stat;	
							
						when "0011" =>					--block erase
							command <= "0100";			--read status from flash to determine state
							flash_out.busy <= '1';
							state <= e_read_stat;
						when "0100" =>					--erase & write one byte
							command <= "0100";			--read status from flash
							wcommand <= "0010"; 		--std byte program command
							flash_out.busy <= '1';
							write_after_erase <= '1';
							state <= e_read_stat;
						when "0111" =>					--read two bytes
							flash_out.busy <= '1';
							command <= "0111";
							state <= r_read;
						when "1100" =>					--read four bytes
							flash_out.busy <= '1';
							command <= "1100";
							state <= r_read;
						when "1000" =>  				--protect sector  without unlock !
							command <= "1000";
							flash_out.busy <= '1';
							state <= wait_op;
						when "1001" =>					--unprotect sector with unlock
							flash_out.busy <= '1';
							command <= "0100"; 			--read status
							state <= up_read_stat;
							
						when "1010" => 				--write status register
							flash_out.busy <= '1';
							command <= "0100"; --read status
							wcommand <= "1010"; --write to config register
							state <= w_read_stat; --start with write but set wcommand to status register write
						when others =>
							state <= idle;
					end case;
				else 							-- wrapper in transparent mode passes through all commands
					command <= flash_in.cmd;
					flash_out.busy <= '1';
					state <= transparent;
				end if;
				flash_out.last_adr <= flash_in.address;
			when transparent =>
				--command <= "0000";
				flash_out.busy <= fbusy;
				state <= idle;
	---------------------------------------wait busy 0------------------------
			when wait_op =>
				command <= "0000";
				state <= r_read; ---go to r_read because it does the right thing
	--------------------------------------------read branch-----------------------------
			when r_read =>
				command <= "0000";
				--state <= wait_until_read;
				state <= r_wait_read;
				
			when r_wait_read =>
				
				if fbusy = '0' then
					state <= idle;
				else
					state <= r_wait_read;
				end if;
	---------------------------------------------write branch---------------------------
			when w_read_stat =>
				command <= "0000";
				state <= w_eval_stat;
			when w_eval_stat =>
				
				if fbusy = '1' then
					state <= w_eval_stat;
				else
					if flash_out.status(1) = '0' then --if wel bit is 0, device is locked
						command <= "0110"; --enable writing
						state <= w_unlock;
					else
						command <= wcommand;	--write enabled flag is on, proceed with write
						state <= w_write_byte;
						
					end if;
				end if;
			when w_unlock =>
				command <= "0000";
				state <= w_wait_unlock;
			when w_wait_unlock =>
				if fbusy = '0' then
					command <= wcommand; --write byte/status register
					state <= w_write_byte;
				else
					state <= w_wait_unlock;
				end if;
			when w_write_byte =>
				command <= "0000";
				state <= w_wait_write;
			when w_wait_write =>
				command <= "0000";
				if fbusy = '0' then
					command <= "0001";
					state <= w_read_byte;
				else
					state <= w_wait_write;
				end if;
			when w_read_byte =>
				command <= "0000";
				state <= w_wait_read;
			when w_wait_read =>
				if fbusy = '0' then
					state <= w_verify;
				else
					state <= w_wait_read;
				end if;
			when w_verify =>
				if flash_out.rdata = flash_in.wdata then
					flash_out.write_ok <= '1';
					state <= idle;
				else
					flash_out.write_ok <= '0';
					state <= idle;
				end if;
	-----------------------------unprotect macro--------------------------------------
	--up_read_stat, up_eval_stat, up_unlock, up_wait_unlock, up_unprotect, up_wait_unprotect
			when up_read_stat =>
				command <= "0000";
				state <= up_eval_stat;
			when up_eval_stat =>
				
				if fbusy = '1' then
					state <= up_eval_stat;
				else
					if flash_out.status(1) = '0' then --WRITE disABLED
						command <= "0110"; --writing is disabled, issuing unlock command
						state <= up_unlock;	
					else
						command <= "1001"; --writing is enabled, issuing sector unprotect command
						state <= up_unprotect;
						
					end if;
				end if;
			when up_unlock =>
				command <= "0000";
				state <= up_wait_unlock;
			when up_wait_unlock =>
				if fbusy = '0' then
					command <= "1001"; -- issue sector unprotect command
					state <= up_unprotect;
				else
					state <= up_wait_unlock;
				end if;
			when up_unprotect =>
				command <= "0000";
				state <= up_wait_unprotect;
			when up_wait_unprotect =>
				if fbusy = '0' then
					state <= idle; --go back to idle
					
				else
					state <= up_wait_unprotect;
				end if;
	-------------------------------------erase branch---------------------------------
	---e_read_stat, e_eval_stat, e_unlock, e_wait_unlock, e_erase, e_wait_erase);
			when e_read_stat =>
				command <= "0000";
				state <= e_eval_stat;
			when e_eval_stat =>
				
				if fbusy = '1' then
					state <= e_eval_stat;
				else
					if flash_out.status(1) = '0' then --WRITE disABLED
						command <= "0110"; --write is disabled goto unlock
						state <= e_unlock;	
					else
						command <= "0011"; --write is enabled, goto erase
						state <= e_erase;
						
					end if;
				end if;
			when e_unlock =>
				command <= "0000";
				state <= e_wait_unlock;
			when e_wait_unlock =>
				if fbusy = '0' then
					command <= "0011";
					state <= e_erase;
				else
					state <= e_wait_unlock;
				end if;
			when e_erase =>
				command <= "0000";
				state <= e_wait_erase;
			when e_wait_erase =>
				if fbusy = '0' then
					--check if write_after_erase command is pending
					if write_after_erase = '1' then
						command <= "0100"; --read status
						state <= w_read_stat; --enter write branch and write one byte
					else
						state <= idle; --go back to idle
					end if;
				else
					state <= e_wait_erase;
				end if;
			
			when others =>
				state <= idle;
		end case;
	end if;
end if;
end process;


flash: flash_at25f512b
port map (clk => clk, rst=> rst, 
			sck => AT_flash_out.sck, 
			so => pso, 
			si => AT_flash_out.si,
			cs => AT_flash_out.cs, 
			wp => AT_flash_out.wp, 
			hold => AT_flash_out.hold, 
			cmd => command, 
			busy => fbusy,
			pstatus => flash_out.status, 
			paddress => flash_in.address, 
			pwdata => flash_in.wdata,
			pwdata2 => flash_in.wdata2,
			pwdata3 => flash_in.wdata3,
			pwdata4 => flash_in.wdata4,
			prdata2 => flash_out.rdata2,
			prdata2_valid => flash_out.read_valid,
			prdata => flash_out.rdata,
			prdata4 => flash_out.rdata4
			);


end;