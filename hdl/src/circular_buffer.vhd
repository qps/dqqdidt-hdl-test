--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: 
-- File history:
--      <Revision number>: <Date>: <Comments>

--
-- Description: FIFO circular buffer with different reading and writing
--	clocks, prepared for clock domain crossing. Uses almost full and almost empty flags
-- 	with a margin of 8 words.
--
-- 
--
-- Targeted device: originally (<Family::IGLOO2> <Die::M2GL060> <Package::484 FBGA>)
-- Targeted device: Altera Cyclone V -- terrasic DE0-nano-SOC

-- Author:Dani Blasco
-- 
--
--------------------------------------------------------------------------------
LIBRARY IEEE;
LIBRARY UQDSLib;

USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.all;
USE UQDSLib.UQDSLib.ALL;

ENTITY circular_buffer IS
	generic(
		memory_size : integer := 256
	);
	PORT(
		-- RS485
		rst      : in  std_logic;
		data_in  : in  std_logic_vector(7 downto 0); -- wclock domain not crossing domains
		wclock   : in  std_logic;
		we       : in  std_logic;       -- wclock domain not crossing domains
		data_out : out std_logic_vector(7 downto 0); -- rclock domain not crossing domains
		rclock   : in  std_logic;
		re       : in  std_logic;       -- rclock domain not crossing domains
		aempty   : out std_logic;       -- rclock domain not crossing domains
		afull    : out std_logic        -- wclock domain not crossing domains
	);
END circular_buffer;

ARCHITECTURE behav OF circular_buffer IS

	type memory_type is array (0 to memory_size - 1) of std_logic_vector(7 downto 0);
	signal memory          : memory_type; -- Crossing domains but waddress =! raddress, then not crossing domains
	attribute syn_ramstyle : string;
	attribute syn_ramstyle of memory : signal is "no_rw_check";

	signal waddress, waddress_unstable, waddress_unstable2, waddress_unstable3, waddress_unstable4, waddress_stable : integer range 0 to memory_size - 1 := 0; -- wclock domain crossing domains!!
	signal raddress, raddress_unstable, raddress_unstable2, raddress_unstable3, raddress_unstable4, raddress_stable : integer range 0 to memory_size - 1 := 0; -- rclock domain crossing domains!!
	signal overlap, overlap_unstable, overlap_stable                                                                : std_logic                          := '0'; -- wclock domain crossing domains!!
	signal flag_raddress_incr, flag_raddress_stable, flag_raddress_incr_edge1, flag_raddress_incr_edge2             : std_logic                          := '0';
	signal raddress_wclock                                                                                          : integer range 0 to memory_size - 1 := 0;
	signal counter_empty                                                                                            : integer range 0 to 49              := 49;

begin

	write_data : process(wclock, rst)
	begin
		if (rising_edge(wclock)) then

			if rst = '1' then
				waddress        <= 0;
				--			raddress_unstable <= 0;
				--			raddress_stable   <= 0;
				raddress_wclock <= 0;
			--overlap           <= '0';

			else

				if (we = '1') then
					memory(waddress) <= data_in;
					waddress         <= waddress + 1;
				end if;

				--if (((waddress + 8) >= raddress_stable and overlap = '1') or ((waddress + 8) >= (raddress_stable + memory_size - 1) and overlap = '0')) then
				if (((waddress + 16 + 50) >= raddress_wclock and (waddress < raddress_wclock)) or ((waddress + 16 + 50) >= (raddress_wclock + memory_size - 1) and (waddress >= raddress_wclock))) then
					afull <= '1';
				else
					afull <= '0';
				end if;

				flag_raddress_stable     <= flag_raddress_incr;
				flag_raddress_incr_edge1 <= flag_raddress_stable;
				flag_raddress_incr_edge2 <= flag_raddress_incr_edge1;
				if (flag_raddress_incr_edge1 = '1' and flag_raddress_incr_edge2 = '0') then
					raddress_wclock <= raddress_wclock + 50;
				end if;

				--			if (waddress < raddress_stable) then
				--				overlap <= '1';
				--			else
				--				overlap <= '0';
				--			end if;

				--			raddress_unstable <= raddress;
				--			raddress_unstable2   <= raddress_unstable;
				--			raddress_unstable3   <= raddress_unstable2;
				--			raddress_unstable4   <= raddress_unstable3;
				--			raddress_stable   <= raddress_unstable4;
			end if;

		end if;
	end process;

	read_data : process(rclock, rst)
		variable counter_flag : integer range 0 to 5 := 5;
	begin
		if (falling_edge(rclock)) then

			if rst = '1' then
				raddress          <= 0;
				waddress_unstable <= 0;
				waddress_stable   <= 0;
				counter_empty     <= 49;
			--overlap_unstable  <= '0';
			--overlap_stable    <= '0';

			else

				if (re = '1' and counter_empty = 49) then

					raddress      <= raddress + 1;
					data_out      <= memory(raddress);
					counter_empty <= counter_empty - 1;
					-- counter for flag = 0
					counter_flag  := 0;

				elsif (re = '1' and counter_empty >= 0) then

					data_out      <= memory(raddress);
					raddress      <= raddress + 1;
					counter_empty <= counter_empty - 1;
					-- send raddress flag only once

					if (counter_empty = 0) then
						counter_empty <= 49;
					end if;

					if (counter_flag = 0) then
						--raddress <= raddress + 1;
						flag_raddress_incr <= '1';
						counter_flag       := counter_flag + 1;
					elsif (counter_flag < 3) then
						flag_raddress_incr <= '1';
						counter_flag       := counter_flag + 1;
					elsif (counter_flag < 5) then
						flag_raddress_incr <= '0';
						counter_flag       := counter_flag + 1;
					else
						flag_raddress_incr <= '0';
						--counter_flag := 0;
					end if;

				end if;

				if ((waddress_stable <= (raddress + 16 + 50) and (waddress_stable >= raddress)) or ((waddress_stable + memory_size - 1) <= (raddress + 16 + 50) and (waddress_stable < raddress))) then
					aempty <= '1';
				else
					aempty <= '0';
				end if;

				waddress_unstable  <= waddress;
				waddress_unstable2 <= waddress_unstable;
				waddress_unstable3 <= waddress_unstable2;
				waddress_unstable4 <= waddress_unstable3;
				waddress_stable    <= waddress_unstable4;
				--			overlap_unstable  <= overlap;
				--			overlap_stable    <= overlap_unstable;

			end if;

		end if;
	end process;

end behav;
