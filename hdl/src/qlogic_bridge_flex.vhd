library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE IEEE.fixed_pkg.all;
use IEEE.fixed_float_types.all;

LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;

--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: qlogic_bridge.vhd
-- File history:
--      0.1: 13.04.2018: first attempt, untested
--		1.0: 19.06.2019: added option to subtract, implemented using fixed pkg
--		2.0: 09.07.2019: generates two comparators on same value
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- implements an bridge like quench detection logic
--	input A + input B <= threshold
--	inputs assumed to be 2s complement signed integers
--
-- ToDo:
--		test !
--
-- Targeted device: IGLOO2
-- Author: Jens Steckert
--
--------------------------------------------------------------------------------
entity qlogic_bridge_flex is
generic(	d_width : natural := 32;				--data width
			subtract: boolean := FALSE;				--subtract inputs or add 
			num_comps: natural range 1 to 2 := 1	--number of comparators instantiated (max 2)
);
	port(
		clk 		: in std_logic;
		rst 		: in std_logic;
		qthreshold	: in channelsArray(num_comps-1 downto 0);
		time_disc	: in channelsArray(num_comps-1 downto 0);
		inA			: in std_logic_vector(d_width-1 downto 0);
		inB			: in std_logic_vector(d_width-1 downto 0);
		outAB 		: out std_logic_vector(d_width-1 downto 0);
		log_out 	: out std_logic_vector(3 downto 0)
	);
end entity qlogic_bridge_flex;

architecture RTL of qlogic_bridge_flex is
	
	
component qlogic_comp_simple is
	generic(d_width : integer range 15 to 33 := 32);
	port (
	    clk : 		in std_logic;
	    rst : 		in std_logic;
	    U_in : 		in std_logic_vector(d_width-1 downto 0);
		T_in : 		in std_logic_vector(d_width -1 downto 0);
		disc	: 	in std_logic_vector(15 downto 0);
		L_out : 	out std_logic_vector(3 downto 0)
	);
end component;	
	
	
	
--signal sumAB 		: signed(d_width downto 0);  --one bit longer
signal AB_clip	: std_logic_vector(d_width -1 downto 0); --standard length, clipped
signal sf_AB		:sf_voltage;
signal sf_inA		:sf_voltage;
signal sf_inB		:sf_voltage;
type log_out_arr_type is array (num_comps -1 downto 0) of std_logic_vector(3 downto 0);
signal log_out_arr : log_out_arr_type;
	
begin

--convert inputs to sfixed
sf_inA <= to_sfixed(inA, sf_inA);
sf_inB <= to_sfixed(inB, sf_inB);


--===========================add/subtract inputs===================================================

addvectors: if subtract = FALSE generate begin
--add U1 and U2 to create UQS1 this is perfomed on base of the decimated and filtered data
--done in a process to relax timing
	adder: process(clk)
	begin
	if rising_edge(clk) then
		if rst = '1' then
			sf_AB <= (others => '0');
		else
			--add inputs and take care of overflow (and rounding)
			sf_AB <= resize(arg 			=> (sf_inA + sf_inB), 
							size_res 		=> sf_AB,
							overflow_style 	=> fixed_saturate,
							round_style		=> fixed_truncate  
			);
			
		end if;
	end if;
	end process;
end generate;
subvectors: if subtract = TRUE generate begin
--subtract U1 and U2 to create UQS1 this is perfomed on base of the decimated and filtered data
--done in a process to relax timing
	subst: process(clk)
	begin
	if rising_edge(clk) then
		if rst = '1' then
			sf_AB <= (others => '0');
		else
			--subtract inputs and take care of overflow (and rounding)
			sf_AB <= resize(arg 			=> (sf_inA - sf_inB), 
							size_res 		=> sf_AB,
							overflow_style 	=> fixed_saturate,
							round_style		=> fixed_truncate  
			);
			
		end if;
	end if;
	end process;
end generate;

--convert back to std_logic vector
AB_clip <= to_slv(sf_AB);

--put sum to output port
outAB <= AB_clip;

--=======================================QD logic============================================================
--Generate one or two logic blocks 
genQDlogic: for i in 0 to num_comps - 1 generate
--quench logic for sum
quench_logic : qlogic_comp_simple 
	generic map(d_width => d_width)
	port map (clk => clk, rst => rst,
		U_in => AB_clip, 
		T_in => qthreshold(i), 
		disc => time_disc(i)(15 downto 0),
		L_out => log_out_arr(i));
end generate;

combine_logic: if num_comps = 1 generate begin
	
	log_out <= "00" & log_out_arr(0)(1 downto 0);
end generate;
combine_logic2: if num_comps = 2 generate begin
	log_out <= log_out_arr(1)(1 downto 0) & log_out_arr(0)(1 downto 0);
end generate; 

--===========================================================================================================



end architecture RTL;