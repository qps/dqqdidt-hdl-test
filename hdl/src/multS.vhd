library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UQDSLib;
use UQDSLib.util.all;

-------------------------------Description--------------------------------------
--Sequential Signed Multiplier
--W1 - bits number of multiplier
--W2 - bits number of multiplicand
--W1+W2-2 - bits number of product
--product is ready in output register after W2+2 clock cycles counting: 
-- -from first clk rising edge when start=1 
-- -to rising edge of done signal
--product can be kept in internal output register when parameter omode = '1'
--when omode = '0' a product is valid only when done=1
---------------------------------Versions-----------------------------------------
-- 28.08.2013 - A.Skoczen
-- 24.09.2013 - bug correction: length of counter 
--		CW = log2c(width_of_multiplier) - A.Skoczen
----------------------------------------------------------------------------------
entity multS is
generic (W1, W2: natural; omode: std_logic);
port(clk, rst, start	:in std_logic;
	multiplier	:in std_logic_vector(W1-1 downto 0);
	multiplicand	:in std_logic_vector(W2-1 downto 0);
	done		:out std_logic;
	product		:out std_logic_vector(W1+W2-2 downto 0));
end mults;

architecture rtl of multS is
--FSM declarations
constant CW: natural := log2c(W1);
constant cinit: unsigned := to_unsigned(W1-1,CW);

type states is (idle, ld, sh, d);
signal st, nst: states;
signal load, sht, add_sh, add, cm, c: std_logic;
signal count: unsigned(CW-1 downto 0);

--Data Path declarations
signal m: std_logic;
signal acc: std_logic_vector(W1+W2 downto 0);
signal tmp: std_logic_vector(W2 downto 0);
signal multiplicandC2: std_logic_vector(W2-1 downto 0);
signal ready: std_logic;

begin
--FSM state reg
streg: process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			st <= idle;
		else
			st <= nst;
		end if;
	end if;
end process;

--FSM nest state
comb_nst: process(all)
begin
	nst <= idle;
	case st is
		when idle => 
			if start = '1' then
				nst <= ld;
			else
				nst <= idle;
			end if;
		when ld => 
			nst <= sh;
		when sh => 
			if c = '1' then
				nst <= sh;
			else
				nst <= d;
			end if;
		when d => 
			nst <= idle;
		when others => null;
	end case;
end process;

--counter
c <= '0' when count = 0 else '1';
counter: process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			count <= cinit;
		else
			case st is
				when idle => count <= cinit;
				when sh => count <= count - 1;
				when others => null;
			end case;
		end if;
	end if;
end process;
	
--FSM outputs
comb_outfsm: process(all)
begin
	load <= '0';
	sht <= '0';
	add_sh <= '0';
	add <= '0';
	ready <= '0';
	cm <= '0';
	case st is
		when idle => 
			load <= start;
		when sh => 
			if c = '1' then
				if m = '1' then
					add_sh <= '1';
				else
					sht <= '1';
				end if;
			else
				add <= m;
				cm <= m;
			end if;
		when d =>
			ready <= '1';
		when others => null;
	end case;
end process;

--data path
m <= acc(0);

complement1: process(all)
begin
	if cm = '1' then
		multiplicandC2 <= not multiplicand;
	else
		multiplicandC2 <= multiplicand;
	end if;
end process;

adder_shifter: process(all)
begin
if add_sh = '1' or add = '1' then
	if cm = '1' then
		tmp <= std_logic_vector(signed(multiplicandC2(W2-1) & multiplicandC2) + signed(acc(W1+W2 downto W1)) + 1);
	else
		tmp <= std_logic_vector(signed(multiplicandC2(W2-1) & multiplicandC2) + signed(acc(W1+W2 downto W1)));
	end if;
else
	tmp <= (others => '0');
end if;
end process;

dp: process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			acc <= (others => '0');
		else
			if load = '1' then
				acc(W1-1 downto 0) <= multiplier;
				acc(W2+W1 downto W1) <= (others => '0');
			elsif add_sh = '1' then
				acc <= multiplicand(W2-1) & tmp & acc(W1-1 downto 1);
			elsif sht = '1' then
				acc <= acc(W1+W2) & acc(W1+W2 downto 1);
			elsif add = '1' then
				acc <= tmp & acc(W1-1 downto 0);
			end if;
		end if;
	end if;

end process;

registered: if omode = '1' generate begin
	reg_out: process(clk) begin
		if rising_edge(clk) then
			if rst = '1' then
				product <= (others => '0');
				done <= '0';
			else
				if ready = '1' then 
					done <= ready;
					product <= acc(W1+W2-1 downto 1);
				else
					done <= '0';
				end if;
			end if;
		end if;
	end process;
end generate;
unregistered: if omode = '0' generate begin
	done <= ready;
	product <= acc(W1+W2-1 downto 1);
end generate;

end rtl;

