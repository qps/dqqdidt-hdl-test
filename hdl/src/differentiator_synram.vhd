library ieee ;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
library UQDSLib;
use UQDSLib.all;

library work;
use work.util.all;
---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: differentiator_synram.vhd
-- File history:
--      0.1: 24.10.2011: first attempt
--		0.2: 07.11.2011: changed to 25 bit
--		0.3: 07.11.2011: array depth defined by generic
--		0.4: 11.01.2012: added edge detection for dready pulse
--      0.5: 19.01.2013: added scaler, to scale to A/s rather than A/dt
--		0.6: 25.06.2013: selectable data width 24, or 25 bit
--		0.7: 02.07.2013: bugfix, got the 9,13 --> 9,14 conversion right, finally !
--		0.8: 11.07.2013: dt and norm_to_s selectable, bigger memory 128 
--		0.9: 11.07.2013: supports now even bigger memory, 256x24
--		1.0: 24.09.2013: new concept, uses sequential multiplier for final multiplication, doesn't need corrector anymore
--							simulated, works
--		1.1: 09.12.2013: Supports save restart if adc had been powercycled
--		2.0: 17.08.2018: Parametrized vector length, ram instantiatied by synplify, normalization by bitshifts (dt = 15.625ms etc), remove startup filter
--		2.1: 03.09.2018: First dt dout samples are 0
--		3.0: 11.03.2018: dblascos: Using now a circular buffer that does not need to shift back all the samples everytime. Complexity and time usage reduced drastically.
--  
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
--
--!@brief calculates the differential of the input signal, dt in ADC sample times
--	
--	
--
--
-- Targeted device: Actel ProAsic3, IGLOO2
-- Author: Jens Steckert
--
----------------------------------------------------------------------------------------------------------
entity differentiator_synram is 
generic(arr_depth : integer range 7 to 4096;
		d_width	: integer range 15 to 32);
port (
	clk, rst 			: in std_logic;
	din 				: in std_logic_vector(d_width -1 downto 0); 		--input current s(15,16)
	dready 				: in std_logic;
	dt 					: in std_logic_vector((log2c(arr_depth)) downto 0);	--# of samples forming the dt window
	normalizing_shifts 	: in std_logic_vector(7 downto 0);		--# of bits to shift for normalization
	dout 				: out std_logic_vector(d_width -1 downto 0); 	--s(15,16) (keep the same as current)
	done				: out std_logic
	
);
end differentiator_synram;
--------------------------------------------------------------------------------------
--
--		Numeric concept
--
--		Values from input channel (current) arrive at <din> with s(15,16) or s(14,17)

--		didt array and calculations are done in s(10,9)
--		after corrector, the values are cropped to s(9,14) 
--		--> didt values should not exceed 512A/s ;-)
--		if no corrector is used, value will be shifted and zero-padded
--
---------------------------------------------------------------------------------------

architecture differentiator_arch of differentiator_synram is




type state_type is (idle, refresh, calc, scale, completed, correct, normalize);
signal state : state_type;
signal din_i : signed(d_width -1 downto 0);
signal adr  : integer range 0 to (arr_depth +1);
signal dready_pulse, dready_old : std_logic;
signal didt_int : signed(d_width downto 0);
signal didt_norm : signed(d_width downto 0);

--signal ram_wadr, ram_radr : integer range 0 to (arr_depth +1);
--signal ram_wdata : std_logic_vector(d_width-1 downto 0);
signal ram_maxadr : integer range 0 to arr_depth;





--arrays for automatic ram instantiation
type ram_array is array(0 to arr_depth -1) of std_logic_vector(d_width -1 downto 0);
signal ram0 : ram_array;
--signal ram1 : ram_array;
--signal ram2 : ram_array;

attribute syn_ramstyle : string;
-- attribute syn_ramstyle of ram0 : signal is "block_ram";
-- attribute syn_ramstyle of ram1 : signal is "block_ram";
-- attribute syn_ramstyle of ram2 : signal is "block_ram";
attribute syn_ramstyle of ram0 : signal is "no_rw_check";
--attribute syn_ramstyle of ram1 : signal is "no_rw_check";
--attribute syn_ramstyle of ram2 : signal is "no_rw_check";

attribute syn_preserve : boolean;
attribute syn_preserve of ram0: signal is true;
--attribute syn_preserve of ram1: signal is true;
--attribute syn_preserve of ram2: signal is true;

signal ram_read_reg : std_logic_vector(d_width-1 downto 0);


begin

ram_maxadr <= to_integer(unsigned(dt));


pulser: process(clk)
begin
if rising_edge(clk) then 
	if rst = '1' then
		dready_pulse <= '0';
		dready_old <= '0';
	else
		dready_old <= dready;
		if dready_old < dready then
			--rising edge
			dready_pulse <= '1';
			din_i <= signed(din);
		else
			din_i <= din_i;
			dready_pulse <= '0';
		end if;
	end if;end if;
end process;


-------------------------------------Algorithm----------------------------------
--
--  Set address to max address
--  Reads value of adress n-1, writes that value to address n
--  Decrement adress 
--  Continues unitl n+1 = max addr
--  Writes incoming value (din) to address 0
--
---------------------------------------------------------------------------------

calculator: process(clk)
	variable sample_counter : integer range 0 to arr_depth := 0;
begin
if rising_edge(clk) then 
	if rst= '1' then
		state <= idle;
		didt_int <= (others => '0');
		
       
--        ram_radr <= 0;
--        ram_wadr <= 0;
       
--        ram_wdata  <=  (others => '0');
        adr <= 0;
        dout <= (others => '0');
		done <= '0';
		
		sample_counter  := 0;
		
	else
		case state is
			
			when idle =>
				done <= '0';
				
				if dready_pulse = '1' then
					state <= refresh;
				else
					state <= idle;
				end if;
				
			when refresh =>
	            -- write new value and read oldest value
				ram0(adr) <= std_logic_vector(din_i);
				adr <= adr + 1;
				if (adr<ram_maxadr-1) then
					ram_read_reg <= ram0(adr+1);
				elsif (adr=ram_maxadr-1) then
					ram_read_reg <= ram0(0);
					adr <= 0;
				end if;
								
				--set read address to the oldest data point (max adr)
				state <= calc;
            
			when calc =>
				--sign-extend & subtract oldest value in array from input data
				--didt int'length = d_width +1				
                didt_int <= signed(din_i(din_i'left) & din_i) - signed(ram_read_reg(ram_read_reg'left) & ram_read_reg);
				
				state <= normalize;
			when normalize =>
				didt_norm <= shift_left(didt_int, to_integer(unsigned(normalizing_shifts)));
				
				state <= completed;
				
			when completed =>
				
				if(sample_counter=arr_depth) then
				
					--check if negative
					if didt_norm(didt_norm'left) = '1' then
						--check if not clipping
						if didt_norm(didt_norm'left -1) = '1' then
							dout <= std_logic_vector(didt_norm(didt_norm'left -1 downto 0));
						else
							--signal is clipping negative, set to negative full scale
							dout <=(dout'left => '1', others => '0') ;
						end if;
					else
					--signal is positive, check if not clipping
						if didt_norm(didt_norm'left -1) = '0' then
							dout <= std_logic_vector(didt_norm(didt_norm'left-1 downto 0));
						else
							--clip to fs pos
							dout <=(dout'left => '0', others => '1') ;

						end if;
					end if;
					
				else
					dout <= (others => '0');
					sample_counter := sample_counter + 1;
				end if;
								
				
				
				done <= '1';
				
				state <= idle;
			when others =>
				state <= idle;
		end case;
	end if;end if;
end process;

end;	