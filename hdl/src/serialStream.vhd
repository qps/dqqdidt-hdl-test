--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: data_aqq_UART_LTADC.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--		  V A1.0				 : 10.11.2016 : Rewriting serial entity
-- Description: 
--
-- Data acquisition on ch0, ch1, ch2 and ch3 for LT ADC svia UART - implementation 
-- based on the implementation of data acquisition for MAX ADC
--
-- Targeted device: <Family::IGLOO2> <Die::M2GL060> <Package::484 FBGA>
-- Author: Josef Kopal <Josef.Kopal@cern.ch>
--
--------------------------------------------------------------------------------

library IEEE;

USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
library UQDSlib;
use UQDSlib.UQDSLib.ALL;

entity serialStream is
	generic(
		channels : integer range 1 to 16 := 1
	);
	port(
		clk      : in  std_logic;
		reset    : in  std_logic;
		uart_rx  : in  std_logic;
		uart_tx  : out std_logic;
		hartBeat : in  std_logic;
		DataIn   : in  channelsArray(channels - 1 downto 0);
		--DataIn2  : in  std_logic_vector(19 downto 0);
		--		DataIn3  : in  std_logic_vector(31 downto 0);
		--		DataIn4  : in  std_logic_vector(19 downto 0);
		--		DataIn5  : in  std_logic_vector(19 downto 0);
		--		DataIn6  : in  std_logic_vector(19 downto 0);
		--statreg  : in  std_logic_vector(7 downto 0);
		--debugSig		:		out std_logic_vector(3 downto 0)
		debugSig : out std_logic_vector(7 downto 0)
	);
end serialStream;

architecture arch of serialStream is
	component top_UART is
		port(
			RESET_N           : in  std_logic;
			CLK               : in  std_logic;
			WEN               : in  std_logic;
			OEN               : in  std_logic;
			CSN               : in  std_logic;
			DATA_IN           : in  std_logic_vector(7 downto 0);
			RX                : in  std_logic;
			BAUD_VAL          : in  std_logic_vector(12 downto 0);
			BIT8              : in  std_logic; --   IF SET TO ONE 8 DATA BITS OTHERWISE 7 DATA BITS
			PARITY_EN         : in  std_logic; --   IF SET TO ONE PARITY IS ENABLED OTHERWISE DISABLED
			ODD_N_EVEN        : in  std_logic; --   IF SET TO ONE ODD PARITY OTHERWISE EVEN PARITY
			BAUD_VAL_FRACTION : in  std_logic_vector(2 downto 0); --USED TO ADD EXTRA PRECISION TO BAUD VALUE WHEN BAUD_VAL_FRCTN_EN = 1
			PARITY_ERR        : out std_logic; --   PARITY ERROR INDICATOR ON RECIEVED DATA
			OVERFLOW          : out std_logic; --   RECEIVER OVERFLOW
			TXRDY             : out std_logic; --   TRANSMIT READY FOR ANOTHER BYTE
			RXRDY             : out std_logic; --   RECEIVER HAS A BYTE READY
			DATA_OUT          : out std_logic_vector(7 downto 0);
			TX                : out std_logic;
			FRAMING_ERR       : out std_logic
		);
	end component;

	constant buffersize : natural := 7 + channels * 4;
	type states is (SENDING, SAMPLEHOLD, WAITFORTXRDY, SENDBYTE, IDLE, DONE, ERROR);
	type txBuffer is array (buffersize downto 0) of std_logic_vector(7 downto 0);

	signal current_state : states;
	signal next_state    : states;
	signal data_buffer   : txBuffer;
	signal byte_cnt      : integer range 0 to buffersize; --buffer size +1 for check sum
	signal byte_cnt_load : std_logic;
	signal byte_buffer   : std_logic_vector(7 downto 0);

	signal test_data : std_logic_vector(7 downto 0) := x"33";
	signal data_rx   : std_logic_vector(7 downto 0);

	signal write_enable : std_logic;
	signal txrdy        : std_logic;

	signal output_enable : std_logic;
	signal rxrdy         : std_logic;

	signal not_reset : std_logic;

	signal sample_cnt     : integer range 0 to integer'high;
	signal sample_cnt_reg : std_logic_vector(31 downto 0);

	signal hartBeatReg : std_logic;

begin

	FSMClock : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				current_state <= IDLE;

			else

				current_state <= next_state;
				hartBeatReg   <= hartBeat;
			end if;
		end if;
	end process;

	SampleHoldCurrentValues : process(clk)
	begin
		if clk'event and clk = '1' then
			if current_state = SAMPLEHOLD then
				--header
				data_buffer(0) <= x"AA";
				data_buffer(1) <= x"BB";
				data_buffer(2) <= x"CC";
				--header
				data_buffer(3) <= sample_cnt_reg(31 downto 24);
				data_buffer(4) <= sample_cnt_reg(23 downto 16);
				data_buffer(5) <= sample_cnt_reg(15 downto 8);
				data_buffer(6) <= sample_cnt_reg(7 downto 0);

				--				data_buffer(7) <= x"0" & DataIn1(19 downto 16);
				--				data_buffer(8) <= DataIn1(15 downto 8);
				--				data_buffer(9) <= DataIn1(7 downto 0);

				for i in 0 to channels - 1 loop
					data_buffer(7 + 4*i)     <= DataIn(i)(31 downto 24);
					data_buffer(7 + 4*i + 1) <= DataIn(i)(23 downto 16);
					data_buffer(7 + 4*i + 2) <= DataIn(i)(15 downto 8);
					data_buffer(7 + 4*i + 3) <= DataIn(i)(7 downto 0);
				end loop;

				--				data_buffer(10) <= x"0" & DataIn2(19 downto 16);
				--				data_buffer(11) <= DataIn2(15 downto 8);
				--				data_buffer(12) <= DataIn2(7 downto 0);

				--				data_buffer(14) <= DataIn3(31 downto 24);
				--				data_buffer(15) <= DataIn3(23 downto 16);
				--				data_buffer(16) <= DataIn3(15 downto 8);
				--				data_buffer(17) <= DataIn3(7 downto 0);
				--
				--				data_buffer(18) <= x"0" & DataIn3(19 downto 16);
				--				data_buffer(19) <= DataIn3(15 downto 8);
				--				data_buffer(20) <= DataIn3(7 downto 0);

				--				data_buffer(16) <= x"0" & DataIn4(19 downto 16);
				--				data_buffer(17) <= DataIn4(15 downto 8);
				--				data_buffer(18) <= DataIn4(7 downto 0);
				--
				--				data_buffer(19) <= x"0" & DataIn5(19 downto 16);
				--				data_buffer(20) <= DataIn5(15 downto 8);
				--				data_buffer(21) <= DataIn5(7 downto 0);

				--				data_buffer(22) <= x"0" & DataIn6(19 downto 16);
				--				data_buffer(23) <= DataIn6(15 downto 8);
				--				data_buffer(24) <= DataIn6(7 downto 0);

				--								data_buffer(25)<=statreg(7 downto 0);
				--								data_buffer(26)<=statreg(7 downto 0);
				data_buffer(buffersize) <= x"00";
			elsif write_enable = '0' then
				data_buffer(buffersize) <= data_buffer(buffersize) xor byte_buffer;
			end if;
		end if;
	end process;

	main_fsm : process(all)
	begin
		case current_state is

			when IDLE =>
				if hartBeat = '1' then
					next_state <= SAMPLEHOLD;
				else
					next_state <= IDLE;
				end if;

			when SAMPLEHOLD =>
				next_state <= WAITFORTXRDY;
			when WAITFORTXRDY =>
				if txrdy = '0' then
					next_state <= WAITFORTXRDY;
				else
					next_state <= SENDBYTE;
				end if;

			when SENDBYTE =>
				if byte_cnt >= buffersize then
					next_state <= DONE;
				else
					next_state <= WAITFORTXRDY;
				end if;

			when DONE =>
				next_state <= IDLE;

			when ERROR =>
				next_state <= ERROR;
			when others =>
				next_state <= ERROR;
		end case;
	end process;
	--(SENDING, SAMPLEHOLD, WAITFORTXRDY, IDLE, DONE, ERROR)
	FSMSignals : process(all)
	begin
		case current_state is
			when IDLE =>
				write_enable  <= '1';
				byte_buffer   <= x"00";
				byte_cnt_load <= '0';
				debugSig      <= x"0F";
			when SAMPLEHOLD =>
				write_enable  <= '1';
				byte_buffer   <= x"00";
				byte_cnt_load <= '1';
				debugSig      <= x"01";
			when WAITFORTXRDY =>
				write_enable  <= '1';
				byte_buffer   <= data_buffer(byte_cnt);
				byte_cnt_load <= '0';
				debugSig      <= x"02";
			when SENDBYTE =>
				write_enable  <= '0';
				byte_buffer   <= data_buffer(byte_cnt);
				byte_cnt_load <= '0';
				debugSig      <= x"03";
			when DONE =>
				write_enable  <= '1';
				byte_buffer   <= x"00";
				byte_cnt_load <= '0';
				debugSig      <= x"04";
			when ERROR =>
				write_enable  <= '1';
				byte_buffer   <= x"00";
				byte_cnt_load <= '0';
				debugSig      <= x"05";
			when others =>
				write_enable  <= '1';
				byte_buffer   <= x"00";
				byte_cnt_load <= '0';
				debugSig      <= x"06";
		end case;
	end process;

	sample_counter : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				sample_cnt <= 0;
			else
				if sample_cnt = integer'high then
					sample_cnt <= 0;
				else
					sample_cnt <= sample_cnt + 1;
				end if;
				sample_cnt_reg <= std_logic_vector(to_unsigned(sample_cnt, 32));
			end if;
		end if;
	end process;

	byte_counter : process(all)
	begin
		if clk'event and clk = '1' then
			if reset = '1' then
				byte_cnt <= 0;
			else

				if byte_cnt_load = '1' then
					byte_cnt <= 0;
				elsif write_enable = '0' then
					byte_cnt <= byte_cnt + 1;
				end if;
			end if;
		end if;
	end process;

	not_reset <= not reset;

	uart_slave : top_UART
		PORT MAP(
			RESET_N           => not_reset,
			CLK               => clk,
			WEN               => write_enable,
			OEN               => '0',
			CSN               => '0',
			DATA_IN           => byte_buffer,
			RX                => uart_rx,
			--BAUD_VAL          => '0' & x"015", --115200
			BAUD_VAL          => '0' & x"001", --921600 and baud_val_fraction
			BIT8              => '1',
			PARITY_EN         => '0',
			ODD_N_EVEN        => '0',
			--BAUD_VAL_FRACTION => "000",
			BAUD_VAL_FRACTION => "110", -- fraction for 921600
			PARITY_ERR        => open,
			OVERFLOW          => open,
			TXRDY             => txrdy,
			RXRDY             => open,
			DATA_OUT          => open,
			TX                => uart_tx,
			FRAMING_ERR       => open
		);

end arch;
