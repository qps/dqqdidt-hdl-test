library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;

LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;

-------------------------------Description------------------------------------------
--Scaler by coefficient - generic for both channels
--Function: signed multiplication by coefficient of width d_difth_factor and
--			reduction of resulted word to d_width_out
-- 
---------------------------------Versions-------------------------------------------
-- 8.09.2014 - A.Skoczen
-- 15.02.2018 - J.Steckert 	version with fixed scaling factors for uQDS
-- 21.09.2018 - J.Steckert	added conversion factor to convert transducer signals
-- 							to currents or didt directly
-- 7.3.19 - D. Blasco 		added the possibility to put any value directly
-- 15.07.2019 - J.Steckert 	added overrange check on raw value and saturation to full scale
-- 21.08.2019 - J.Steckert  added output overrange (if it clips) indicator						
------------------------------------------------------------------------------------
entity scaler is
	generic(d_width_in     : integer range 15 to 32 := 20;
	        d_width_out    : integer range 15 to 32 := 32;
	        d_width_factor : integer range 2 to 32  := 21);
	port(clk, rst, str     : in  std_logic;
	     gainsel           : in  std_logic_vector(3 downto 0);
	     transducer_factor : in  std_logic_vector(3 downto 0);
	     resistor_divider  : in  std_logic_vector(0 downto 0);
	     scaler_value      : in  std_logic_vector(d_width_factor - 2 downto 0);
	     direct_mode       : in  std_logic;
	     din               : in  std_logic_vector(d_width_in - 1 downto 0);
	     dout              : out std_logic_vector(d_width_out - 1 downto 0);
	     input_overrange   : out std_logic;
	     output_overrange  : out std_logic;
	     ready             : out std_logic
	    );
end scaler;

architecture arch of scaler is

	constant d_width_res : integer range 5 to 65 := d_width_in + d_width_factor - 2;
	signal input         : std_logic_vector(d_width_in - 1 downto 0);
	signal result        : std_logic_vector(d_width_res downto 0);
	signal start, ad     : std_logic;
	signal coeff_hardc   : std_logic_vector(d_width_factor - 1 downto 0);
	signal coeff         : std_logic_vector(d_width_factor - 1 downto 0);

	signal combined_factor : std_logic_vector(8 downto 0);
	signal saturation_threshold_raw : std_logic_vector(d_width_in -1 downto 0);
	signal scaled 			: std_logic_vector(d_width_out -1 downto 0);
	signal ip_overrange_logic, op_overrange_logic : std_logic_vector(3 downto 0);
	signal MSBs			: std_logic_vector(1 downto 0);
	
	component multS is
		generic(W1, W2 : natural; omode : std_logic);
		port(clk, rst, start : in  std_logic;
		     multiplier      : in  std_logic_vector(W1 - 1 downto 0);
		     multiplicand    : in  std_logic_vector(W2 - 1 downto 0);
		     done            : out std_logic;
		     product         : out std_logic_vector(W1 + W2 - 2 downto 0));
	end component;

begin

	posedge : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				start <= '0';
				ad    <= '0';
			else
				ad    <= str;
				start <= not ad and str;
			end if;
		end if;
	end process;

	combined_factor <= resistor_divider & transducer_factor & gainsel;
	-------------------------------------------------------------------------------------
	--selection of proper scaling factors according to input channel configuration
	--
	--scaling factors selected for s(7,24) target format LSB = 2^-24 = 59.604645e-9
	--If tansducer factor is non 0 then output format switches to s(15,16) in unit A
	with combined_factor select coeff_hardc <=
		"0" & x"19000" when "000000000", --g=1		LSB= 47.683716 uV 
		"0" & x"02C71" when "000000001", --g=9		LSB= 5.298191 uV            
        "0" & x"008E3" when "000000010", --g=45		LSB= 1.059638 uV  
        "0" & x"000E3" when "000000011", --g=450	LSB= 105.9638 nV  
        -- "0" & x"7D000" when "000000100", --g=0.2	LSB= 238.41858 uV
        -- Channel with 1/6 divider
        "0" & x"96000" when "100000000", --g=1		LSB= 286.10230 uV 
        "0" & x"10AA6" when "100000001", --g=9		LSB= 31.7891 uV              
        "0" & x"03552" when "100000010", --g=45		LSB= 6.3578 uV  
        "0" & x"00552" when "100000011", --g=450	LSB= 635.7828 nV  
		--conversion factors for transducers
			"0" & x"05DC0" when "000010000", --g=1, transducer with 60A/V
			"0" & x"06D60" when "000100000", --g=1, transducer with 70A/V
			"0" & x"0EA60" when "000110000", --g=1, transducer with 150A/V
			"0" & x"927C0" when "001000000", --g=1, transducer with 1500A/V
			"0" & x"C3500" when "001010000", --g=1, transducer with 2000A/V
			"0" & x"1B207" when "001100000", --g=1, transducer with 277.777A/Vs (didt sensor one coil)
			"1" & x"0F446" when "001110000", --g=1, transducer with 2777A/Vs -->  720uVs/A #not possible with MSB 0
			"0" & x"03039" when "001100001", --g=9, transducer with 277.77 A/Vs		
			"0" & x"1E240" when "001110001", --g=9, transducer with 2777A/Vs -->  720uVs/A
			"0" & x"19000" when others;

	-- Multiplexer to choose from hardcodede values or direct value
	coeff <= "0" & scaler_value when direct_mode = '1' else coeff_hardc;

	input <= std_logic_vector(din);

	mul : multS
		generic map(W1 => d_width_factor, W2 => d_width_in, omode => '1')
		port map(clk          => clk, rst => rst, start => start,
		         multiplier   => coeff, multiplicand => input,
		         done         => ready, product => result);
	
	--extraction of the d_width_out most-significant bits
	--dout <= result(d_width_res - 1 downto (d_width_res - d_width_out));
	MSBs <= result(result'left downto result'left -1);
	
	--clip results in case they're out of rangen & coerce to range
	with MSBs select dout <= 
		--positive, not clipping
		result(d_width_res - 1 downto (d_width_res - d_width_out)) 	when "00",
		--positive, clip to fs
		(dout'left => '0', others => '1') 							when "01",
		--negative, not clipping
		result(d_width_res - 1 downto (d_width_res - d_width_out))	when "11",
		--negative, clipping to -fs
		(dout'left => '1', others => '0')							when "10",
		--Other impossible cases
		(dout'left => '0', others => '0')							when others;
	
	--set over range indicator when clipping
	-- with MSBs select output_overrange <= 
		-- --positive, not clipping
		-- '0' 	when "00",
		-- --positive, clip to fs
		-- '1' 	when "01",
		-- --negative, not clipping
		-- '0'		when "11",
		-- --negative, clipping to -fs
		-- '1'		when "10",
		-- --Other impossible cases
		-- '0'		when others;
		
	--valid for all gen 7 channels (can be different for other channels)
	saturation_threshold_raw <= x"73334";
	
	--check whether ADC raw value is within sensitive range of +/-22.5V at g1
range_detector : qlogic_comp_simple 
	generic map(d_width => d_width_in)
	port map (clk => clk, rst => rst,
		U_in => din, 
		T_in => saturation_threshold_raw, 
		disc => x"0064", --set discriminator to 10ms
		L_out => ip_overrange_logic);
	--propagate overrange bit	
	input_overrange <= ip_overrange_logic(0);
	
output_range_detector : qlogic_comp_simple
	generic map(d_width => 16)
	port map (clk => clk, rst => rst,
		U_in => ("000000000000000" & (MSBs(0) xor MSBs(1))),
		T_in => (others=>'0'), 
		disc => x"0064", --set discriminator to 10ms
		L_out => op_overrange_logic);
	output_overrange <= op_overrange_logic(0);
	
	--extraction of the d_width_out most-significant bits
	--scaled <= result(d_width_res - 1 downto (d_width_res - d_width_out));
	--dout <= scaled;
	--For each gain/transducer setting calculate the effective sensitive input range
	--Set the threshold of the sensitive range detector accordingly
	 
	--with combined_factor select saturation_threshold <=
	--	x"16800000" when "000000000", --g=1		LSB= 47.683716 uV, 	max sensitive range = +/-22.5V 
	--	x"02800000" when "000000001", --g=9		LSB= 5.298191 uV    max sensitive range = +/-2.5V        
    --  x"00800000" when "000000010", --g=45	LSB= 1.059638 uV  	max sensitive range = +/-0.5V
    --    x"000CCCCD" when "000000011", --g=450	LSB= 105.9638 nV  	max sensitive range = +/-0.05V
    --    -- "0" & x"7D000" when "000000100", --g=0.2	LSB= 238.41858 uV
    --    -- Channel with 1/6 divider
    --    x"7FFFFFFF" when "100000000", --g=1		LSB= 286.10230 uV 	max sensitive range = +/-135V (beyond numeric range)
    --    x"0F000000" when "100000001", --g=9		LSB= 31.7891 uV     max sensitive range = +/-15V         
    --    x"03000000" when "100000010", --g=45	LSB= 6.3578 uV		max sensitive range = +/-3V  
    --    x"004CCCCC" when "100000011", --g=450	LSB= 635.7828 nV  	max sensitive range = +/-0.3V
		--conversion factors for transducers
	--	x"06270000" when "000010000", 	 --g=1, transducer with 70A/V 		max sensitive range = +/-1575A
	--	x"0D2F0000" when "000100000", 	--g=1, transducer with 150A/V		max sensitive range = +/-3375A
	--	x"7FFFFFFF" when "000110000", --g=1, transducer with 1500A/V		max sensitive range = +/-33750A (beyond num. range)
	--	x"7FFFFFFF" when "001000000", --g=1, transducer with 2000A/V		max sensitive range = +/-45000A (beyond num. range)
	--	x"186A0000" when "001010000", --g=1, transducer with 277.777A/Vs (didt sensor one coil) max sensitive range = +/-6250A
	--	x"61A7FF5C" when "001100000", --g=1, transducer with 1111.11A/Vs (didt sensor one coil 1/4 current) max sensitive range = +/-25000A
	--	x"7FFFFFFF" when "001110000", --g=1, transducer with 2777A/Vs -->  720uVs/A max sensitive range = +/-62482A (beyond num. range)
	--	x"1B1E8000" when "001110001", --g=9, transducer with 2777A/Vs -->  720uVs/A max sensitive range = +/-6943A
	--	x"02B67147" when "001010001", --g=9, transducer with 277.77 A/Vs			 
	--	x"16800000" when others;
	
	

	
end;
