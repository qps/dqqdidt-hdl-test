LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;

--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: sram_tester.vhd
-- File history:
--      0.1: 27.11.2018: first version
--    
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- Tests all data and addr lines of an SRAM module. Uses cy_sram_interface entity to communicate with SRAM
--
--
-- Targeted device: IGLOO2
-- Author: Daniel Blasco
-- 
--------------------------------------------------------------------------------

entity sram_tester is
	generic(
		addr_width : natural := 20;
		data_width : natural := 16
	);
	port(
		clk       : in  std_logic;
		rst       : in  std_logic;
		start     : in  std_logic;
		flags     : out std_logic_vector(2 downto 0); -- [error found, test finished, test started]
		cy_adr_in : out std_logic_vector(addr_width - 1 downto 0);
		cy_rdata  : in  std_logic_vector(data_width - 1 downto 0);
		cy_wdata  : out std_logic_vector(data_width - 1 downto 0);
		cy_r_done : in  std_logic;
		cy_w_done : in  std_logic;
		cy_cmd    : out std_logic_vector(1 downto 0)
	);
end sram_tester;

architecture sram_tester_arch of sram_tester is

	type states is (IDLE,
	                TEST_DATA_LINES_WRITE,
	                TEST_DATA_LINES_WRITE_WAIT0,
	                TEST_DATA_LINES_WRITE_WAIT1,
	                TEST_DATA_LINES_READ,
	                TEST_DATA_LINES_READ_WAIT,
	                TEST_ADDR_LINES_WRITE_FIRST_PATTERN,
	                TEST_ADDR_LINES_WRITE_FIRST_PATTERN_WAIT0,
	                TEST_ADDR_LINES_WRITE_FIRST_PATTERN_WAIT1,
	                TEST_ADDR_LINES_WRITE_SECOND_PATTERN,
	                TEST_ADDR_LINES_WRITE_SECOND_PATTERN_WAIT0,
	                TEST_ADDR_LINES_WRITE_SECOND_PATTERN_WAIT1,
	                TEST_ADDR_LINES_READ_SECOND_PATTERN,
	                TEST_ADDR_LINES_READ_SECOND_PATTERN_WAIT,
	                FINISH
	               );
	signal state             : states;
												  
	signal counter, counter2 : integer range 0 to addr_width;

begin

	FSM : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				cy_adr_in <= (others => '0');
				cy_wdata  <= (others => '0');
				state     <= IDLE;
				counter   <= 0;
				counter2  <= 0;
				flags     <= "000";
			else

				case state is

					when IDLE =>        -- Wait until start flag is issued
						cy_adr_in <= (others => '0');
						cy_wdata  <= (others => '0');
						counter   <= 0;
					  
						counter2  <= 0;
						flags <= "000";
						if start = '1' then
							state <= TEST_DATA_LINES_WRITE;
						else
							state <= IDLE;
						end if;

					when TEST_DATA_LINES_WRITE => -- Write a 1 in one data line using address 0
						flags(0)          <= '1';
						cy_adr_in         <= (others => '0');
						cy_wdata          <= (others => '0');
						cy_wdata(counter) <= '1';
						cy_cmd            <= "10"; --write to memory
						state             <= TEST_DATA_LINES_WRITE_WAIT0;

					when TEST_DATA_LINES_WRITE_WAIT0 =>
						state <= TEST_DATA_LINES_WRITE_WAIT1;

					when TEST_DATA_LINES_WRITE_WAIT1 =>
						cy_cmd <= "00"; --set mem idle
						if cy_w_done = '1' then
							state <= TEST_DATA_LINES_READ;
						else
							state <= TEST_DATA_LINES_WRITE_WAIT1;
						end if;

					when TEST_DATA_LINES_READ => -- Read back the address 0
						cy_adr_in <= (others => '0');
						cy_cmd    <= "01"; --read ram
						state     <= TEST_DATA_LINES_READ_WAIT;

					when TEST_DATA_LINES_READ_WAIT => -- Check that the 1 was correctly written. Once all lines are checked, go to phase 2
						cy_cmd <= "00"; --set ram back to idle
						if cy_r_done = '1' then
							if (cy_rdata /= cy_wdata) then
								flags(2) <= '1';
							end if;
							if counter >= data_width - 1 then
								counter <= 0;
								state   <= TEST_ADDR_LINES_WRITE_FIRST_PATTERN;
							else
								counter <= counter + 1;
								state   <= TEST_DATA_LINES_WRITE;
							end if;
						else
							state <= TEST_DATA_LINES_READ_WAIT;
						end if;

					when TEST_ADDR_LINES_WRITE_FIRST_PATTERN => -- Write a pattern in the address with one line set to '1'
						cy_adr_in          <= (others => '0');
						cy_adr_in(counter) <= '1';
						cy_wdata           <= std_logic_vector(to_unsigned(counter, data_width));
						cy_cmd             <= "10"; --write to memory
						state              <= TEST_ADDR_LINES_WRITE_FIRST_PATTERN_WAIT0;

					when TEST_ADDR_LINES_WRITE_FIRST_PATTERN_WAIT0 =>
						state <= TEST_ADDR_LINES_WRITE_FIRST_PATTERN_WAIT1;

					when TEST_ADDR_LINES_WRITE_FIRST_PATTERN_WAIT1 => -- Once all addresses with a line set to '1' are written go to next phase
						cy_cmd <= "00"; --set mem idle
						if cy_w_done = '1' then
							if counter >= addr_width - 1 then
								counter <= 0;
								state   <= TEST_ADDR_LINES_WRITE_SECOND_PATTERN;
							else
								counter <= counter + 1;
								state   <= TEST_ADDR_LINES_WRITE_FIRST_PATTERN;
							end if;
						else
							state <= TEST_ADDR_LINES_WRITE_FIRST_PATTERN_WAIT1;
						end if;

					when TEST_ADDR_LINES_WRITE_SECOND_PATTERN => -- Rewrite a different pattern in the address
						cy_adr_in          <= (others => '0');
						cy_adr_in(counter) <= '1';
						cy_wdata           <= not std_logic_vector(to_unsigned(counter, data_width));
						cy_cmd             <= "10"; --write to memory
						state              <= TEST_ADDR_LINES_WRITE_SECOND_PATTERN_WAIT0;

					when TEST_ADDR_LINES_WRITE_SECOND_PATTERN_WAIT0 =>
						state <= TEST_ADDR_LINES_WRITE_SECOND_PATTERN_WAIT1;

					when TEST_ADDR_LINES_WRITE_SECOND_PATTERN_WAIT1 =>
						cy_cmd <= "00"; --set mem idle
						if cy_w_done = '1' then
							state <= TEST_ADDR_LINES_READ_SECOND_PATTERN;
						else
							state <= TEST_ADDR_LINES_WRITE_SECOND_PATTERN_WAIT1;
						end if;

					when TEST_ADDR_LINES_READ_SECOND_PATTERN => -- Read back each address every time
						cy_adr_in           <= (others => '0');
						cy_adr_in(counter2) <= '1';
						cy_cmd              <= "01"; --read ram
						state               <= TEST_ADDR_LINES_READ_SECOND_PATTERN_WAIT;

					when TEST_ADDR_LINES_READ_SECOND_PATTERN_WAIT => -- Check that the pattern is the first or second. Then change pattern in the next address, until finished.
						cy_cmd <= "00"; --set ram back to idle
						if cy_r_done = '1' then
							if (counter2 > counter) then
								if (cy_rdata /= std_logic_vector(to_unsigned(counter2, data_width))) then
									flags(2) <= '1';
								end if;
							else
								if (cy_rdata /= not std_logic_vector(to_unsigned(counter2, data_width))) then
									flags(2) <= '1';
								end if;
							end if;

							if counter2 >= addr_width - 1 then
								counter2 <= 0;
								if counter >= addr_width - 1 then
									counter <= 0;
									state   <= FINISH;
								else
									counter <= counter + 1;
									state   <= TEST_ADDR_LINES_WRITE_SECOND_PATTERN;
								end if;
							else
								counter2 <= counter2 + 1;
								state    <= TEST_ADDR_LINES_READ_SECOND_PATTERN;
							end if;

						else
							state <= TEST_ADDR_LINES_READ_SECOND_PATTERN_WAIT;
						end if;

					when FINISH =>
						flags(1) <= '1';
						state    <= IDLE;

				end case;
			end if;
		end if;
	end process;

end;
