--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: ucom_v2_0.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--		  V A1.0 			 :	10.11.2016 : modified version for the altera FPGA
--			2.0 : 13.2.2018 :  added SPI communication (dblasco)
--
-- Description: 
-- Register control block used for a configuration of the device via RS485 interface, 
-- it decodes address, and command from the packet and propagates the data to upper 
-- entity together with read/write strobe and register address
--
-- 
--
-- Targeted device: originally (<Family::IGLOO2> <Die::M2GL060> <Package::484 FBGA>)
-- Targeted device: Altera Cyclone V -- terrasic DE0-nano-SOC

-- Author: Josef Kopal <wilddreams@gmail.com> Dani Blasco
-- 
--
--------------------------------------------------------------------------------
LIBRARY IEEE;
LIBRARY UQDSLib;

USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.all;
USE UQDSLib.UQDSLib.ALL;

ENTITY ucom_v2_0 IS
	PORT(
		-- RS485 and FTDI
		uartComTX_FTDI  : OUT STD_LOGIC;
		uartComRX_FTDI  : IN  STD_LOGIC;
		uartComTX_RS485  : OUT STD_LOGIC;
		uartComRX_RS485  : IN  STD_LOGIC;
		--SPI
		spi_csn    : in  std_logic;
		spi_sclk   : in  std_logic;
		spi_sdin   : in  std_logic;
		spi_sdout  : out std_logic;
		--Registes
		--deviceAddress : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
		regAddress : OUT STD_LOGIC_VECTOR(REGISTER_ADR_WIDTH - 1 downto 0);
		DataIn     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		regWRsig   : OUT STD_LOGIC_VECTOR(2**REGISTER_ADR_WIDTH - 1 downto 0);
		DataOut    : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
		regRDSig   : OUT STD_LOGIC_VECTOR(2**REGISTER_ADR_WIDTH - 1 downto 0);
		-- standard
		clk        : IN  STD_LOGIC;
		nrst       : IN  STD_LOGIC;
		rstOut     : OUT STD_LOGIC
	);
END ucom_v2_0;

ARCHITECTURE behav OF ucom_v2_0 IS
	CONSTANT BUFFERSIZE      : NATURAL := 7;
	CONSTANT WATCHDOGTIMEOUT : INTEGER := 1000000;
	-- definition of the TX/RX buffer type
	TYPE serBuff IS ARRAY (BUFFERSIZE DOWNTO 0) OF STD_LOGIC_VECTOR(7 DOWNTO 0);
	-- receiver buffer
	SIGNAL serBuffRX         : serBuff;
	-- control check sum for the receiving buffer 
	SIGNAL serBuffRXSum      : STD_LOGIC_VECTOR(7 DOWNTO 0);
	-- counter of received bytes
	SIGNAL cntByteRx         : INTEGER RANGE 0 TO BUFFERSIZE;
	-- watch dog counter for incomplete message detection
	SIGNAL cntWatchDog       : INTEGER;

	-- transmitter buffer
	SIGNAL serBuffTX                        : serBuff;
	-- control checksum of the transmitting buffer
	SIGNAL serBuffTXSum                     : STD_LOGIC_VECTOR(7 DOWNTO 0);
	-- transmitting byte counter
	SIGNAL cntByteTX                        : INTEGER RANGE 0 TO BUFFERSIZE;
	-- main control FSM	
	-- this fsm identify and check incoming packet for the command CRC and decode command.
	--	Once correct packet is received it starts processing FSM to process the data
	TYPE FSMReceiveMessage IS (IDLE, WAIT4BYTE, READBYTE, CHECKSUM, DECODE, PROCESSMESSAGE, MESSAGEERROR, WAITCYCLE);
	SIGNAL FSMRX, FSMRXNext                 : FSMReceiveMessage;
	--Message processing subFSM for the main control FSM to split the complexity
	--TYPE FSMProcessingMessage IS (FSMPIDLE, FSMPREGRESET, FSMPREGWRITE, FSMPREGREADSAMPLE, FSMPREGREADTXWAIT, FSMPREGREADTXWRITE, FSMPREGREADTXWAITCYCLE, FSMPDONE);
	TYPE FSMProcessingMessage IS (FSMPIDLE, FSMPREGWRITE, FSMPREGREADSAMPLE, FSMPREGREADTXWAIT, FSMPREGREADTXWRITE, FSMPREGREADTXWAITCYCLE, FSMPDONE);
	SIGNAL FSMProcessing, FSMProcessingNext : FSMProcessingMessage;
	SIGNAL uartWR                           : STD_LOGIC                    := '1';
	SIGNAL uartRD                           : STD_LOGIC                    := '1';
	--  SIGNAL      uartDataIn      :   STD_LOGIC;
	SIGNAL uartTXRdy                        : STD_LOGIC                    := '0';
	SIGNAL uartRXRdy                        : STD_LOGIC                    := '0';
	SIGNAL SPIuartDataTX                    : STD_LOGIC_VECTOR(7 downto 0) := x"00";
	SIGNAL uartDataRX                       : STD_LOGIC_VECTOR(7 downto 0);

	SIGNAL SPITXRdy   : STD_LOGIC := '0';
	SIGNAL SPIRXRdy   : STD_LOGIC := '0';
	--SIGNAL SPIDataTX                       : STD_LOGIC_VECTOR(7 downto 0)  := x"00";
	SIGNAL SPIDataRX  : STD_LOGIC_VECTOR(7 downto 0);
	signal SPIMessage : std_logic := '0';

	SIGNAL uartDataRXwide : STD_LOGIC_VECTOR(15 downto 0) := x"0000";
	--SIGNAL regWrite                         : STD_LOGIC;
	--SIGNAL      regRead         :   STD_LOGIC;
	--SIGNAL      regBuffer       :   STD_LOGIC;
	--SIGNAL regRDSample                      : STD_LOGIC;

	SIGNAL LoadWatchDog    : STD_LOGIC; --signal used to clear watch dog
	SIGNAL LoadByteCounter : STD_LOGIC; --this signal is used to clear byte counter for received message
	SIGNAL ProcessMess     : STD_LOGIC; --this signal starts processing FSM and is driven by the main FSM 
	SIGNAL processingDone  : STD_LOGIC; --this signal tells main FSM that processing FSM finished the task.
	--SIGNAL      regReadDone     :   STD_LOGIC;
	SIGNAL messageLength   : INTEGER RANGE 0 TO BUFFERSIZE - 1;

	signal flag_debug : std_logic := '1';
	
	signal uartComTX : std_logic;
	signal uartComRX : std_logic;

	component top_UART is
		port(
			RESET_N           : in  std_logic;
			CLK               : in  std_logic;
			WEN               : in  std_logic;
			OEN               : in  std_logic;
			CSN               : in  std_logic;
			DATA_IN           : in  std_logic_vector(7 downto 0);
			RX                : in  std_logic;
			BAUD_VAL          : in  std_logic_vector(12 downto 0);
			BIT8              : in  std_logic; --   IF SET TO ONE 8 DATA BITS OTHERWISE 7 DATA BITS
			PARITY_EN         : in  std_logic; --   IF SET TO ONE PARITY IS ENABLED OTHERWISE DISABLED
			ODD_N_EVEN        : in  std_logic; --   IF SET TO ONE ODD PARITY OTHERWISE EVEN PARITY
			BAUD_VAL_FRACTION : in  std_logic_vector(2 downto 0); --USED TO ADD EXTRA PRECISION TO BAUD VALUE WHEN BAUD_VAL_FRCTN_EN = 1
			PARITY_ERR        : out std_logic; --   PARITY ERROR INDICATOR ON RECIEVED DATA
			OVERFLOW          : out std_logic; --   RECEIVER OVERFLOW
			TXRDY             : out std_logic; --   TRANSMIT READY FOR ANOTHER BYTE
			RXRDY             : out std_logic; --   RECEIVER HAS A BYTE READY
			DATA_OUT          : out std_logic_vector(7 downto 0);
			TX                : out std_logic;
			FRAMING_ERR       : out std_logic
		);
	END COMPONENT;

	component spi_slave is
		generic(trans_length : natural := 8);
		port(clk, rst       : in  std_logic;
		     --spi hw lines
		     spi_csn        : in  std_logic;
		     spi_sclk       : in  std_logic;
		     spi_sdin       : in  std_logic;
		     spi_sdout      : out std_logic;
		     --internal signals
		     rdata          : out std_logic_vector(7 downto 0);
		     wdata          : in  std_logic_vector(7 downto 0);
		     -- spi_active		:out std_logic;
		     -- rx_valid		:out std_logic;
		     -- tx_valid		:out std_logic;
		     rx_valid_pulse : out std_logic;
		     tx_valid_pulse : out std_logic
		     --tx_req			:out std_logic
		    );
	end component;

	-----------------------------------------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------------------------------------	
BEGIN
	FSMSReceiveMessage : PROCESS(ALL)
	BEGIN
		IF clk'event and clk = '1' THEN
			IF nRST = '0' THEN
				FSMRX <= Idle;
			else
				-- when watch dog reaches the maximum value reinit
				IF cntWatchDog > WATCHDOGTIMEOUT THEN
					FSMRX <= MESSAGEERROR;
				ELSE
					FSMRX <= FSMRXNext;
				END IF;
			END IF;
		end if;
	END PROCESS;

	FSMStateChange : PROCESS(ALL)
	BEGIN
		CASE FSMRX IS
			--######################################################################
			WHEN IDLE =>
				IF uartRXRdy = '1' THEN
					FSMRXNext  <= READBYTE;
					SPIMessage <= '0';
				ELSIF SPIRXRdy = '1' THEN
					FSMRXNext  <= READBYTE;
					SPIMessage <= '1';
				ELSE
					FSMRXNext <= IDLE;
				END IF;
			--######################################################################
			WHEN WAIT4BYTE =>
				IF uartRXRdy = '0' and SPIRXRdy = '0' THEN
					FSMRXNext <= WAIT4BYTE;
				ELSE
					FSMRXNext <= READBYTE;
				END IF;
			--######################################################################
			WHEN READBYTE =>
				-- if read first byte then check the address if it is invalid
				-- then reinit waiting for the next byte.
				--				IF cntByteRx = 0 THEN
				--					FSMRXNEXT <= CHECKADDRESS;
				-- once the address is correct then decode the instruction to
				-- determine the length of the entire message if instruction is 
				-- not recognized restart reception
				IF cntByteRx = 0 THEN
					FSMRXNEXT <= DECODE;
					-- if the  received message length reaches the estimated size then 
					-- verify the received message check sum. 
				ELSIF cntByteRx >= messageLength THEN
					FSMRXNEXT <= CHECKSUM;
					-- otherwise wait for another byte
				ELSE
					FSMRXNEXT <= WAITCYCLE;
				END IF;
			--######################################################################
			--			WHEN CHECKADDRESS =>
			--				IF deviceAddress /= serBuffRX(0) THEN
			--					FSMRXNEXT <= IDLE;
			--				ELSE
			--					FSMRXNEXT <= WAIT4BYTE;
			--				END IF;
			--######################################################################
			WHEN DECODE =>
				--IF (serBuffRX(1) = x"3F") or (serBuffRX(1) = x"01") or (serBuffRX(1) = x"02") then
				IF (serBuffRX(0) = x"01") or (serBuffRX(0) = x"02") then
					FSMRXNEXT <= WAIT4BYTE;
				ELSE
					FSMRXNEXT <= MESSAGEERROR;
				END IF;
			--######################################################################
			WHEN CHECKSUM =>
				IF serBuffRXSum = x"00" THEN
					FSMRXNEXT <= PROCESSMESSAGE;
				ELSE
					FSMRXNEXT <= MESSAGEERROR;
				END IF;
			--######################################################################
			WHEN WAITCYCLE =>
				FSMRXNEXT <= WAIT4BYTE;
			--######################################################################
			WHEN PROCESSMESSAGE =>
				IF processingDone = '0' THEN
					FSMRXNEXT <= PROCESSMESSAGE;
				ELSE
					FSMRXNEXT <= IDLE;
				END IF;
			--######################################################################
			--if needed some error RX function can be implemented here
			WHEN MESSAGEERROR =>
				FSMRXNEXT <= IDLE;
			--######################################################################
			WHEN OTHERS =>
				FSMRXNEXT <= IDLE;
		END CASE;
	END PROCESS;

	regAddress <= serBuffRX(1);
	dataIn     <= serBuffRX(2) & serBuffRX(3) & serBuffRX(4) & serBuffRX(5);
	--######################################################################
	FSMStateSignals : PROCESS(ALL)
	BEGIN
		CASE FSMRX IS
			WHEN IDLE =>
				LoadWatchDog    <= '1';
				LoadByteCounter <= '1';
				uartRD          <= '1';
				processMess     <= '0';
			WHEN WAIT4BYTE =>
				LoadWatchDog    <= '0';
				LoadByteCounter <= '0';
				uartRD          <= '1';
				ProcessMess     <= '0';
			WHEN READBYTE =>
				LoadWatchDog    <= '0';
				LoadByteCounter <= '0';
				uartRD          <= '0' when (SPIMessage = '0') else '1';
				processMess     <= '0';
			--			WHEN CHECKADDRESS =>
			--				LoadWatchDog    <= '0';
			--				LoadByteCounter <= '0';
			--				uartRD          <= '1';
			--				processMess     <= '0';
			WHEN CHECKSUM =>
				LoadWatchDog    <= '0';
				LoadByteCounter <= '0';
				uartRD          <= '1';
				processMess     <= '0';
			WHEN DECODE =>
				LoadWatchDog    <= '0';
				LoadByteCounter <= '0';
				uartRD          <= '1';
				processMess     <= '0';
			WHEN PROCESSMESSAGE =>
				LoadWatchDog    <= '0';
				LoadByteCounter <= '0';
				uartRD          <= '1';
				processMess     <= '1';
			--  			  regAddress<=serBuffRX(2);
			--  			  dataIn<=serBuffRX(3)&serBuffRX(4)&serBuffRX(5)&serBuffRX(6);
			WHEN MESSAGEERROR =>
				LoadWatchDog    <= '1';
				LoadByteCounter <= '0';
				--uartRD          <= '1';
				processMess     <= '0';
			WHEN WAITCYCLE =>
				LoadWatchDog    <= '0';
				LoadByteCounter <= '0';
				--uartRD          <= '1';
				ProcessMess     <= '0';
			WHEN OTHERS =>
				LoadWatchDog    <= '0';
				LoadByteCounter <= '0';
				--uartRD          <= '1';
				processMess     <= '0';
		END CASE;
	END PROCESS;
	--######################################################################
	FSMReceiveMessageWatchDog : PROCESS(all)
	BEGIN
		IF clk'event and clk = '1' THEN
			IF nRST = '0' or LoadWatchDog = '1' THEN
				cntWatchDog <= 0;
			else
				cntWatchDog <= cntWatchDog + 1;
			end if;
		END If;
	END PROCESS;
	--######################################################################
	FSMReceiveMessageByteCounter : PROCESS(all)
	BEGIN
		IF LoadByteCounter = '1' THEN
			cntByteRx     <= 0;
			messageLength <= 2;
			serBuffRXSum  <= x"00";
		ELSIF clk'event and clk = '1' THEN
			IF FSMRX = READBYTE THEN
				cntByteRX            <= cntByteRX + 1;
				serBuffRX(cntByteRX) <= uartDataRX when (SPIMessage = '0') else SPIDataRX;
				serBuffRXSum         <= (serBuffRXSum xor uartDataRX) when (SPIMessage = '0') else (serBuffRXSum xor SPIDataRX);
			END IF;
			IF FSMRX = DECODE THEN
				--based on the command given by the incoming message, 
				--the corresponding expected length of the message is set
				--for the receiver 
				CASE serBuffRX(0) IS
					--WHEN x"3F" =>
					--	messageLength <= 2;
					WHEN x"01" =>
						messageLength <= 6;
					WHEN x"02" =>
						messageLength <= 2;
					WHEN OTHERS =>
						messageLength <= 0;
				END CASE;
			END IF;
		END IF;
	END PROCESS;

	--######################################################################
	FSMProcessingClock : PROCESS(ALL)
	BEGIN
		IF clk'event and clk = '1' THEN
			IF nRST = '0' THEN
				FSMProcessing <= FSMPIDLE;
			else
				FSMProcessing <= FSMProcessingNext;
			END IF;
		end if;
	END PROCESS;

	FSMProcessingStateChange : PROCESS(ALL)
	BEGIN
		CASE FSMProcessing IS
			--######################################################################
			WHEN FSMPIDLE =>
				IF processMess = '1' THEN
					CASE serBuffRX(0) IS
						--WHEN x"3F" =>
						--	FSMProcessingNext <= FSMPREGRESET;
						WHEN x"01" =>
							FSMProcessingNext <= FSMPREGWRITE;
						WHEN x"02" =>
							FSMProcessingNext <= FSMPREGREADSAMPLE;
						WHEN OTHERS =>
							FSMProcessingNext <= FSMPIDLE;
					END CASE;
				ELSE
					FSMProcessingNext <= FSMPIDLE;
				END IF;
			--######################################################################
			--WHEN FSMPREGRESET =>
			--	FSMProcessingNext <= FSMPDONE;
			WHEN FSMPREGWRITE =>
				FSMProcessingNext <= FSMPDONE;
			WHEN FSMPREGREADSAMPLE =>
				FSMProcessingNext <= FSMPREGREADTXWAIT;
			WHEN FSMPREGREADTXWAIT =>
				IF uartTXRdy = '0' and SPIMessage = '0' then
					FSMProcessingNext <= FSMPREGREADTXWAIT;
				elsif SPITXRdy = '0' and SPIMessage = '1' THEN
					FSMProcessingNext <= FSMPREGREADTXWAIT;
				ELSE
					FSMProcessingNext <= FSMPREGREADTXWRITE;
				END IF;
			WHEN FSMPREGREADTXWRITE =>
				FSMProcessingNext <= FSMPREGREADTXWAITCYCLE;
			WHEN FSMPREGREADTXWAITCYCLE =>
				IF cntByteTX <= 4 THEN
					FSMProcessingNext <= FSMPREGREADTXWAIT;
				ELSE
					FSMProcessingNext <= FSMPDONE;
				END IF;
			WHEN FSMPDONE =>
				FSMProcessingNext <= FSMPIDLE;
			WHEN OTHERS =>
				FSMProcessingNext <= FSMPIDLE;
		END CASE;
	END PROCESS;
	--######################################################################
	FSMProcessingSignals : PROCESS(ALL)
	BEGIN
		CASE FSMProcessing IS
			WHEN FSMPIDLE =>
				rstOut         <= '0';
				flag_debug     <= '1';
				--regWrite       <= '0';
				--regRDSample    <= '0';
				uartWR         <= '1';
				processingDone <= '0';
			--WHEN FSMPREGRESET =>
			--	rstOut         <= '1';
			--	flag_debug <= '1';
			--	--regWrite       <= '0';
			--	--regRDSample    <= '0';
			--	uartWR         <= '1';
			--	processingDone <= '0';
			WHEN FSMPREGWRITE =>
				rstOut         <= '0';
				flag_debug     <= '1';
				--regWrite       <= '0';
				--regRDSample    <= '0';
				uartWR         <= '1';
				processingDone <= '0';
			WHEN FSMPREGREADSAMPLE =>
				rstOut         <= '0';
				flag_debug     <= '1';
				--regWrite       <= '0';
				--regRDSample    <= '1';
				uartWR         <= '1';
				processingDone <= '0';
			WHEN FSMPREGREADTXWAIT =>
				rstOut         <= '0';
				flag_debug     <= '1';
				--regWrite       <= '0';
				--regRDSample    <= '0';
				uartWR         <= '1';
				processingDone <= '0';
			WHEN FSMPREGREADTXWRITE =>
				rstOut         <= '0';
				flag_debug     <= '0';
				--regWrite       <= '0';
				--regRDSample    <= '0';
				uartWR         <= '0' when (SPIMessage = '0') else '1';
				processingDone <= '0';
			WHEN FSMPREGREADTXWAITCYCLE =>
				rstOut         <= '0';
				flag_debug     <= '1';
				--regWrite       <= '0';
				--regRDSample    <= '0';
				uartWR         <= '1';
				processingDone <= '0';
			WHEN FSMPDONE =>
				rstOut         <= '0';
				flag_debug     <= '1';
				--regWrite       <= '0';
				--regRDSample    <= '0';
				uartWR         <= '1';
				processingDone <= '1';
			WHEN OTHERS =>
				rstOut         <= '0';
				flag_debug     <= '1';
				--regWrite       <= '0';
				--regRDSample    <= '0';
				uartWR         <= '0';
				processingDone <= '0';
		END CASE;
	END PROCESS;
	--######################################################################
	--According to the write message address relevant Write strobe is asserted to 1
	decodeWriteAddress : PROCESS(ALL)
	BEGIN
		if clk'event and clk = '1' THEN
			if FSMProcessing = FSMPREGWRITE THEN
				regWRsig                                     <= (others => '0');
				regWRsig(to_integer(unsigned(serBuffRX(1)))) <= '1';
			else
				regWRsig <= (others => '0');
			end if;
		end if;
	END PROCESS;

	--According to the Read message address relevant Write strobe is asserted to 1
	--this can be used to controll fifo in the upper entity 

	decodeReadAddress : PROCESS(ALL)
	BEGIN
		if clk'event and clk = '1' THEN
			if FSMProcessing = FSMPREGREADSAMPLE THEN
				regRDsig                                     <= (others => '0');
				regRDsig(to_integer(unsigned(serBuffRX(1)))) <= '1';
			else
				regRDsig <= (others => '0');
			end if;
		end if;
	END PROCESS;

	FSMTransmitRegisterByte : PROCESS(all)
	BEGIN
		IF FSMProcessing = FSMPREGREADSAMPLE THEN
			cntByteTX    <= 0;
			--serBuffTX(0) <= deviceAddress;
			serBuffTX(0) <= DataOut(31 downto 24);
			serBuffTX(1) <= DataOut(23 downto 16);
			serBuffTX(2) <= DataOut(15 downto 8);
			serBuffTX(3) <= DataOut(7 downto 0);
			serBuffTX(4) <= x"00";
			serBuffTXSum <= x"00";
		ELSIF clk'event and clk = '1' THEN
			IF FSMProcessing = FSMPREGREADTXWAIT THEN
				SPIuartDataTX <= serBuffTX(cntByteTX);
			END IF;
			IF FSMProcessing = FSMPREGREADTXWRITE THEN
				cntByteTX     <= cntByteTX + 1;
				SPIuartDataTX <= serBuffTX(cntByteTX);
				serBuffTXSum  <= serBuffTXSum xor serBuffTX(cntByteTX);
				serBuffTX(4)  <= serBuffTX(4) xor serBuffTX(cntByteTX);
			END IF;
		END IF;
	END PROCESS;

	uartConfig : top_UART
		PORT MAP(
			RESET_N           => nrst,
			CLK               => clk,
			WEN               => uartWR,
			OEN               => uartRD,
			CSN               => '0',
			DATA_IN           => SPIuartDataTX,
			RX                => uartComRX,
			--BAUD_VAL            => '0' & x"001", --1.25Mbit   
			BAUD_VAL            => '0' & x"000", --2.5Mbit   
			--BAUD_VAL          => '0' & x"015", --115200bit
			--BAUD_VAL          => '0' & x"004", --500000bit
			BIT8              => '1',
			PARITY_EN         => '0',
			ODD_N_EVEN        => '0',
			BAUD_VAL_FRACTION => "000", --0 correction
			PARITY_ERR        => open,
			OVERFLOW          => open,
			TXRDY             => uartTXRdy, -- ready in 0
			RXRDY             => uartRXRdy, -- ready in 0
			DATA_OUT          => uartDataRX,
			TX                => uartComTX,
			FRAMING_ERR       => open
		);
		
		-- Can use FTDI or RS485 simultaneously
		uartComTX_FTDI <= uartComTX;
		uartComTX_RS485 <= uartComTX;
		uartComRX <= '0' when uartComRX_FTDI = '0' or uartComRX_RS485 = '0' else '1';

	spiConfig : spi_slave
		generic map(
			trans_length => 8
		)
		port map(
			clk            => clk,
			rst            => not nrst,
			--spi hw lines
			spi_csn        => spi_csn,
			spi_sclk       => spi_sclk,
			spi_sdin       => spi_sdin,
			spi_sdout      => spi_sdout,
			--internal signals
			rdata          => SPIDataRX,
			wdata          => SPIuartDataTX,
			rx_valid_pulse => SPIRXRdy,
			tx_valid_pulse => SPITXRdy
		);

end behav;
