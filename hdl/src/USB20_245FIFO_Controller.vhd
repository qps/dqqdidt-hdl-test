--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: USB3_245FIFO.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- Sends information to USB 3.0 (FT601) through 245 FIFO (32 bits)
--
-- Targeted device: <Family::SmartFusion2> <Die::M2S150TS> <Package::1152 FC>
-- Author: Daniel Blasco Serrano
--
--------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.UQDSLib.all;

entity USB20_245FIFO_Controller is
	generic(
		PACKET_SIZE : natural := 11
	);
	port(
		-- USB 245FIFO lines
		USBnRXF    : in  std_logic;     -- Data available to be received (not used)
		USBnTXE    : in  std_logic;     -- Data allowed to be sent
		USBCLK     : in  std_logic;
		USBDataBus : out std_logic_vector(7 downto 0);
		USBnRD     : out std_logic;     -- Start receiving data (not used)
		USBnWR     : out std_logic;     -- Start sending data
		USBnOE     : out std_logic;     -- 0 - Enable reading / 1 - Enable writing
		-- FIFO lines
		FIFOData   : in  data8Array(PACKET_SIZE - 9 downto 0); -- Data to be added to FIFO (header, counter and CRC not included)
		FIFOWR     : in  std_logic;     -- Enable writing
		FIFOFull   : out std_logic;
		clk        : in  std_logic;
		nRST       : in  std_logic
	);
end USB20_245FIFO_Controller;

architecture behav of USB20_245FIFO_Controller is

	type states is (IDLE, SAMPLEHOLD, WAITEMPTY, WRITEBYTES, DONE, ERROR);
	type txBuffer is array (PACKET_SIZE - 1 downto 0) of std_logic_vector(7 downto 0);

	signal current_state : states;
	signal next_state    : states;

	signal data_buffer : txBuffer;      -- Whole packet to be written in FIFO
	signal byte_buffer : std_logic_vector(7 downto 0); -- Actual byte being written in FIFO

	signal byte_cnt           : integer range 0 to PACKET_SIZE + 1;
	signal start_byte_counter : std_logic;

	signal sample_cnt     : integer range 0 to integer'high;
	signal sample_cnt_reg : std_logic_vector(31 downto 0);

	signal write_byte : std_logic;

	signal FIFO_empty : std_logic;

	signal USBDataBusWire : std_logic_vector(7 downto 0);

	--	signal counter : integer range 0 to 255;
	--	signal not_send : std_logic := '0';
	signal USBnTXEPrev : std_logic := '1';
	signal USBnTXEBuff : std_logic := '1';
	--signal USBnWRPrev  : std_logic := '1';
	--signal USBnWRPrev2 : std_logic := '1';
	--signal USBnWRPrev3 : std_logic := '1';
	--signal USBnWRPrev4 : std_logic := '1';

	signal actual_counter, previous_counter : integer range 0 to integer'high;
	signal flag_missing_packets             : std_logic := '0';
	signal counter_empty_ext                : integer range 0 to 49 := 0;

BEGIN

	FSMClock : process(clk, nRST)
	begin
		if rising_edge(clk) then
			if nRST = '0' then
				current_state <= IDLE;
			else
				current_state <= next_state;
			end if;
		end if;
	end process;

	FSMMain : process(all)
	begin
		case current_state is

			when IDLE =>                -- Wait for incoming data (and FIFO is not full)
				if (FIFOFull = '0') and (FIFOWR = '1') then
					next_state <= SAMPLEHOLD;
				else
					next_state <= IDLE;
				end if;

			when SAMPLEHOLD =>          -- Adding header and counter to packet
				next_state <= WRITEBYTES;

			when WAITEMPTY =>           -- FIFO became full in middle of packet
				if FIFOFull = '1' then
					next_state <= WAITEMPTY;
				else
					next_state <= WRITEBYTES;
				end if;

			when WRITEBYTES =>          -- Currently writing bytes to FIFO
				if byte_cnt >= PACKET_SIZE - 1 then
					next_state <= DONE;
				elsif FIFOFull = '1' then
					next_state <= WAITEMPTY;
				else
					next_state <= WRITEBYTES;
				end if;

			when DONE =>
				next_state <= IDLE;

			when ERROR =>
				next_state <= IDLE;

		end case;
	end process;

	FSMSignals : process(all)
	begin
		case current_state is
			when IDLE =>
				byte_buffer        <= x"00";
				start_byte_counter <= '0';
				write_byte         <= '0';
			when SAMPLEHOLD =>
				byte_buffer        <= x"00";
				start_byte_counter <= '1';
				write_byte         <= '0';
			when WAITEMPTY =>
				byte_buffer        <= data_buffer(byte_cnt);
				start_byte_counter <= '0';
				write_byte         <= '0';
			when WRITEBYTES =>
				byte_buffer        <= data_buffer(byte_cnt);
				start_byte_counter <= '0';
				write_byte         <= '1';
			when DONE =>
				byte_buffer        <= x"00";
				start_byte_counter <= '0';
				write_byte         <= '0';
			when ERROR =>
				byte_buffer        <= x"00";
				start_byte_counter <= '0';
				write_byte         <= '0';
		end case;
	end process;

	SampleHoldCurrentValues : process(clk)
	begin
		if rising_edge(clk) then
			if current_state = SAMPLEHOLD then
				--Add header and counter
				data_buffer(0) <= x"AA";
				data_buffer(1) <= x"BB";
				data_buffer(2) <= x"CC";
				data_buffer(3) <= sample_cnt_reg(31 downto 24);
				data_buffer(4) <= sample_cnt_reg(23 downto 16);
				data_buffer(5) <= sample_cnt_reg(15 downto 8);
				data_buffer(6) <= sample_cnt_reg(7 downto 0);

				if (actual_counter /= (previous_counter + 55) and actual_counter > previous_counter) then
					flag_missing_packets <= '1';
				else
					flag_missing_packets <= '0';
				end if;

				actual_counter               <= sample_cnt;
				previous_counter             <= actual_counter;
				-- Add the data
				mapFIFOInputToBuffer : for i in 7 to PACKET_SIZE - 2 loop
					data_buffer(i) <= FIFOData(i - 7);
				end loop;
				-- CRC byte (empty)
				data_buffer(PACKET_SIZE - 1) <= x"00";

			elsif current_state = WRITEBYTES and byte_cnt <= PACKET_SIZE - 1 then
				data_buffer(PACKET_SIZE - 1) <= data_buffer(PACKET_SIZE - 1) xor data_buffer(byte_cnt);
			end if;

		end if;
	end process;

	SampleCounter : process(clk, nRST)
	begin
		if rising_edge(clk) then
			if nRST = '0' then
				sample_cnt <= 0;
			else
				if sample_cnt = integer'high then
					sample_cnt <= 0;
				else
					sample_cnt <= sample_cnt + 1;
				end if;
				sample_cnt_reg <= std_logic_vector(to_unsigned(sample_cnt, 32));
			end if;
		end if;
	end process;

	ByteCounter : process(all)
	begin
		if nRST = '0' then
			byte_cnt <= 0;
		elsif rising_edge(clk) then
			if start_byte_counter = '1' then
				byte_cnt <= 0;
			elsif current_state = WRITEBYTES then
				byte_cnt <= byte_cnt + 1;
			end if;
		end if;
	end process;

	--	USBFIFOController : circular_buffer
	--		generic map(
	--			memory_size => 2048
	--		)
	--			-- Port list
	--		port map(
	--			-- Inputs
	--			rst      => not nRST,
	--			data_in  => byte_buffer,
	--			wclock   => clk,
	--			we       => write_byte,
	--			data_out => USBDataBusWire,
	--			rclock   => USBCLK,
	--			re       => not USBnWR and not USBnTXE,     --not USBnTXE and not FIFO_empty,
	--			aempty   => FIFO_empty,
	--			afull    => FIFOFull
	--		);

	--USBFIFOController : USBFIFO
	--port map(
	--	DATA => byte_buffer,
	--	RCLOCK => USBCLK,
	--	RE => not USBnWR and not USBnTXE,
	--	RESET => not nRST,
	--	WCLOCK => clk,
	--	WE => write_byte,
	--	AEMPTY => FIFO_empty,
	--	AFULL => FIFOFull,
	--	EMPTY => open,
	--	FULL => open,
	--	Q => USBDataBusWire
	--);

	-- intentar cambiar de byte cada 2 clocks
	-- 80 mhz
	--		counterfake : process(all)
	--			variable retroceder : std_logic := '0';
	--	begin
	--		if nRST = '0' then
	--			counter <= 0;
	--		elsif falling_edge(USBCLK) then
	--			if USBnWR = '0' and USBnTXE = '0' then
	--				retroceder := '1';
	--				if counter = 255 then
	--					counter <= 0;
	--				else
	--					counter <= counter + 1;
	--				end if;
	--			elsif USBnTXE = '1' then
	----				if(retroceder = '1') then
	----					retroceder := '0';
	----					if counter = 0 then
	----						counter <= integer'high;
	----					else
	----						counter <= counter - 1;
	----					end if;
	----				end if;
	--			end if;
	--		end if;
	--	end process;

	USBFIFO : FIFOUSB
		port map(
			-- Inputs
			DATA   => byte_buffer,
			RCLOCK => USBCLK,
			RE     => not USBnWR and not USBnTXE,
			RESET  => not nRST,
			WCLOCK => clk,
			WE     => write_byte,
			-- Outputs
			AEMPTY => FIFO_empty,
			AFULL  => FIFOFull,
			EMPTY  => open,
			FULL   => open,
			Q      => USBDataBusWire
		);

	--USBnWR     <= not ((not USBnTXE) and (not FIFO_empty));
	writeusb : process(USBCLK, nRST)
		--		variable other_cnt : integer range 0 to 100;
		--variable correction : std_logic := '0';
	begin
		if rising_edge(USBCLK) then
			if nRST = '0' then
			--			counter_empty_ext  <= 0;
			else
				--			--USBnWR <= not ((not USBnTXE) and (not FIFO_empty) and (not USBnTXEPrev));
				--			if(counter_empty_ext = 0) then
				USBnWR <= not ((not USBnTXE) and (not USBnTXEPrev) and (not FIFO_empty));
				--				if(USBnTXE = '0' and USBnTXEPrev = '0' and FIFO_empty = '0') then
				--					counter_empty_ext <= 49;
				--				end if;
				--			else
				--				USBnWR <= not ((not USBnTXE) and (not USBnTXEPrev));
				--				if (USBnTXE = '0' and USBnTXEPrev = '0') then
				--					counter_empty_ext <= counter_empty_ext - 1;
				--					correction := '1';
				--				elsif (USBnTXE = '1' and USBnTXEPrev = '0' and correction = '1') then
				--					counter_empty_ext <= counter_empty_ext + 1;
				--					correction := '0';
				--				end if;
				--			end if;

				--			USBnWRPrev <= USBnWR; 		-- This will force that raddress is constant for 3 clocks,
				--			USBnWRPrev2 <= USBnWRPrev;	-- necessary to cross clock domain from fast to slow
				--			USBnWRPrev3 <= USBnWRPrev2;
				--			USBnWRPrev4 <= USBnWRPrev3;

				USBnTXEBuff <= USBnTXE;
				USBnTXEPrev <= USBnTXEBuff;

				--			if(other_cnt = 100) then
				--				other_cnt := 0;
				--			else
				--				other_cnt := other_cnt + 1;
				--				if (other_cnt = 10 or other_cnt = 11 or other_cnt = 12 or other_cnt = 50 or other_cnt = 51 or other_cnt = 80) then
				--					not_send <= '1';
				--				else
				--					not_send <= '0';
				--				end if;
				--			end if;

			end if;
		end if;
	end process;

	USBDataBus <= USBDataBusWire;       --std_logic_vector(to_unsigned(counter,8));--USBDataBusWire;

	USBnOE <= '1';
	USBnRD <= '1';

end behav;
