LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
use ieee.std_logic_unsigned.all;

--library RADlib;
--use RADlib.util.all;

library UQDSLib;
use UQDSLib.util.all;

-------------------------------Description------------------------------------------
--Decimator by X (power of 2)
--Function: substitution of X samples with one which vale is mean value of X samples
--
--omode switches between registered and unregistered outputs
--
---------------------------------Versions-------------------------------------------
-- 3.09.2014 - A.Skoczen
-- extra comments J. Steckert
-- 14.2.2018 - dblascos: changed d_width range up to 32 bits
------------------------------------------------------------------------------------

entity decimateX is
generic(	d_width : integer range 15 to 32 := 24;
			X : natural := 4; 
			omode: boolean := false
	); 
port (clk, reset, dready	: in std_logic;
			data_in		: in std_logic_vector(d_width -1 downto 0);
			slow_dready	: out std_logic;
			slow_data	: out std_logic_vector(d_width -1 downto 0)
	);
end decimateX;

architecture a of decimateX is
constant cntw : natural := log2c(X);
constant nullcnt : std_logic_vector(cntw-1 downto 0) := (others => '0');

type state is (idle, accum, sumf, calc);

signal st, nst : state;
signal sum :  signed(d_width+cntw-1 downto 0);
signal sdata :  signed(d_width-1 downto 0);
signal cnt : std_logic_vector(cntw-1 downto 0);
signal sdready, dry, buf : std_logic;
signal z : boolean;

begin
--check if decimation is 2^n
assert (X = 2**cntw) report "decimateX: genric X must be power of 2" severity error;

registered: if omode generate begin
outdata: process(clk)
begin
	if rising_edge(clk) then
		if reset = '1' then
			slow_data <= (others => '0');
			slow_dready <= '0';
		else
			if sdready = '1' then
				slow_data <= std_logic_vector(sdata);
				slow_dready <= sdready;
			else
				slow_dready <= '0';
			end if;
		end if;
	end if;
end process;
end generate;

unregistered: if not omode generate begin
slow_data <= std_logic_vector(sdata);
slow_dready <= sdready;
end generate;

--2 process state machine, state register
fsm_reg: process(clk)
begin
	if rising_edge(clk) then
		if reset = '1' then
			st <= accum;
		else
			st <= nst;
		end if;
	end if;
end process;

--state machine logic
fsm_logic: process(all)
begin
	sdready <= '0';
	sdata <= (others => '0');
	case st is
		when idle => 
			if dry = '1' then
				nst <= sumf;
			else 
				nst <= idle;
			end if;
		when accum => 
			if dry = '1' then
				nst <= sumf;
			else 
				nst <= accum;
			end if;
		when sumf => 
			--if desired number of samples are added (z=0) calculate mean
			--otherwise keep adding
			if z then
				nst <= calc;
			else 
				nst <= accum;
			end if;
		when calc => 
			sdata <= sum(d_width+cntw-1 downto cntw);
			sdready <= '1';
			nst <= idle;
	end case;
end process;

accumul: process(clk)
begin
	if rising_edge(clk) then
		if reset = '1' then
			sum <= (others => '0');
		else
			case st is
				when idle => sum <= (others => '0');
				when sumf => sum <= sum + signed(data_in);
				when others => sum <= sum;
			end case;		
		end if;		
	end if;		
end process;

--z = true if counter is at zero
z <= true when cnt = nullcnt else false;



cnt4: process(clk)
begin
        if rising_edge(clk) then
                if reset = '1' then
			cnt <= nullcnt;
		else
			if dry = '1' then
				if X-1 = to_integer(unsigned(cnt)) then
					cnt <= nullcnt;
				else
					cnt <= cnt + 1;
				end if;
			end if;
		end if;		
	end if;		
end process;

edgedetector: process(clk)
begin
if rising_edge(clk) then 
	if reset = '1' then 
		dry <= '0';
		buf <= '0';
   	else   
		--detects the rising edge of dready and creates the one clock wide dry pulse
	        buf <= dready;
        	if dready < buf then   
            		dry <= '1';
        	else
            		dry <= '0';
        	end if;
	end if;
end if;
end process;

end;