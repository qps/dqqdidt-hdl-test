# -*- coding: utf-8 -*-
"""
Created on ?????????
Description: Package to generate VHDL package from Excell sheet 
    
@author: Jelena Spasic

Modified: direction of port comRegOut; A.Skoczen (AGH-UST); 6.08.2019
Modified: function with three arguments; A.Skoczen (AGH-UST); 7.08.2019
Modified: to exclode Header.chd; A.Skoczen (AGH-UST); 12.08.2019
Modified: based on one excel sheet only: Surbhi
Modified: comments --Jens
"""
import xlrd
import os

def gen_header(rmo, lib_name):
    rmo.write('library IEEE;\n')
    rmo.write('use IEEE.STD_LOGIC_1164.all;\n\n')
    rmo.write('library '+lib_name+';\n')
    rmo.write('use '+lib_name+'.'+lib_name+'.all;\n')
    rmo.write('use '+lib_name+'.buildstamp.all;\n')
    rmo.write('\npackage reg_map_pkg is\n')
    rmo.write('\t--inputs\n')
    rmo.write('\tsignal comRegDataIn_MUX\t\t: std_logic_vector(REGISTER_SIZE - 1 downto 0);\n')
    rmo.write('\tsignal comRegOut\t\t\t: regArray;\n')
    rmo.write('\t--outputs\n')
    rmo.write('\tsignal comRegIn, comRegRD, comRegMk\t: regArray;\n')
    rmo.write('\tsignal comRegRO\t\t\t\t\t\t: std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);\n')
    return 1

#select the configuration block to generate
#config_block_select = 2      #one more argument
#indicate path to Excel file
#xlsx_path
#indicate path to VHDL output file 
#pckg_vhdl_path

def generate(config_block_select, xlsx_path, pckg_vhdl_path):
    lib_name = 'UQDSLib'
    xlsx_file = os.path.join(xlsx_path, 'Register_map.xlsx')
    if not os.path.exists(xlsx_file):
        print('Input Excel file ', xlsx_file, ' dose not exist')
        return 0
    else:
        print('Input Excel file ', xlsx_file)
    reg_map_input = xlrd.open_workbook(xlsx_file)
    sheets = reg_map_input.sheet_names()
    print('Available sheets: ', sheets)
    if config_block_select > len(sheets):
        print('Sheet with index', config_block_select, 'is not available')
        exit()
    else:
        print('Sheet', sheets[config_block_select-1], 'is selected')
    pckg_vhdl_file = os.path.join(pckg_vhdl_path, 'reg_map_pkg.vhd')
    if not os.path.exists(pckg_vhdl_file):
        print('Output VHDL file ', pckg_vhdl_file, ' dose not exist')
        print('It will be generated')
    else:
        print('Output VHDL file ', pckg_vhdl_file)
        print('It will be regenerated')
    print('....................')
    reg_map_output = open(pckg_vhdl_file,'w')
    h_f = os.path.join(xlsx_path, 'header.vhd')
    if not os.path.exists(h_f):
        #print('Header VHDL file ', h_f, ' is missing')
        gen_header(reg_map_output, lib_name)
        #return 0
    else:
        #put the content of the header file into the reg_map package
        header_file = open(h_f)
        for line in header_file.readlines():
            reg_map_output.write(line)
        header_file.close()
    
    #Handling the top signals
    #take the first sheet from the excel workbook	
    reg_map_top = reg_map_input.sheet_by_index(config_block_select-1)
    #extract all the reg signals
    reg_signal_top = [reg_map_top.cell_value(r, 2) for r in range(1, 129)]
    reg_type_top = [reg_map_top.cell_value(r, 4) for r in range(1, 129)]
    reg_used_top = [reg_map_top.cell_value(r, 1) for r in range(1, 129)]
    reg_action_top = [reg_map_top.cell_value(r, 5) for r in range(1, 129)]
    reg_subcnt_top = [reg_map_top.cell_value(r, 3) for r in range(1, 129)]
    reg_default_top = [reg_map_top.cell_value(r, 9) for r in range(1, 129)]
    reg_mask_top = [reg_map_top.cell_value(r, 8) for r in range(1, 129)]
    reg_signal_name_top = []
    reg_signal_type_top = []
    reg_signal_action_top = []
    for i in range(len(reg_used_top)):
        if reg_used_top[i] == 'y' and (reg_signal_top[i] not in reg_signal_name_top) and reg_signal_top[i] != 'build_rev':
            reg_signal_name_top.append(reg_signal_top[i])
            reg_signal_type_top.append(reg_type_top[i])
            if reg_action_top[i] == 'r':
                reg_signal_action_top.append('in')
            elif reg_action_top[i] == 'w':
                reg_signal_action_top.append('out')
            elif reg_action_top[i] == 'rw':
                reg_signal_action_top.append('inout')
    for i in range(len(reg_signal_name_top)):
        if reg_signal_type_top[i] == '':
            reg_map_output.write('\tsignal ' + reg_signal_name_top[i] + '\t: std_logic_vector(REGISTER_SIZE - 1 downto 0);\n')	
        else:
            reg_map_output.write('\tsignal ' + reg_signal_name_top[i] + '\t:' + reg_signal_type_top[i] + ';\n')
    reg_map_output.write('\tsignal ConfigCustomRegister' + '\t:' + 'customRegArray ;\n')
    reg_map_output.write('\n')
    
    #Handling the configuration signals		
    #take the lower part of the selected sheet from the excel workbook
    #NOTE: FIX-coded boundary (line 129 and below) --> should be more flexible but leave it for now --Jens	
    reg_map_config = reg_map_input.sheet_by_index(config_block_select-1)
    #extract all the reg signals
    reg_signal_config = [reg_map_config.cell_value(r, 2) for r in range(129, reg_map_config.nrows)]
    reg_type_config = [reg_map_config.cell_value(r, 4) for r in range(129, reg_map_config.nrows)]
    reg_used_config = [reg_map_config.cell_value(r, 1) for r in range(129, reg_map_config.nrows)]
    reg_action_config = [reg_map_config.cell_value(r, 5) for r in range(129, reg_map_config.nrows)]
    reg_subcnt_config = [reg_map_config.cell_value(r, 3) for r in range(129, reg_map_config.nrows)]
    reg_default_config = [reg_map_config.cell_value(r, 9) for r in range(129, reg_map_config.nrows)]
    reg_mask_config = [reg_map_config.cell_value(r, 8) for r in range(129, reg_map_config.nrows)]
    reg_signal_name_config = []
    reg_signal_type_config = []
    reg_signal_action_config = []
    for i in range(len(reg_used_config)):
        if reg_used_config[i] == 'y' and reg_signal_config[i] not in reg_signal_name_config:
            reg_signal_name_config.append(reg_signal_config[i])
            reg_signal_type_config.append(reg_type_config[i])
            if reg_action_config[i] == 'r':
                reg_signal_action_config.append('in')
            elif reg_action_config[i] == 'w':
                reg_signal_action_config.append('out')
            elif reg_action_config[i] == 'rw':
                reg_signal_action_config.append('inout')
    		
    for i in range(len(reg_signal_name_config)):
        if reg_signal_type_config[i] == '':
            reg_map_output.write('\tsignal ' + reg_signal_name_config[i] + '\t: std_logic_vector(REGISTER_SIZE - 1 downto 0);\n')	
        else: 
            reg_map_output.write('\tsignal ' + reg_signal_name_config[i] + '\t:' + reg_signal_type_config[i] + ';\n')
    
    reg_map_output.write('\n')
    		
    #Procedure top
    reg_map_output.write('\tprocedure RegisterMap (\n')
    reg_map_output.write('\t\tsignal comRegDataIn_MUX\t: in std_logic_vector(REGISTER_SIZE - 1 downto 0);\n')
    reg_map_output.write('\t\tsignal comRegOut\t: in regArray;\n')
    reg_map_output.write('\t\tsignal comRegIn, comRegRD, comRegMk\t: out regArray;\n')
    reg_map_output.write('\t\tsignal comRegRO\t: out std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);\n')
    for i in range(len(reg_signal_name_top)):
        if reg_signal_type_top[i] == '':
            reg_map_output.write('\t\tsignal ' + reg_signal_name_top[i] + '\t: ' + reg_signal_action_top[i] + ' std_logic_vector(REGISTER_SIZE - 1 downto 0);\n')	
        else: 
            reg_map_output.write('\t\tsignal ' + reg_signal_name_top[i] + '\t: ' + reg_signal_action_top[i] + ' ' + reg_signal_type_top[i] + ';\n')
    reg_map_output.write('\t\tsignal ConfigCustomRegister' + '\t: ' + ' out customRegArray\n')
       
    reg_map_output.write('\n\t);\n\n')
    
    #Procedure definition configuration specific registers mapper
    # this procedure maps the generic configCustomRegisters array to more specific arrays within config block
    # here only procedure definition is generated see below for implementation (package body)
    reg_map_output.write('\tprocedure RegisterMapConfigBlock (\n')
    reg_map_output.write('\t\tsignal ConfigCustomRegister\t: in customRegArray;\n')
    for i in range(len(reg_signal_name_config)):
        if i != len(reg_signal_name_config) - 1:
            if reg_signal_type_config[i] == '':
                reg_map_output.write('\t\tsignal ' + reg_signal_name_config[i] + '\t: ' + reg_signal_action_config[i] + ' std_logic_vector(REGISTER_SIZE - 1 downto 0);\n')	
            else: 
                reg_map_output.write('\t\tsignal ' + reg_signal_name_config[i] + '\t:' + reg_signal_action_config[i] + ' ' + reg_signal_type_config[i] + ';\n')
        else:
            if reg_signal_type_config[i] == '':
                reg_map_output.write('\t\tsignal ' + reg_signal_name_config[i] + '\t: ' + reg_signal_action_config[i] + ' std_logic_vector(REGISTER_SIZE - 1 downto 0)\n')	
            else: 
                reg_map_output.write('\t\tsignal ' + reg_signal_name_config[i] + '\t:' + reg_signal_action_config[i] + ' ' + reg_signal_type_config[i])
    reg_map_output.write('\n\t);\n\n')
    reg_map_output.write('\tend reg_map_pkg;\n\n')
    
    #Package body
    # Implementation of register map (each registers consists out of four(five for writables) entries):
    # comRegRO(): read only indicator
    # comRegIN(): register input
    # comRegRD(): register reset default
    # comRegM():  register mask ('1' denotes writable bits)
    # xxx <= comRegOut() register output to rest of design (only for writable registers)
    
    reg_map_output.write('package body reg_map_pkg is\n\n')
    reg_map_output.write('\tprocedure RegisterMap (\n')
    reg_map_output.write('\t\tsignal comRegDataIn_MUX   : in std_logic_vector(REGISTER_SIZE - 1 downto 0);\n')
    reg_map_output.write('\t\tsignal comRegOut\t: in regArray;\n')
    reg_map_output.write('\t\tsignal comRegIn, comRegRD, comRegMk\t: out regArray;\n')
    reg_map_output.write('\t\tsignal comRegRO : out std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);\n')
    for i in range(len(reg_signal_name_top)):
        if reg_signal_type_top[i] == '':
            reg_map_output.write('\t\tsignal ' + reg_signal_name_top[i] + '\t: ' + reg_signal_action_top[i] + ' std_logic_vector(REGISTER_SIZE - 1 downto 0);\n')	
        else: 
            reg_map_output.write('\t\tsignal ' + reg_signal_name_top[i] + '\t: ' + reg_signal_action_top[i] + ' ' + reg_signal_type_top[i] + ';\n')
    reg_map_output.write('\t\tsignal ConfigCustomRegister' + '\t: ' + ' out customRegArray\n')
    reg_map_output.write('\n\t) is \n \tbegin \n')
    
    for i in range(len(reg_used_top)):
        if reg_used_top[i] == 'y':	
            if reg_action_top[i] == 'w':
                reg_map_output.write('\t\tcomRegRO(' + str(i) + ') <= \'0\';\n')
            else:
                reg_map_output.write('\t\tcomRegRO(' + str(i) + ') <= \'1\';\n')
    		
            if reg_action_top[i] == 'w':
                reg_map_output.write('\t\tcomRegIn(' + str(i) + ') <= comRegDataIn_MUX;\n')
            else:
                if reg_subcnt_top[i] == '':
                    reg_map_output.write('\t\tcomRegIn(' + str(i) + ') <= ' + reg_signal_top[i] + ';\n')
                else: 
                    reg_map_output.write('\t\tcomRegIn('+str(i) + ') <= ' + reg_signal_top[i] + '(' + str(int(reg_subcnt_top[i])) + ');\n')
            # if i > 127 and reg_default_config[i-128] != "":
                # reg_map_output.write('\t\tcomRegRD(' + str(i) + ') <= x\"' + reg_default_config[i-128] + '\";\n')
                # reg_map_output.write('\t\tcomRegMk(' + str(i) + ') <= x\"' + reg_mask_config[i-128] + '\";\n')
    
            # else:
            reg_map_output.write('\t\tcomRegRD(' + str(i) + ') <= x\"' + reg_default_top[i] + '\";\n')
            reg_map_output.write('\t\tcomRegMk(' + str(i) + ') <= x\"' + reg_mask_top[i] + '\";\n')
    
            if reg_action_top[i] == 'w':
                if reg_subcnt_top[i] == '':
                    reg_map_output.write('\t\t' + reg_signal_top[i] + ' <= comRegOut(' + str(i) + ');\n')
                else: 
                    reg_map_output.write('\t\t' + reg_signal_top[i] + '(' + str(int(reg_subcnt_top[i])) + ') <= comRegOut(' + str(i) + ');\n')
            reg_map_output.write('\n')    
            
    for i in range(len(reg_used_config)):
        if reg_used_config[i] == 'y':	
            if reg_action_config[i] == 'w':
                reg_map_output.write('\t\tcomRegRO(' + str(i+128) + ') <= \'0\';\n')
            else:
                reg_map_output.write('\t\tcomRegRO(' + str(i+128) + ') <= \'1\';\n')
    		
            if reg_action_config[i] == 'w':
                reg_map_output.write('\t\tcomRegIn(' + str(i+128) + ') <= comRegDataIn_MUX;\n')
            else:
                if reg_subcnt_config[i] == '':
                    reg_map_output.write('\t\tcomRegIn(' + str(i+128) + ') <= ' + reg_signal_config[i] + ';\n')
                else: 
                    reg_map_output.write('\t\tcomRegIn('+str(i+128) + ') <= ' + reg_signal_config[i] + '(' + str(int(reg_subcnt_config[i])) + ');\n')
            reg_map_output.write('\t\tcomRegRD(' + str(i+128) + ') <= x\"' + reg_default_config[i] + '\";\n')
            reg_map_output.write('\t\tcomRegMk(' + str(i+128) + ') <= x\"' + reg_mask_config[i] + '\";\n')
        
            if reg_action_config[i] == 'w':
                reg_map_output.write('\t\t' + 'ConfigCustomRegister(' + str(i) + ') <= comRegOut(' + str(i+128) + ');\n')
            reg_map_output.write('\n')        
    reg_map_output.write('\tend procedure;\n\n')
    
    #generates body of configCustomRegister procedure mapping the generic arrya to specific signals in config block
    reg_map_output.write('\tprocedure RegisterMapConfigBlock (\n')
    reg_map_output.write('\t\tsignal ConfigCustomRegister   : in customRegArray;\n')
    
    for i in range(len(reg_signal_name_config)):
        if i != len(reg_signal_name_config) - 1:
            if reg_signal_type_config[i] == '':
                reg_map_output.write('\t\tsignal ' + reg_signal_name_config[i] + '\t: ' + reg_signal_action_config[i] + ' std_logic_vector(REGISTER_SIZE - 1 downto 0);\n')	
            else: 
                reg_map_output.write('\t\tsignal ' + reg_signal_name_config[i] + '\t:' + reg_signal_action_config[i] + ' ' + reg_signal_type_config[i] + ';\n')
        else:
            if reg_signal_type_config[i] == '':
                reg_map_output.write('\t\tsignal ' + reg_signal_name_config[i] + '\t: ' + reg_signal_action_config[i] + ' std_logic_vector(REGISTER_SIZE - 1 downto 0)\n')	
            else: 
                reg_map_output.write('\t\tsignal ' + reg_signal_name_config[i] + '\t:' + reg_signal_action_config[i] + ' ' + reg_signal_type_config[i])	
    reg_map_output.write('\n\t) is \n \tbegin \n')
    
    for i in range(len(reg_used_config)):
        if reg_used_config[i] == 'y':	
            if reg_subcnt_config[i] == '':
                reg_map_output.write('\t\t' + reg_signal_config[i] + ' <= ConfigCustomRegister('+str(i)+');\n')
            else: 
                reg_map_output.write('\t\t' + reg_signal_config[i] + '(' + str(int(reg_subcnt_config[i])) + ') <= ConfigCustomRegister(' + str(i) + ');\n')
    		
    reg_map_output.write('\tend procedure;\n\n')
    reg_map_output.write('end reg_map_pkg;')
    reg_map_output.close()
    return 1
