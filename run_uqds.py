# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 18:01:42 2019
Description: Control of design flow for uQDS project

@author: anskocze
A.Skoczen, AGH-UST, FP&ACS; CERN, TE-MPE-EP - March 2019
A.Skoczen, AGH-UST, FP&ACS; CERN, TE-MPE-EP - July - September 2019: wide development 
A.Skoczen, AGH-UST, FP&ACS; CERN, TE-MPE-EP - January-February 2020 - further development
"""

import os  
import sys
import shutil
import datetime
import re
import igloo2 as ig2

print(sys.version)
if sys.version_info.major != 3:
    print("Wrong python version")
    exit()
    
if os.path.exists('.\\DOC'):
    sys.path.append('.\\DOC')
    import reg_map_pkg as rmp
else:
    print('No access to reg_map_pkg package')
    exit()

if os.path.exists('.\\constraints'):
    sys.path.append('.\\constraints')
    import gen_pdc_pkg as pdc
else:
    print('No access to gen_pdc_pkg package')
    exit()

def show_globals():
    path_tab = os.environ['PATH'].split(';')
    print('=====================================================')
    print('PATH:')
    for pp in path_tab:
        print(pp)
    print('=====================================================')
    print('source directory = ', src_dir)
    print('project name = ', prj_name)
    print("library name = ", lib_name)
    print("top name = ", top_name)
    if gitp:
        print("path to Git = ",gitp)
    else: 
        print("Git is not configured")
    print("path to Libero = ",liberop)
    if modelsimp: 
        print('path to ModelSim = ', modelsimp)
    else: 
        print("ModelSim is not configured")
    print("path to Synplify = ",synplifyp)
    """
    print('=====================================================')
    print('PATH:')
    for pp in path_tab:
        print(pp)
    print('=====================================================')
    """
    return True

""" load project config """
fname = '.\config.cfg'
if not os.path.exists(fname):
    print("Lack of ", fname, "file -> create it:")
    print("proj_name=")
    print("lib_name=")
    print("top_name=")
    print("source_dir=")
    print("tech=")
    exit()
    
with open(fname, 'r') as fin:
    lines = fin.readlines()
for line in lines:
    one_entry = line.splitlines()[0].split('=',2)
    #print(one_entry)
    if (one_entry[0] == 'source_dir'):
        src_dir = one_entry[1]
    if (one_entry[0] == 'proj_name'):
        prj_name = one_entry[1]
    if (one_entry[0] == 'lib_name'):
        lib_name = one_entry[1]
    if (one_entry[0] == 'top_name'):
        top_name = one_entry[1]

""" load PATH config """
fname = '.\path.cfg'
if not os.path.exists(fname):
    print("Lack of ", fname, "file -> create it:")
    print("Libero=")
    print("Synplify=")
    print("ModelSim=")
    print("Git=")
    exit()
    
modelsimp = ""
gitp = ""
with open(fname, 'r') as fin:
    lines = fin.readlines()
for line in lines:
    one_entry = line.splitlines()[0].split('=',2)
    if (one_entry[0] == 'Libero'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            liberop = one_entry[1]
            ig2.check_add_path(one_entry[0],one_entry[1])
            lver = ig2.find_between(liberop, '_v', '.')
        else:
            liberop = "Error: no Libero path"
    if (one_entry[0] == 'Synplify'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            synplifyp = one_entry[1]
            #check_add_path(one_entry[0],one_entry[1])
            ig2.check_add_path('Synopsys',one_entry[1])
        else:
            synplifyp = "Error: no Synplify path"
    if (one_entry[0] == 'ModelSim'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            modelsimp = one_entry[1]
            ig2.check_add_path(one_entry[0],one_entry[1])
        else:
            modelsimp = "Error: no ModelSim path"
    if (one_entry[0] == 'Git'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            gitp = one_entry[1]
            ig2.check_add_path(one_entry[0],one_entry[1])
        else:
            gitp = "Error: no Git path"

#if len(sys.argv) == 1:
#    ig2.script_help()
#    exit()

if os.path.isdir(os.path.join(src_dir,'config_blocks')):
    if len(sys.argv) == 1 or sys.argv[1] != "reg_map":
        config_block_file = os.path.join(src_dir, 'src\\config_block.vhd')
        if not os.path.exists(config_block_file):
            print('Lack of config block file', config_block_file)
            print('Assign a Config Block to your design: python run_uqds reg_map config_block_number')
            ig2.script_help()
            exit()
        cnt, cb_purpose = ig2.purpose_of_config_block(config_block_file, '@brief')
        if cnt != 1:
            print('Edit your Config Block VHDL file and add or modifie @brief line')
            ig2.script_help()
            exit()
else:
    cb_purpose = 'No config block in current design'

""" manage arguments """
if len(sys.argv) > 1:
    #############################################
    #synthesis
    if sys.argv[1] == "synth":
        if ig2.buildstamp(src_dir, 'buildstamp.vhd'):
            print("Built stamp")
        else:
            exit()
        if len(sys.argv) == 3:
            argtmr = sys.argv[2]
        else:
            argtmr = 'none'
        if not ig2.tmr(argtmr,lib_name,top_name):
            exit()
        if(ig2.workDir(src_dir) == 0):
            print("Warning: !!!")
        synth_resfile  = ig2.find_first_file_sub('.\\synplify', 'edn')
        t0 = datetime.datetime.now()
        print("Start time:", t0.strftime('%H:%M:%S'))
        print(cb_purpose)
        if not ig2.synthesize(synth_resfile, src_dir):
            exit()
        synth_resfile  = ig2.find_first_file_sub('.\\synplify', 'edn')
        if os.path.exists(synth_resfile):
            print("EDN ",synth_resfile," created")
        else:
            print("no EDN output from synthesis")
        sdc_resfile  = ig2.find_first_file_sub('.\\synplify', 'sdc')
        if os.path.exists(sdc_resfile):
            dst = os.path.join('.\\constraints',os.path.basename(sdc_resfile))
            shutil.copyfile(sdc_resfile,dst)
            print("SDC copied from synthesis directory to",dst)
        else:
            print("no SDC output from synthesis")
        dt = str(datetime.datetime.now()-t0)
        print("Total Run Time:",dt," of synthesis phase")
        srr_file = os.path.join('.\\synplify\\synthesis', os.path.basename(prj_name)+'.srr')
        if os.path.exists(srr_file):
            ig2.clocks(srr_file)
        else:
            print("no SRR logfile from synthesis")
        print('Design with Config Block:', cb_purpose)
        exit()
        #############################################
        #conver PPD to STAPL
    elif sys.argv[1] == "stapl":
        synth_resfile  = ig2.find_first_file_sub('.\\synplify', 'edn')
        ig2.export_stapl(synth_resfile, lver, top_name, prj_name)
        #############################################
		# generate PDC (and NDC in case at ver 12)
    elif sys.argv[1] == "pdc":
        if len(sys.argv) > 2:
            in_file = int(sys.argv[2])
        else:
            in_file = os.path.join('.\\constraints\\', os.path.basename(prj_name)+ig2.xls_ext+'.xlsx')
        print("Libero version:",lver)
        if pdc.gen_pdc(int(lver), in_file, top_name):
            print('Done')
        else:
            print('Failed')
            exit()
        #############################################
		# generate reg map package for specified config block from excel file and copy to lib folder
		# copies specified config block to main source dir
		# copies specified config block library to lib dir
    elif sys.argv[1] == "reg_map":
        if len(sys.argv) > 2:
            config_block_select = int(sys.argv[2])
        else:
            print('Lack of second argument')
            print('Usage: python run_uqds reg_map index_to_data_sheet_in_Excel_file')
            exit()
        gen = rmp.generate(config_block_select, '.\\DOC', '.\\hdl\\lib')
        if gen == 1:
            print('Package generated')
        else:
            print('Error: Package not generated')
            exit()
        cb_path = '.\\hdl\\config_blocks'
        config_block_file = os.path.join(cb_path, 'config_block_'+sys.argv[2]+'.vhd')
        config_block_lib_file = os.path.join(cb_path, 'config_block_'+sys.argv[2]+'_lib.vhd')
        if not os.path.exists(config_block_file):
            print('File', config_block_file, 'does not exist: reg_map_pkg is not synchronized with config_block !')
            cb_files = [f for f in os.listdir(cb_path) if os.path.isfile(os.path.join(cb_path, f))]
            print('Available files in', cb_path)
            for f in cb_files:
                print(f)
        elif not os.path.exists(config_block_lib_file):
            print('File', config_block_lib_file, 'does not exist: reg_map_pkg is not synchronized with config_block !')
            cb_files = [f for f in os.listdir(cb_path) if f.endswith("_lib.vhd") if os.path.isfile(os.path.join(cb_path, f))]
            print('Available lib files in', cb_path)
            for f in cb_files:
                print(f)
        else:
            fname = os.path.join('.\\hdl\\src', 'config_block.vhd')
            fname_lib = os.path.join('.\\hdl\\lib', 'config_block_lib.vhd')
            print('Synchronisation with config block:')
            # metadata preserved
            shutil.copy2(config_block_file, fname)
            print(config_block_file, '=>', fname)
            # metadata preserved
            shutil.copy2(config_block_lib_file, fname_lib)
            print(config_block_lib_file, '=>', fname_lib)
            pattern = '@brief'
            ig2.purpose_of_config_block(fname, pattern)
        exit()
        #############################################
        #displey revision number and built stamp
    elif sys.argv[1] == "rev":
        if ig2.buildstamp(src_dir, 'buildstamp.vhd'):
            print("Built stamp")
        else:
            exit()
        #############################################
        #deisplay config block number
    elif sys.argv[1] == "cb_nr":
        print('Current config block number is:', ig2.cfg_blk_nr())
        #############################################
        # Implement with Libero
    elif sys.argv[1] == "impl":
        if(ig2.workDir(src_dir) == 0):
            print("Warning: !!!")
            exit()
        synth_resfile  = ig2.find_first_file_sub('.\\synplify', 'edn')
        t0 = datetime.datetime.now()
        print("Start time:", t0.strftime('%H:%M:%S'))
        print('Design with Config Block:', cb_purpose)
        ig2.implement(synth_resfile, lver, src_dir, top_name, prj_name)
        dt = str(datetime.datetime.now()-t0)
        print("Total Run Time:",dt," of implemetation phase")
        tv_path = '.\\actel\\designer\\' + top_name
        if os.path.exists(tv_path):
            tv_files = [f for f in os.listdir(tv_path) if re.search(r'.timing_violations.', f)]
            #print(tv_files)
            ig2.time_violations(tv_files, tv_path)
            io_bank_file = os.path.join(tv_path, top_name + '_bankrpt.rpt')
            #print(io_bank_file)
            if ig2.io_func_rep(io_bank_file):
                iocomb_rep = os.path.join(tv_path, top_name + '_ioff.rpt')
                if os.path.exists(iocomb_rep): 
                    print('I/O Register Combining Summary is available in file ', iocomb_rep)
            """
            bits = os.path.join('.\\bitstream\\', prj_name + '.stp')
            if os.path.exists(bits):
                cb_nr = ig2.cfg_blk_nr(src_dir)
                if os.path.isdir(os.path.join('.\\','.git')):
                    gitver = ig2.take_rev_git()
                else:
                    gitver = 'no-git-proj'
                if cb_nr == 0:
                    newbits = os.path.join('.\\bitstream\\', prj_name + '_' + gitver + '.stp')
                else:
                    newbits = os.path.join('.\\bitstream\\', prj_name + '_' + str(cb_nr) + '_' + gitver + '.stp')
                os.rename(bits, newbits)
                print('STAPL file renamed to:', newbits)
            else:
                print('No STAPL file:', bits)
            """
        else:
            print('Debug mode - implemntation not done')
        print('Design with Config Block:', cb_purpose)
        #############################################
        #clean design 
    elif sys.argv[1] == "clean":
        ig2.clean()
        #############################################
        #clean only implementation and leave synthesis 
    elif sys.argv[1] == "clean_impl":
        ig2.clean_impl()
        #############################################
        #show congiguration parameters and paths
    elif sys.argv[1] == "show_cfg":
        show_globals()
        #############################################
        #display help
    elif sys.argv[1] == "cfg_help":
        ig2.script_help('cfg')
        #############################################
        #display help
    elif sys.argv[1] == "help":
        ig2.script_help('cmd')
        #############################################
        #display help
    else: ig2.script_help('cmd')
else: ig2.script_help()
