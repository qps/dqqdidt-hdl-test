library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

package buildstamp is
constant build_rev : std_logic_vector(31 downto 0) := x"zzzzzzzz";
end buildstamp;

package body buildstamp is 
end buildstamp;
