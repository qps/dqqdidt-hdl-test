--------------------------------------------------------------------------------
--! Company: CERN
--!
--! @File: uart_model.vhd
-- File history:
--      <Revision number>: <Date>: <Designer>: <Comments>
--				v1.0	:	28.02.2020	:	A.Skoczen	:	creation
--
--! @brief simple behavioural model of UART for simulation of UART connection at uQDS
-- Description: 
--				simple behaviouralmodel of UART
--
-- Targeted device: M2GL150
--! @Author: A.Skoczen AGH-UST, 
--
--------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
USE IEEE.numeric_std.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use ieee.std_logic_textio.all;
-- 
entity uart_model is 
	generic (samplefile : string := "cmd_uart.dat";
		len : integer :=8;
		nrSamples : integer := 96;
		baudrate : integer := 2500000);
	port (tx : out std_logic := '1';
		rx : in std_logic;
		enable : in boolean);
end entity;

architecture beh of uart_model is
constant half_period_int : integer := integer(1.0e9/(2.0*real(baudrate)));
constant half_period : time := 1 ns * half_period_int;		--3*60 ns;		--41666 ps;
type mem is array (0 to nrSamples) of std_logic_vector(len-1 downto 0);
signal receive, receive_latch, parity_byte : std_logic_vector(len - 1 downto 0) := (others => '0');
signal l : integer := 0;
--signal k : integer := len-1;
signal init, cs1 : std_logic := '0';
signal the_end, data_end : boolean := FALSE;
signal max	: integer := 0;
signal sdata : std_logic_vector(len-1 downto 0);
signal nrx : integer range 0 to 4 := 0;

--constant ec1_ini : integer := 1;
--constant ec0_ini : integer := len+1;
--signal nec1, pec1 : integer range 0 to 9 := ec1_ini;
--signal nec0, pec0 : integer range 0 to 9 := ec0_ini;
signal clk : std_logic := '0';
signal ccs : std_logic;

signal cs : std_logic := '1';
signal rx_ready : std_logic := '0';
--signal txi : std_logic := '1';
begin
init_mem: process
begin
	init <= '1';
	wait;
end process;

sampl1: process(init, ccs, rx_ready) 

variable inline	: LINE;
variable samples : mem;
variable indata	: std_logic_vector(len-1 downto 0);
variable k, i	: integer := 0;
file     memfile: text;
variable status	: file_open_status;

begin
--initialization at INIT_MEM 
if ( rising_edge(init) ) then
	file_open(status, memfile, samplefile, read_mode);
	if ( status=open_ok ) then
		while (( i <= nrSamples ) and ( not endfile(memfile) )) loop
			readline(memfile, inline);
			hread(inline, indata);
			samples(i) := indata;
			i := i + 1;
--			assert false report "init !" severity note;
		end loop;
		max <= i;
		assert ( false ) report "Memory initialized !" severity note;
	else
		assert ( samplefile'length = 0 )
		report "Failed to open memory initialization in read mode"
		severity note;
	end if;
	file_close(memfile);
end if;

if falling_edge(ccs) then
	if ( max > l ) then
		sdata <= samples(l);
	end if;
end if;

if falling_edge(ccs) or rising_edge(rx_ready) then
	if nrx = 0 then
		if ( max > l ) then
			l <= l + 1;
		end if;
	end if;
	if nrx = 4 then 
		l <= 0;
	end if;
end if;

if rising_edge(ccs) then
	if ( max < l ) then
		data_end <= true;
		assert ( false ) report "End of data" severity note;
	end if;
end if;
end process;

finish: process(clk) begin
if rising_edge(clk) then
	if max <= l then
		the_end <= TRUE;
	else
		the_end <= FALSE;
	end if;
end if;
end process;

clk <= not clk after half_period;

main_tx: process begin
	wait until rising_edge(clk);
	--wait for half_period/5;
	if not the_end then
		cs <= '0';
		tx <= '0';
		for i in 0 to 7 loop
			--wait for half_period/5;
			wait until rising_edge(clk);
			--wait for half_period/5;
			tx <= sdata(i);
		end loop;
		wait until rising_edge(clk);
		tx <= '1';
		wait for half_period/5;
		cs <= '1';
	end if;
	wait until rising_edge(clk);
end process;

main_rx: process  begin
	wait until falling_edge(rx);
	rx_ready <= '0';
	wait until falling_edge(clk);	-- in the middle of start bit
	--wait until rising_edge(clk);
	for k in 0 to 7 loop
		wait until falling_edge(clk);
		receive(k) <= rx;
	end loop;
	wait until rising_edge(clk);	-- at begining of stop bit
	rx_ready <= '1';
	wait until falling_edge(clk);
	receive <= (others => '0');
end process;

latch_rx:  process(rx_ready) begin
if rising_edge(rx_ready) then
	if nrx < 4 then
		nrx <= nrx + 1;
	else
		nrx <= 0;
	end if;
	receive_latch <= receive;
	parity_byte <= parity_byte xor receive;
end if;

end process;

--tx <= txi;
ccs <= cs when enable else '1';

end beh;
