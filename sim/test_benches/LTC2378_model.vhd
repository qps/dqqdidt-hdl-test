--------------------------------------------------------------------------------
--! Company: CERN
--!
--! @File: LTC2378_model.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      v0.1	03/07/2019  start of development of model of ADC interface LTC2378
--		v1.0	31/07/2019	isolation delay added for ISO 7842
--		v1.1	25/05/2020	isolation delay added for ADUM 263 N1 BRIZ
--      
--! @brief basic command interface register 
-- Description: 
--	simulation behavioral model of LTC2378 ADC interface for use in testbenches  				
--
-- Targeted : testbench
--! @Author: Andrzej Skoczen AGH-UST
--
--------------------------------------------------------------------------------
library IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
use std.textio.all;
use ieee.std_logic_textio.all;

entity LTC2378_model is
	generic (samplefile : string := "signalInp.dat";
		len : integer :=8;
		nrSamples : integer := 96;
		isolator : string := "adum");	-- four letters identifier of isolator: "adum" or "iso "
	port(
		cnv		: IN std_logic;
		busy	: OUT std_logic := '0';
		sck		: IN std_logic;
		sdo		: OUT std_logic
	);
end entity;

architecture behavioral of LTC2378_model is 
	procedure Message ( str : string ) is
		variable buf : LINE;
	begin
		write(buf, str);
		writeline(output, buf);
	end;
	
signal data : std_logic_vector(19 downto 0) := x"CCCCC";
signal i : integer range 0 to 19;
signal d_cnv, d_sck, busy_nd, sdo_nd : std_logic;

type mem is array (0 to nrSamples) of std_logic_vector(len-1 downto 0);
--signal receive, receive_latch : std_logic_vector(len - 1 downto 0) := x"00";
signal l : integer := 0;
--signal k : integer := len-1;
signal init : std_logic := '0';
signal data_end : boolean := FALSE;
signal max	: integer := 0;
--signal sdata : std_logic_vector(len-1 downto 0);
signal samples : mem;
constant iso_prop_del : time := 16 ns;
constant adum_prop_del : time := 14 ns;		--max value for supply 3.3V and 2.5V

begin
init_mem: process
begin
	init <= '1';
	wait;
end process;

sampl1: process(init, cnv) 

variable inline	: LINE;
--variable samples : mem;
variable indata	: std_logic_vector(len-1 downto 0);
variable k, i	: integer := 0;
file     memfile: text;
variable status	: file_open_status;

begin
--initialization at INIT_MEM 
if ( rising_edge(init) ) then
	file_open(status, memfile, samplefile, read_mode);
	if ( status=open_ok ) then
		while (( i <= nrSamples ) and ( not endfile(memfile) )) loop
			readline(memfile, inline);
			hread(inline, indata);
			samples(i) <= indata;
			i := i + 1;
--			assert false report "init !" severity note;
		end loop;
		max <= i;
		assert ( false ) report "Memory initialized !" severity note;
	else
		assert ( samplefile'length = 0 )
		report "Failed to open memory initialization in read mode"
		severity note;
	end if;
	Message(samplefile);
	file_close(memfile);
end if;

 if falling_edge(cnv) then
	 if ( max > l ) then
		 data <= samples(l);
		 l <= l + 1;
	 end if;
 end if;
 if rising_edge(cnv) then
	 if ( max <= l ) then
		 data_end <= true;
		 Message("End of data - " & samplefile);
		 --assert ( false ) report "End of data" severity note;
	 end if;
 end if;
end process;

choose_isolator_delay : case isolator generate 
	when "iso " => 
--propoagation delay introduced by ISO 7842 
d_cnv <= cnv after iso_prop_del;
d_sck <= transport sck after iso_prop_del;
busy <= busy_nd after iso_prop_del;
sdo <= sdo_nd after iso_prop_del;
assert ( false ) report "ISO 7842 max dealy" severity note;
	when "adum" => 
--propoagation delay introduced by ADUM 263
d_cnv <= cnv after adum_prop_del;
d_sck <= transport sck after adum_prop_del;
busy <= busy_nd after adum_prop_del;
sdo <= sdo_nd after adum_prop_del;
assert ( false ) report "ADUM 263 max dealy" severity note;
	when others =>
		d_cnv <= cnv;
		d_sck <= sck;
		busy <= busy_nd;
		sdo <= sdo_nd;
		assert ( false ) report "without isolatir, delay = 0" severity note;
end generate;

busy_generator : process begin
	wait until rising_edge(d_cnv);
	wait for 5 ns;
	busy_nd <= '1';
	wait for 650 ns;
	busy_nd <= '0';
end process;

bits_out : process begin
	wait until falling_edge(busy_nd);
	wait for 5 ns;
	i <= 19;
	sdo_nd <= data(19);
	for x in 18 downto 0 loop
		wait until rising_edge(d_sck);
		wait for 8 ns;
		i <= x;
		sdo_nd <= data(x);	
	end loop;
	wait until rising_edge(d_sck);
	wait for 8 ns;
	sdo_nd <= 'X';
end process;

-- bit_counter : process begin
	-- wait until falling_edge(busy_nd);
	-- i <= 19;
	-- wait for 6 ns;
	-- i <= 18;
	-- for x in 17 downto 0 loop
		-- wait until rising_edge(d_sck);
		-- wait for 9 ns;	
		-- i <= x;
	-- end loop;
-- end process;

end behavioral;
