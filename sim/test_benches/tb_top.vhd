--------------------------------------------------------------------------------
--! Company: CERN
--!
--! @File: tb_top.vhd for comm_fast project DQQDIDT
-- File pre-history:
--      <Revision number>: <Date>: <Designer>: <Comments>
--				v1.0	:	2.9.2019	:	A.Skoczen	:	creation 
-- File history:
--      <Revision number>: <Date>: <Designer>: <Comments>
--				v1.0	:	18.2.2020	:	A.Skoczen	:	copy & modification 
--      
--! @brief Test-bench for fast readout top 
-- Description: 
--				
--! @Author: Andrzej Skoczen, AGH-UST
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.txt_util.all;

library modelsim_lib;
use modelsim_lib.util.ALL;

library UQDSlib;
use UQDSlib.UQDSlib.all;
use UQDSLib.reg_map_pkg.all;
use UQDSLib.config_block_lib.all;

entity tb_top is
	generic(cnv_nr : natural;
		nr_of_channels : natural := 5;
		dat_file_name : string := "signalInp.dat";
		sinus_input : integer range 0 to 1 := 0;
		freq : real := 30.0);
end entity;

architecture test of tb_top is
constant half_period : time := 12.5 ns;
constant PACKET_SIZE : natural := 8 + CHANNELS_OUT_FR * 4;
constant WRITE_DEPTH : natural := 10;
constant READ_DEPTH : natural := 10;

signal clk : std_logic := '0';
signal nrst : std_logic := '1';
signal fast_clk	: std_logic;
signal txe, rxf, rd, wr, oe, FastReadoutDataRdy	: std_logic;
signal data_out : std_logic_vector(7 downto 0);
signal data_in	: data8Array(PACKET_SIZE - 9 downto 0);
signal FastReadoutData    : outputChannelsArray;
signal cmp : boolean;
signal to_compare : data8Array(PACKET_SIZE-9 downto 0);
signal cnt_dgen : natural := 0;
signal nr_sample : integer := 0;
signal wptr : std_logic_vector(WRITE_DEPTH DOWNTO 0);
signal rptr : std_logic_vector(READ_DEPTH DOWNTO 0);
signal ptr_diff : unsigned(READ_DEPTH DOWNTO 0);
signal cnv, busy, sck, sdo : std_logic_vector(CHANNELS_IN - 1 downto 0);
signal uart_lines : std_logic_vector(1 downto 0);	--at uut side: 0 - RX, 1 - TX
signal the_end : boolean;

type dataOutArray is array (CHANNELS_IN_MAX - 1 downto 0) of std_logic_vector(19 downto 0);
signal adc_inside               : dataOutArray;
signal adc_stb         : std_logic_vector(CHANNELS_IN - 1 downto 0);
signal scaled_stb         : std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
signal adc_in : std_logic_vector(19 downto 0);
signal scaled_dat          : channelsArray(CHANNELS_IN_MAX - 1 downto 0);
signal scaled_int, adc_int : integer;
signal scaling_factor : real;

--	constant CHANNELS_OUT          : integer := 13;
--	constant REGISTER_SIZE      : INTEGER := 32;
--	constant CHANNELS_OUT_FR          : integer := 9;
--	type outputChannelsArray is array (CHANNELS_OUT - 1 downto 0) of std_logic_vector(REGISTER_SIZE - 1 downto 0);	13*32/8 = 13*4 = 52 bytes
--	type data8Array is array (natural range <>) of std_logic_vector(7 downto 0);	PACKET_SIZE = 44 bytes

component uart_model is 
	generic (samplefile : string := "cmd_uart.dat";
		len : integer :=8;
		nrSamples : integer := 96;
		baudrate : integer := 2500000);
	port (tx : out std_logic := '1';
		rx : in std_logic;
		enable : in boolean);
end component;

component LTC2378_model is
	generic (samplefile : string := "signalInp.dat";
		len : integer :=8;
		nrSamples : integer := 96;
		isolator : string := "adum");		-- four letters identifier of isolator: "adum" or "iso "
	port(
		cnv		: IN std_logic;
		busy	: OUT std_logic;
		sck		: IN std_logic;
		sdo		: OUT std_logic
	);
end component;

component Top is
	port(
		POWER_ON_RESET 	: IN    std_logic; --power on reset
		CLK            	: IN    std_logic; --main clock
		DEBUG_LED      	: OUT   std_logic_vector(7 downto 0);
		SPARE_A			: OUT   std_logic_vector(7 downto 0);
		SPARE_B			: OUT   std_logic_vector(7 downto 0);
		SPARE_C			: OUT   std_logic_vector(1 downto 0);
		
		--Connections to the FLASH flash memory
		FLASH_HOLD_A  	: OUT   std_logic;
		FLASH_WP_A    	: OUT   std_logic;
		FLASH_SO_A     	: IN    std_logic;
		FLASH_SI_A     	: OUT   std_logic;
		FLASH_CLK_A    	: OUT   std_logic;
		FLASH_CS_A    	: OUT   std_logic;
		
		PORT_ADC0_CNV  : OUT   std_logic_vector(CHANNELS_IN - 1 downto 0);
		PORT_ADC0_BUSY : IN    std_logic_vector(CHANNELS_IN - 1 downto 0);
		PORT_ADC0_SCK  : OUT   std_logic_vector(CHANNELS_IN - 1 downto 0);
		PORT_ADC0_SDO  : IN    std_logic_vector(CHANNELS_IN - 1 downto 0);
		
		BUFFER_CH1_nOE 	: OUT	std_logic;
		
		-- Connected to FTDI module B (FT232 pin 13 & 14) serving as RX and TX in RS232 mode
		FTDI_B_D       : INOUT std_logic_vector(1 downto 0);

		SPI_MISO_FPGA 	: OUT std_logic;
		SPI_MOSI_FPGA 	: IN std_logic;
		SPI_nCS_FPGA 	: IN std_logic;
		SPI_SCK_FPGA 	: IN std_logic
	);
end component;

function func_dat_file_name(i: natural; fname : string) return string is
--if i > 9 it does not work !!
variable outstr : string(1 to fname'length + 1);
begin
	outstr := fname(1 to fname'length-4) & natural'image(i) & fname(fname'length-3 to fname'length);
	return outstr;
end;

	procedure Message ( str : string ) is
		variable buf : LINE;
	begin
		write(buf, str);
		writeline(output, buf);
	end;
	
	function conv_to_string(val : data8Array) return string is
	variable str_val : string(1 to 2*(val'length));
	begin
		for i in val'range loop
			--str_val(str_val'length-2*i-1 to str_val'length-2*i) := hstr(val(i));
			str_val(2*i+1 to 2*(i+1)) := hstr(val(i));
		end loop;
		return str_val;
	end;
	
begin

clk <= not clk after half_period;

power_on_reset: process begin
	nrst <= '1';
	wait until rising_edge(clk);
	wait until falling_edge(clk);
	nrst <= '0';
	for i in 0 to 4 loop wait until rising_edge(clk); end loop;
	wait for half_period;
	nrst <= '1';
	wait;
end process;

uart_connection : uart_model 
	generic map (samplefile => "./data/cmd_uart1.dat", len => 8, nrSamples => 6)
	port map (tx => uart_lines(0), rx => uart_lines(1), enable => true);		--at uut side: 0 - RX, 1 - TX

--data_from_file : if sinus_input = 0 generate
adc_set : for i in 0 to CHANNELS_IN - 1 generate		--CHANNELS_IN_MAX		nr_of_channels
	adc_model : LTC2378_model
		generic map(samplefile => func_dat_file_name(i, dat_file_name), len => 20, nrSamples => cnv_nr, isolator => "adum")
		port map(
			cnv	=> cnv(i), busy => busy(i),	sck	=> sck(i), sdo => sdo(i)
			);
end generate;
the_end <= <<signal .tb_top.adc_set(0).adc_model.data_end : boolean>>;
--end generate;

uut: top
		generic map(PACKET_SIZE => PACKET_SIZE)
		port map (POWER_ON_RESET => nrst, CLK => clk,           	
		DEBUG_LED => open, SPARE_A => open,	SPARE_B => open, SPARE_C => open,		
		FLASH_HOLD_A => open, FLASH_WP_A => open, FLASH_SO_A => '0', FLASH_SI_A => open, FLASH_CLK_A => open, FLASH_CS_A => open,     
		
		PORT_ADC0_CNV => cnv, PORT_ADC0_BUSY => busy, PORT_ADC0_SCK => sck, PORT_ADC0_SDO => sdo, 
		
		BUFFER_CH1_nOE => open, FTDI_B_D => uart_lines,
		SPI_MISO_FPGA => open, SPI_MOSI_FPGA => '0', SPI_nCS_FPGA => '0', SPI_SCK_FPGA =>  '0');

adc_in <= <<signal .tb_top.adc_set(0).adc_model.data : std_logic_vector(19 downto 0)>>;
adc_inside <= <<signal .tb_top.uut.adc_data : dataOutArray>>;
adc_stb <= 	<<signal .tb_top.uut.adc_data_ready : std_logic_vector(CHANNELS_IN - 1 downto 0)>>;
scaled_stb <= 	<<signal .tb_top.uut.scaled_data_rdy : std_logic_vector(CHANNELS_IN_MAX - 1 downto 0)>>;
scaled_dat <= <<signal .tb_top.uut.scaled_data : channelsArray(CHANNELS_IN_MAX - 1 downto 0)>>;

comparison_of_adc_data: process(adc_stb(0))
begin
	if falling_edge(adc_stb(0)) then
		if adc_in /= adc_inside(0) then
			Message("Data WRONG at " & time'image(now));
			Message("Sent:     " & hstr(adc_in));
			Message("Received: " & hstr(adc_inside(0)));
		end if;
	end if;
end process;

factor_for_scaled_data: process(scaled_stb(0))
begin
	if falling_edge(scaled_stb(0)) then	
		scaled_int <= to_integer(unsigned(scaled_dat(0)));
		adc_int <= to_integer(unsigned(adc_in));
		scaling_factor <= real(scaled_int) / real(adc_int);
		--end if;
	end if;
end process;

end architecture;
