--------------------------------------------------------------------------------
--! Company: CERN
--!
--! @File: tb_uart_model.vhd 
-- File history:
--      <Revision number>: <Date>: <Designer>: <Comments>
--				v1.0	:	28.2.2020	:	A.Skoczen	:	creation 
--      
--! @brief Test-bench for UART readout  
-- Description: 
--				
--! @Author: Andrzej Skoczen, AGH-UST
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.txt_util.all;

library modelsim_lib;
use modelsim_lib.util.ALL;

entity tb_uart_model is
	--generic();
end entity;

architecture test of tb_uart_model is

signal uart_lines : std_logic_vector(1 downto 0);	--at uart_connection_2 side: 0 - RX, 1 - TX

component uart_model is 
	generic (samplefile : string := "cmd_uart.dat";
		len : integer :=8;
		nrSamples : integer := 96;
		baudrate : integer := 2500000);
	port (tx : out std_logic := '1';
		rx : in std_logic;
		enable : in boolean);
end component;

begin

uart_connection_1 : uart_model 
	generic map (samplefile => "./data/cmd_uart1.dat", len => 8, nrSamples => 6)
	port map (tx => uart_lines(0), rx => uart_lines(1), enable => true);		

uart_connection_2 : uart_model 
	generic map (samplefile => "./data/cmd_uart2.dat", len => 8, nrSamples => 6)
	port map (tx => uart_lines(1), rx => uart_lines(0), enable => true);		
	
end architecture;
