


vsim  work.tb_uart_model -lib work 

quietly set data_clk_int [examine /tb_uart_model/uart_connection_1/half_period_int]
puts "UART clk (int): $data_clk_int"

quietly set data_clk_time [examine /tb_uart_model/uart_connection_1/half_period]
puts "UART clk: $data_clk_time"

quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_uart_model/uart_connection_1/clk
add wave -noupdate /tb_uart_model/uart_connection_2/clk
add wave -noupdate /tb_uart_model/*
add wave -noupdate /tb_uart_model/uart_connection_1/tx
add wave -noupdate /tb_uart_model/uart_connection_1/rx
add wave -noupdate /tb_uart_model/uart_connection_1/cs
add wave -noupdate /tb_uart_model/uart_connection_1/rx_ready
add wave -noupdate /tb_uart_model/uart_connection_1/receive
add wave -noupdate /tb_uart_model/uart_connection_1/receive_latch
add wave -noupdate /tb_uart_model/uart_connection_1/l
add wave -noupdate /tb_uart_model/uart_connection_1/sdata
add wave -noupdate /tb_uart_model/uart_connection_2/tx
add wave -noupdate /tb_uart_model/uart_connection_2/rx
add wave -noupdate /tb_uart_model/uart_connection_2/cs
add wave -noupdate /tb_uart_model/uart_connection_2/rx_ready
add wave -noupdate /tb_uart_model/uart_connection_2/receive
add wave -noupdate /tb_uart_model/uart_connection_2/receive_latch
add wave -noupdate /tb_uart_model/uart_connection_2/l
add wave -noupdate /tb_uart_model/uart_connection_2/sdata
add wave -noupdate /tb_uart_model/uart_connection_1/the_end

add mem /tb_uart_model/uart_connection_1/sampl1/samples -a decimal -d hexadecimal
add mem /tb_uart_model/uart_connection_2/sampl1/samples -a decimal -d hexadecimal

run 20 us
view -undock wave
wave zoomfull
