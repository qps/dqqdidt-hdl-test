quietly set ACTELLIBNAME smartfusion2
quietly set PROJECT_EXTIR "./sim"
quietly set nch 3
quietly set file_template_str "./data/adc_3chboard.dat"


#Hz
quietly set freq 300.0	
#ms			
quietly set period [format %f [expr {1000/$freq}]]	
quietly set simtime [expr 2*$period]
	
quietly set gen_sinus 0
if { $gen_sinus == 1 } {
	puts "Harmonic input signal frequency $freq Hz"
	puts "Requested simulation time is $simtime ms"
} else {
	puts "Input waveforms from files:"
	quietly set file_template [split $file_template_str {}]
	#puts $file_template_str
	#puts $file_template
	quietly set pos [lsearch -start 2 $file_template .]
	#puts "Position: $pos"
	for {set i 0} {$i < $nch} {incr i} {
		quietly set file [join [linsert $file_template $pos $i] {}]
		if {[file exists $file]} {
			quietly set fid [open $file r]
			quietly set data [read $fid [file size $file]]
			quietly set lineCount [llength [split $data "\n"]]
			puts "$i : $file length $lineCount"
		} else {
			puts "$file does not exists"
		}
	}
	#puts "Data $file has $lineCount lines"
}

vsim  work.tb_top -lib work -L UQDSLib -L smartfusion2 -t 1ps -suppress 8684,8683 -g/tb_top/nr_of_channels=$nch -g/tb_top/dat_file_name=$file_template_str -g/tb_top/cnv_nr=$lineCount -g/tb_top/sinus_input=$gen_sinus -g/tb_top/freq=$freq
# ** Warning: (vsim-8684) No drivers exist on out port /tb_top/uut/config_block_inst/RegMUXData(13)(5), and its initial value is not used.
# Therefore, simulation behavior may occur that is not in compliance with
# the VHDL standard as the initial values come from the base signal /reg_map_pkg/ChannelData(13)(5).

# ** Warning: (vsim-8683) Uninitialized out port /tb_top/adc_set(0)/adc_model/busy has no driver.
# This port will contribute value (U) to the signal network.
# ** Warning: (vsim-8683) Uninitialized out port /tb_top/adc_set(0)/adc_model/sdo has no driver.
# This port will contribute value (U) to the signal network.

quietly set StdArithNoWarnings 1
quietly set NumericStdNoWarnings 1

quietly WaveActivateNextPane {} 0
onerror {resume}

quietly set n_ch [examine -d /tb_top/nr_of_channels]
puts "Number of channels (tb): $n_ch"

quietly set n_ch_max [examine -d /uqdslib/CHANNELS_IN_MAX]
puts "Number of channels Max: $n_ch_max"

quietly set n_ch_real [examine -d /config_block_lib/CHANNELS_IN]
puts "Number of channels (config block): $n_ch_real"

#mux_data:
# 0 - inversion
# 3 - 1 - decimation factor
# 4 - median
# 5 - avg filter
# 6 - corrector 
# 7 - offset
#dec4 + mean_ram_filter
#force -freeze /tb_top/uut/config_block_inst/data_processing(0)/channel_data_processing/mux_data 2#00100010 {0 ps}
#no decimation + no mean_ram_filter
force -freeze /tb_top/uut/config_block_inst/data_processing(0)/channel_data_processing/mux_data 2#00000000 {0 ps}
#force -freeze /reg_map_pkg/ChannelGenericA(7:0)(0) 2#00100010 {1 ps}
#force -freeze /tb_top/uut/genComRegs(128)/comRegs/REG(7:0) 2#00100010 {1 ps}
#small value for dicrimination time for AP2 qlogic 0


quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_top/uut/channel_direct_scaler(0)
add wave -noupdate /tb_top/uut/pipeline(0)/channel_scaler_20_to_32/coeff
add wave -noupdate /tb_top/scaling_factor
add wave -noupdate /tb_top/adc_set(0)/adc_model/data
#add wave -noupdate /tb_top/*
add wave -noupdate /tb_top/clk
#add wave -noupdate /tb_top/power_rst
#add wave -noupdate /tb_top/rst_sw
add wave -noupdate /tb_top/cnv
add wave -noupdate /tb_top/busy
add wave -noupdate /tb_top/sck
add wave -noupdate /tb_top/sdo
add wave -noupdate /tb_top/the_end
#add wave -noupdate /tb_top/USB2_cntr
#add wave -noupdate /tb_top/USB2_data

add wave -noupdate /tb_top/uut/config_block_inst/BufferMUXData
add wave -noupdate /tb_top/uut/config_block_inst/PMBufferMask
add wave -noupdate /tb_top/uut/FReadoutMUXDataRdy
add wave -noupdate /tb_top/uut/TriggerLines
add wave -noupdate /tb_top/uut/TriggerPM
add wave -noupdate /tb_top/uut/trigger_postmortem
#add wave -noupdate /tb_top/uut/config_block_inst/bridge_AP2/log_out
#add wave -noupdate /tb_top/uut/config_block_inst/bridge_AP2/log_out_arr
#add wave -noupdate /tb_top/uut/config_block_inst/bridge_AP2/genQDlogic(1)/quench_logic/discrim/lsig
#add wave -noupdate /tb_top/uut/config_block_inst/bridge_AP2/genQDlogic(1)/quench_logic/discrim/gate
#add wave -noupdate /tb_top/uut/config_block_inst/bridge_AP2/genQDlogic(0)/quench_logic/discrim/lsig
#add wave -noupdate /tb_top/uut/config_block_inst/bridge_AP2/genQDlogic(0)/quench_logic/discrim/gate

#add wave -noupdate /tb_top/uut/config_block_inst/bridge_AP1/log_out
#add wave -noupdate /tb_top/uut/config_block_inst/bridge_symmetric/log_out
add wave -noupdate -radix decimal /tb_top/uut/config_block_inst/DynThres
add wave -noupdate -radix decimal /tb_top/uut/config_block_inst/DynDiscr
#add wave -noupdate /tb_top/uut/sync_out_pulse_init
#add wave -noupdate /tb_top/model_ftdi/st_reg
#add wave -noupdate /tb_top/model_ftdi/header_found

add wave -noupdate /tb_top/uut/rst
add wave -noupdate /tb_top/uut/nrst
add wave -noupdate /tb_top/uut/RST_FF3 
add wave -noupdate /tb_top/uut/com_reset 
#add wave -noupdate /tb_top/uut/PRST
add wave -noupdate /tb_top/uut/adc_start_conv
add wave -noupdate /tb_top/uut/adc_clock_counter
add wave -noupdate /tb_top/uut/adc_data_ready(0)
add wave -noupdate /tb_top/uut/adc_data(0)
add wave -noupdate /tb_top/uut/scaled_data(0)
add wave -noupdate /reg_map_pkg/ChannelData(0)
#add wave -noupdate /tb_top/uut/config_block_inst/bridge_AP2/inA
#add wave -noupdate /tb_top/uut/config_block_inst/bridge_AP2/inB
#add wave -noupdate /tb_top/uut/config_block_inst/bridge_AP2/outAB

#add wave -noupdate -format Analog-Step -height 74 -max 519045.0 -min -519045.0 -radix decimal /tb_top/uut/adc_data(0)
add wave -noupdate -format Analog-Step -height 74 -max 200000.00000000003 -min 100.0 -radix decimal /tb_top/uut/adc_data(0)
add wave -noupdate -format Analog-Step -height 74 -max 200000.00000000003 -min 100.0 -radix decimal /tb_top/uut/adc_data(1)
add wave -noupdate -format Analog-Step -height 74 -max 200000.00000000003 -min 100.0 -radix decimal /tb_top/uut/adc_data(2)
#add wave -noupdate -format Analog-Step -height 74 -max 200000.00000000003 -min 100.0 -radix decimal /tb_top/uut/adc_data(3)
#add wave -noupdate -format Analog-Step -height 74 -max 200000.00000000003 -min 100.0 -radix decimal /tb_top/uut/adc_data(4)

add wave -noupdate /tb_top/uut/scaled_data_rdy(0)
#add wave -noupdate -format Analog-Step -height 74 -max 415235200.0 -min -415235200.0 -radix decimal /tb_top/uut/scaled_data(0)
add wave -noupdate -format Analog-Step -height 74 -max 160000000.0 -radix decimal /tb_top/uut/scaled_data(0)
add wave -noupdate -format Analog-Step -height 74 -max 160000000.0 -radix decimal /tb_top/uut/scaled_data(1)
add wave -noupdate -format Analog-Step -height 74 -max 160000000.0 -radix decimal /tb_top/uut/scaled_data(2)
#add wave -noupdate -format Analog-Step -height 74 -max 160000000.0 -radix decimal /tb_top/uut/scaled_data(3)
#add wave -noupdate -format Analog-Step -height 74 -max 160000000.0 -radix decimal /tb_top/uut/scaled_data(4)

add wave -noupdate /tb_top/uut/config_block_inst/ChannelsScaledData(0)
add wave -noupdate /tb_top/uut/config_block_inst/data_processing(0)/channel_data_processing/Data_in
add wave -noupdate /tb_top/uut/config_block_inst/data_processing(0)/channel_data_processing/mux_data 
add wave -noupdate /tb_top/uut/config_block_inst/data_processing(0)/channel_data_processing/Dmux_data_out
#add wave -noupdate -format Analog-Step -height 74 -max 415211000.0 -min -415162800.0 -radix decimal /tb_top/uut/config_block_inst/data_processing(0)/channel_data_processing/Dmux_data_out
add wave -noupdate -format Analog-Step -height 74 -max 159880000.0 -radix decimal /tb_top/uut/config_block_inst/data_processing(0)/channel_data_processing/Dmux_data_out
add wave -noupdate -format Analog-Step -height 74 -max 159880000.0 -radix decimal /tb_top/uut/config_block_inst/data_processing(1)/channel_data_processing/Dmux_data_out
add wave -noupdate -format Analog-Step -height 74 -max 159880000.0 -radix decimal /tb_top/uut/config_block_inst/data_processing(2)/channel_data_processing/Dmux_data_out
#add wave -noupdate -format Analog-Step -height 74 -max 159880000.0 -radix decimal /tb_top/uut/config_block_inst/data_processing(3)/channel_data_processing/Dmux_data_out
#add wave -noupdate -format Analog-Step -height 74 -max 159880000.0 -radix decimal /tb_top/uut/config_block_inst/data_processing(4)/channel_data_processing/Dmux_data_out

#add wave -noupdate /tb_top/uut/config_block_inst/data_processing(4)/channel_data_processing/Dmux_data_out
#add wave -noupdate /tb_top/uut/config_block_inst/data_processing(4)/channel_data_processing/Data_in
#add wave -noupdate /tb_top/uut/config_block_inst/data_processing(4)/channel_data_processing/d_inv_mux
#add wave -noupdate /tb_top/uut/config_block_inst/data_processing(4)/channel_data_processing/d_dec_mux
#add wave -noupdate /tb_top/uut/config_block_inst/data_processing(4)/channel_data_processing/d_f_median_mux
#add wave -noupdate /tb_top/uut/config_block_inst/data_processing(4)/channel_data_processing/d_f0_mux
add wave -noupdate /tb_top/uut/config_block_inst/channel_mean_depth_int
add wave -noupdate /tb_top/uut/config_block_inst/data_processing(0)/channel_data_processing/d_ready_out
#add wave -noupdate /tb_top/uut/config_block_inst/data_processing(4)/channel_data_processing/d_ready_out
add wave -noupdate /tb_top/uut/config_block_inst/channel_filter_cfg(0)
add wave -noupdate /reg_map_pkg/ChannelGenericA(0)(7:0)
add wave -noupdate /reg_map_pkg/ConfigCustomRegister(0)
add wave -noupdate /reg_map_pkg/comRegOut(128)
add wave -noupdate /tb_top/uut/genComRegs(128)/comRegs/REG
add wave -noupdate /tb_top/uut/config_block_inst/data_processing(0)/channel_data_processing/sampleclock
add wave -noupdate /tb_top/uut/config_block_inst/data_processing(0)/channel_data_processing/d_inv_mux
add wave -noupdate /tb_top/uut/config_block_inst/data_processing(0)/channel_data_processing/d_dec_mux
#add wave -noupdate /tb_top/uut/pipeline(0)/channel_adc/current_state
add wave -noupdate /tb_top/uut/pipeline(0)/channel_adc/start_conv
add wave -noupdate /tb_top/uut/pipeline(0)/channel_adc/cnt_read
#add wave -noupdate /tb_top/uut/pipeline(0)/channel_adc/cnt_wait
add wave -noupdate /tb_top/uut/pipeline(0)/channel_adc/busy
add wave -noupdate /reg_map_pkg/ChannelGenericA
#add wave -noupdate /tb_top/adc_set(0)/adc_model/*
#add wave -noupdate /tb_top/adc_set(0)/adc_model/cnv
#add wave -noupdate /tb_top/adc_set(0)/adc_model/busy
#add wave -noupdate /tb_top/adc_set(0)/adc_model/sck
#add wave -noupdate /tb_top/adc_set(0)/adc_model/sdo
add wave -noupdate /tb_top/adc_set(0)/adc_model/data
#add wave -noupdate /tb_top/adc_set(0)/adc_model/i
#add wave -noupdate /tb_top/uut/LED

add wave -noupdate /tb_top/uart_connection/sdata
add wave -noupdate /tb_top/uart_connection/receive_latch
add wave -noupdate /tb_top/uut/ComunicationInterface/byte_counter
add wave -noupdate /tb_top/uut/ComunicationInterface/state
add wave -noupdate /tb_top/uut/ComunicationInterface/clk
add wave -noupdate /tb_top/uut/ComunicationInterface/SPIuartDataTX 
add wave -noupdate /tb_top/uut/ComunicationInterface/reg_value
add wave -noupdate /tb_top/uut/ComunicationInterface/uartTXRdy
add wave -noupdate /tb_top/uut/ComunicationInterface/uartRXRdy
add wave -noupdate /tb_top/uut/ComunicationInterface/uartDataRX
add wave -noupdate /tb_top/uut/ComunicationInterface/uartComTX
add wave -noupdate /tb_top/uut/ComunicationInterface/uartCom/xmit_pulse
add wave -noupdate /tb_top/uut/ComunicationInterface/uartCom/baud_clock
add wave -noupdate /tb_top/uart_lines
add wave -noupdate /tb_top/uart_connection/rx
add wave -noupdate /tb_top/uart_connection/rx_ready
add wave -noupdate /tb_top/uart_connection/receive
add wave -noupdate /tb_top/uart_connection/clk
add wave -noupdate /tb_top/uart_connection/receive_latch
add wave -noupdate /tb_top/uart_connection/parity_byte
add wave -noupdate /tb_top/uart_connection/l
add wave -noupdate /tb_top/uart_connection/nrx
add wave -noupdate /tb_top/uart_connection/the_end

if { $gen_sinus == 1 } {
add wave -noupdate /tb_top/dat_in
add wave -noupdate /tb_top/data_from_sinus_function/parallel_2_serial/data_in	
add wave -noupdate /tb_top/data_from_sinus_function/parallel_2_serial/cnv
add wave -noupdate /tb_top/data_from_sinus_function/analysis_of_input_waveform/max
add wave -noupdate /tb_top/data_from_sinus_function/analysis_of_input_waveform/min
add wave -noupdate /tb_top/data_from_sinus_function/analysis_of_output_waveform/max
add wave -noupdate /tb_top/data_from_sinus_function/analysis_of_output_waveform/min
}
add mem /reg_map_pkg/ChannelGenericA -a hexadecimal -d hexadecimal

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {5241935 ps} 0}
configure wave -namecolwidth 216
configure wave -valuecolwidth 158
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
#configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {105 us}


update
if { $gen_sinus == 1 } {
	run $simtime ms
	quietly set ampl_out [examine -d /tb_top/result_o]
	quietly set ampl_in [examine -d /tb_top/result_i]
	puts "Amplitude for $freq Hz and $ampl_in is $ampl_out"
} else {
	when { /tb_top/the_end } {
		puts "end of data and simulation"
		puts "time $now $resolution"
		stop
	}
	run -all
}

view -undock wave
wave zoomfull
