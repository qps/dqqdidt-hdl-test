# Clocks 
create_clock -name {CLK} -period 25 -waveform {0 12.5 } [ get_ports { CLK } ]

set_input_delay 12 -clock {CLK} [ get_ports { *SDO } ]
set_input_delay 12 -clock {CLK} [ get_ports { *BUSY } ]
set_output_delay 12 -clock {CLK} [ get_ports { *CNV } ]
set_output_delay 12 -clock {CLK} [ get_ports { *SCK } ]
