# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 18:01:42 2019
Description: Package igloo2 for control of design flow for uQDS project

@author: anskocze
A.Skoczen, AGH-UST, FP&ACS; CERN, TE-MPE-EP - March 2019
A.Skoczen, AGH-UST, FP&ACS; CERN, TE-MPE-EP - January-February 2020 - further development
"""

import subprocess
import os  
import shutil
import re
import filecmp
import sys

if os.path.exists('.\\constraints'):
    sys.path.append('.\\constraints')
    import gen_pdc_pkg as pdc
else:
    print('No access to gen_pdc_pkg package')
    #exit()

#user extention for Excel part list file 
xls_ext = '_pin_list_v1-1'

def check_dir_empty(ddir):
    if(os.path.exists(ddir) and os.path.isdir(ddir)):
        if(len(os.listdir(ddir)) == 0):
            print("Warning: Directory ", ddir, " is empty")
            return 0
        return 1
    else:
        print("Warning: ", ddir, " dose not exist.")
        return 0

def workDir(src_dir):
    dirlist = ['.\\synplify'] + ['.\\constraints'] + ['.\\bitstream'] + ['.\\backanno'] + ['.\\actel']
    if(check_dir_empty(src_dir) == 0): 
        return 0
    print("DIRs: ", dirlist)
    for direc in dirlist:
        if not os.path.exists(direc):
            os.makedirs(direc)
            if(direc == '.\\constraints'):
                print("Warning: Lack of constraints in ", direc, " directory.")
                return 0
    if(check_dir_empty('.\\constraints') == 0): 
        return 0
    return 1

def clean():
    dirlist = ['.\\synplify'] + ['.\\actel'] + ['.\\bitstream'] + ['.\\backanno']
    for direc in dirlist:
        if os.path.exists(direc):
            shutil.rmtree(direc)
            print(direc," removed - synthsis and implementation")
    return 1

def clean_impl():
    dirlist = ['.\\actel'] + ['.\\bitstream'] + ['.\\backanno']
    for direc in dirlist:
        if os.path.exists(direc):
            shutil.rmtree(direc)
            print(direc," removed - implementation")
    return 1

def is_tool(name):
    """Check whether `name` is on PATH and marked as executable """
    p = shutil.which(name)
    if p == None:
        return False
    else:
        return True

def check_license(company):
    lm_util = "lmutil.exe"
    path_lm = shutil.which(lm_util)
    if path_lm == None : 
            print("Lack of license util ", lm_util)
            return False
    if company == "Synopsys":
        lic_env = "SNPSLMD_LICENSE_FILE"
        if lic_env not in os.environ:
            print("Lack of licenses server path: ", lic_env)
            return False
        else:
            val_lic_env = os.environ[lic_env]
        args = [lm_util]
        args.extend(['lmstat'])
        args.extend(['-c'])
        args.extend([val_lic_env])
        args.extend(['-a'])
        print("CMD: ", args)
        print("wait for output ...")
        lic_log = subprocess.check_output(args, shell=True)
        print('------------------ ', company, ' License check -------------------')
        print(lic_env, " = ", val_lic_env)
        #ix = 0
        srv = ''
        for line in lic_log.decode("utf-8").split('\n'):
            #ix = ix + 1 
            #print(ix, " -- ", line)
            server = re.findall(r'License server status:', line)
            if server:
                srv = line.split(':')[1]
                #print(ix, " --- ", srv)
            line2 = re.findall(r'synplify', line)
            if line2:
                print(line.strip() , " at server", srv)
        print('------------------ License check done-------------------')
    return True
        
def find_synthesis_tool():
    tools_list = ['synplify_premier.exe', 'synplify_pro.exe']
    for tool in tools_list:
        full_path = shutil.which(tool)
        if full_path != None : 
            return tool
    return None

def purpose_of_config_block(fname, pattern):
    cnt = 0
    cb_purpose = 'Purpose of Config Block is not known!'
    with open(fname, 'r') as fin:
        lines = fin.readlines()
    for line in lines:
        if pattern in line:
            one_entry = line.splitlines()[0].split(pattern, 2)
            cnt = cnt + 1
    if cnt == 1:
        cb_purpose = one_entry[1]
        print(cb_purpose)
    if cnt > 1:
        print('Ambigues @brief description ! in', fname)
    if cnt == 0:
        print('Lack of information about purpose of config block in', fname)
    return cnt, cb_purpose

def synthesize(synth_resfile, src_dir):
    """ Launch synthesis with synplify_premier """
    if not check_license('Synopsys'):
        print('Synopsys License failed')
        exit
    if not os.path.exists(synth_resfile):
        tool = find_synthesis_tool()
        if is_tool(tool):
            args = [tool]
            args.extend(['-batch'])
            args.extend(['-tcl'])
            tclscript = '.\\scripts\\synplify.tcl'
            if not os.path.exists(tclscript):
                print("TCL script for Synplify ",tclscript," is missing")
                return False
            args.extend([tclscript])
            args.extend(['-log'])
            log_synplify = '.\\synplify\\xx.log'
            args.extend([log_synplify])
            print("CMD: ", args)
            print("wait for output ...")
            subprocess.run(args)
            if os.path.exists(log_synplify):
                file = open(log_synplify, "r") 
                for line in file: 
                    print(line)
                print("CMD: finished")
                return True
            else:
                print("Lack of synplify logfile")
                return False
        else:
            print(tool, "is not reachable - check your path.cfg and varaiable PATH")
            return False
    else:
        print("Synthesis already done")
        print("If you want to resynthesise clean previous result with option clean")
        return False

def add_syn_radhardlevel(fdc,attrval,lib_name,top_name):
    """ Add syn_radhardlevel attribute to FDC constraint file """
    new_line = '\ndefine_attribute  {v:' + lib_name + '.' + top_name + '} {syn_radhardlevel} {'+attrval+'}'
    with open (fdc, 'a' ) as f:
        f.write(new_line)
    print('Added syn_radhardlevel with',attrval,'to',fdc)

def tmr(tmrarg,lib_name,top_name):
    """ Update FDC with syn_radhardlevel attribute """
    path_fdc = os.path.join('.\\constraints', top_name+'.fdc')
    if os.path.exists(path_fdc):
        if tmrarg == 'tmr':
            with open (path_fdc, 'r' ) as f:
                content = f.read()
            if re.search('{syn_radhardlevel} {tmr}', content, flags = re.M):
                print('Attribute TMR already define in FDC file')
            elif re.search('{syn_radhardlevel} {none}', content, flags = re.M):
                content_new = re.sub('none', 'tmr', content, flags = re.M)
                with open (path_fdc, 'w' ) as f:
                    f.write(content_new)
                print('Updated syn_radhardlevel:', path_fdc,'none -> tmr')
            else:
                add_syn_radhardlevel(path_fdc,'tmr',lib_name,top_name)
        else:
            with open (path_fdc, 'r' ) as f:
                content = f.read()
            if re.search('{syn_radhardlevel} {none}', content, flags = re.M):
                print('Attribute no TMR already define if FDC')
            elif re.search('{syn_radhardlevel} {tmr}', content, flags = re.M):
                content_new = re.sub('tmr', 'none', content, flags = re.M)
                with open (path_fdc, 'w' ) as f:
                    f.write(content_new)
                print('Updated syn_radhardlevel:', path_fdc,'tmr -> none')
            else:
                add_syn_radhardlevel(path_fdc,'none',lib_name,top_name)
        return True
    else:
        print('Lack of contraints file:',path_fdc,'- create it')
        return False

def export_stapl(synth_resfile, lver, top_name, prj_name):
    """ Extract STAPL file from PPD with libero """
    ppd_file = os.path.join('.\\actel\\designer\\' + top_name, top_name + '.ppd')
    if os.path.exists(ppd_file):
        print("PPD OK")
    else:
        print("PPD', ppd_file, 'does not exist")
        exit()
    tcl_stapl =  os.path.join('.\\scripts', 'stapl.tcl')
    tool = 'libero.exe'
    if is_tool(tool):
        args = [tool]
        if not os.path.exists(tcl_stapl):
            print("TCL script for STAPL ",tcl_stapl," is missing")
            exit()
        args.extend(['SCRIPT:'+tcl_stapl])
        #args.extend(['SCRIPT_ARGS:'+os.path.dirname(synth_resfile)])
        log_libero = '.\\actel\\stapl.log'
        arg_log_libero = 'LOGFILE:' + log_libero
        args.extend([arg_log_libero])
        print("CMD: ", args)
        print("wait for output ...")
        subprocess.run(args)
        if os.path.exists(log_libero):
            file = open(log_libero, "r") 
            for line in file: 
                print(line) 
            print("CMD: finished")
            #rename the STAPL 
            old_stapl = find_file_ext('.\\bitstream', 'stp')
            if old_stapl == "":
                print("Lack of STAPL")
                return False
            new_stapl = os.path.join('.\\bitstream', prj_name + '_' + take_rev_git() + '.stp')
            os.rename(old_stapl, new_stapl)
            print("Path to STAPL:", new_stapl)
            return True 
        else:
            print("Lack of libero STAPL logfile")
            return False
    else:
        return False

def pdc_check_gen(lver, top_name, prj_name, xls_ext):
    in_file = os.path.join('.\\constraints\\', os.path.basename(prj_name)+xls_ext+'.xlsx')
    pdc_flag = True
    pdc_flag = pdc.gen_pdc(int(lver), in_file, top_name)
    if pdc_flag:
        print('PDC Done')
        return True
    else:
        print('PDC Failed')
        return False
    

def implement(synth_resfile, lver, src_dir, top_name, prj_name):
    """ Launch implementation with libero """
    if os.path.exists(synth_resfile):
        print("Go to implement by Libero",lver)
        if not pdc_check_gen(lver, top_name, prj_name, xls_ext):
            return False
        sdcfile = '.\\constraints\\' + top_name + '.sdc'
        if not os.path.exists(sdcfile):
            print("Lack of SDC file ",sdcfile)
            exit()
        tool = 'libero.exe'
        if is_tool(tool):
            args = [tool]
            tclscript = '.\\scripts\\libero'+lver+'.tcl'
            if not os.path.exists(tclscript):
                print("TCL script for Libero ",tclscript," is missing")
                exit()
            args.extend(['SCRIPT:'+tclscript])
            args.extend(['SCRIPT_ARGS:'+os.path.dirname(synth_resfile)])
            log_libero = '.\\igloo2.log'
            arg_log_libero = 'LOGFILE:' + log_libero
            args.extend([arg_log_libero])
            print("CMD: ", args)
            print("wait for output ...")
            subprocess.run(args)
            if os.path.exists(log_libero):
                file = open(log_libero, "r") 
                for line in file: 
                    print(line) 
                print("CMD: finished")
                #rename the STAPL 
                old_stapl = find_file_ext('.\\bitstream', 'stp')
                if old_stapl == "":
                    print("Lack of STAPL")
                    return False
                cb_nr = cfg_blk_nr(src_dir)
                if os.path.isdir(os.path.join('.\\','.git')):
                    gitver = take_rev_git()
                else:
                    gitver = 'no-git-proj'
                if cb_nr == 0:
                    new_stapl = os.path.join('.\\bitstream\\', prj_name + '_' + gitver + '.stp')
                else:
                    new_stapl = os.path.join('.\\bitstream\\', prj_name + '_' + str(cb_nr) + '_' + gitver + '.stp')
                #new_stapl = os.path.join('.\\bitstream', prj_name + '_' + take_rev_git() + '.stp')
                os.rename(old_stapl, new_stapl)
                print("Path to STAPL:", new_stapl)
                return True 
            else:
                print("Lack of libero logfile")
                return False
    else:
        print("Lack of EDIF output ",synth_resfile," from synthsis - resynthesise with option synth")
        return False

def take_rev_git():
    """ Return first 7 chars of git hash. If dirty '1' is added, if clean string ends with '0' """
    tool = 'git.exe'
    if is_tool(tool):
       
        # call git two times once with dirty indicator once without
        # if both return strings have equal length, git is clean 
        # if different size git is not clean 
        
        #tool = 'git.exe'
        args = [tool]
        args.extend(['describe'])
        args.extend(['--always'])
        args2 = [tool]
        args2.extend(['describe'])
        args2.extend(['--always'])
        args2.extend(['--dirty=1'])
        #print("CMD: ", args)
        #print("wait for output ...")
        proc = subprocess.run(args, encoding='utf-8', stdout=subprocess.PIPE)
        # get output from git
        t = proc.stdout
        t= t.strip()
        proc = subprocess.run(args2, encoding='utf-8', stdout=subprocess.PIPE)
        t2 = proc.stdout
        t2 = t2.strip()
        #print(len(t))
        
        
        if len(t) == len(t2) :
            #print('clean')
            print(t[-7:]+'0')
            return t[-7:]+'0'
        else:
            #print('dirty')
            #print(t2[-8:])
            return t2[-8:]

        
        #print(t[2].splitlines()[0][1:]+'0')
        #return t[2].splitlines()[0][1:]+'0'
    else:
        print(tool,"is not available")
        return 'no-git'


def script_help(sw):
    if sw == 'cmd':
        fname = '.\\help.txt'
    elif sw == 'cfg':
        fname = '.\\cfg_help.txt'
    if os.path.exists(fname):
        with open(fname, 'r') as fin:
            print(fin.read())
    else:
        print('Help does not exist')

def user_paths():
    try:
        up = os.environ['PATH'].split(os.pathsep)
        print(type(up))
        user_paths = '\n'.join(up)
    except KeyError:
        user_paths = []
    return user_paths 

def find_first_file_sub(mdir, ext):
    for dirpath, dirnames, filenames in os.walk(mdir):
        for filename in [f for f in filenames if f.endswith("."+ext)]:
            return os.path.join(dirpath, filename)
    return ""

#find file with name name in directoey src_dir
def find_file(src_dir, name):
    for p, d, f in os.walk(src_dir):
        for fn in f:
            if fn == name:
                return os.path.join(p, fn)
    return ""

#find first file with extension ext i directoey src_dir
def find_file_ext(src_dir, ext):
    for file in os.listdir(src_dir):
        if file.endswith(ext):
            return os.path.join(src_dir, file)
    return ""
            
def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def check_add_path(name, path):
    up = os.environ['PATH'].split(os.pathsep)
    #print(type(up))
    cnt = 0
    for x in up:
        if (x.find(name) != -1):
           cnt += 1
    if(cnt == 0):
        print(name," added to PATH")
        os.environ["PATH"] += os.pathsep + path
        #print('\n'.join(os.environ['PATH'].split(os.pathsep)))
    if(cnt == 1):
        print(name," is already in PATH")
    if(cnt > 1):
        print("Error: Conflict - to many path to ",name," in the PATH - manage you PATH environmental variable")

def buildstamp(src_dir, bs_name):
    """ Update or create VHDL file with git revision number """
    """ to do: Creation need testing and correction """
    if os.path.isdir(os.path.join('.\\','.git')):
        gitrev = take_rev_git()
        print("git rev:",gitrev)
        full_path = find_file(src_dir, bs_name)
        print('Buildstamp:', full_path)
        if full_path:
            with open (full_path, 'r' ) as f:
                content = f.read()
                content_new = re.sub('([/"])(........)', '"'+gitrev, content, flags = re.M)
            with open (full_path, 'w' ) as f:
                f.write(content_new)
            print('Updated:', full_path)
        else:
            full_path = os.path.join(src_dir+'\\lib', bs_name)
            bs_handle = open(full_path, 'w+') 
            fname = '.\\template_buildstamp.txt'
            if os.path.exists(fname):
                with open(fname, 'r') as fin:
                    lines = fin.readlines()
                for l in lines:
                    l = l.replace('x"zzzzzzzz";','x"'+gitrev+'";')
                    print(l)
                    bs_handle.write(l)
            else:
                print("Lack of template file")
                return False
            print('Createed:', full_path)
    else:
        print("Project is not in GIT - buildstamp not necessary")
    return True

def clocks(srr_file):
    """ Display short report about clocks """
    if 'Performance Summary' in open(srr_file).read():
        text = open(srr_file).readlines()
        begining = text.index('Performance Summary\n')
        end = text.index('Clock Relationships\n')
        for nn in range(begining, end):
            tx = text[nn]
            if tx and tx.strip(): 
                print(tx)
    return 0

def time_violations(tv_files, tv_path):
    """ Display short report concerning negative slacxks if exists"""
    for f in tv_files:
        tvpath = os.path.join(tv_path, f)
        i = 0
        text = open(tvpath).readlines()
        for t in text:
            matched_lines = [line for line in t.split('\n') if "Slack" in line]
            if matched_lines: 
                i = i + 1
                print(i, '.', matched_lines)
        if i > 0 :
            print(i, "negative slacks - detailed information in", tvpath)
    return 0

def io_func_rep(io_bank_file):
    """ Display short report about I/O functions """
    combined = False
    text = open(io_bank_file).readlines()
    begining = text.index('I/O Function:\n') + 2
    end = text.index('I/O Technology:\n') - 2
    for nn in range(begining, end):
        print(text[nn].replace('\n', ' '))
        fields = text[nn].split('|')
        if nn > (begining + 1):
            #print(fields[2])
            if int(fields[2]) > 0:
                    combined = True
    return combined

def cfg_blk_nr(src_dir):
    """ Return config block number - by comparison of two VHDL files """
    nr = 0
    cb_path = os.path.join(src_dir,'config_blocks')
    if os.path.exists(cb_path):
        cb_files = [f for f in os.listdir(cb_path) if not f.endswith("_lib.vhd") if os.path.isfile(os.path.join(cb_path, f))]
        f_golden = '.\\hdl\\src\\config_block.vhd'
        for cbf_vhdl in cb_files:
            ff = os.path.join(cb_path, cbf_vhdl)
            if filecmp.cmp(f_golden, ff):
                nr = cbf_vhdl.split('_')[2].split('.')[0]
    else:
        print('No config blocks in current design')
    return nr
